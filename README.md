# cellshapy

`cellshapy` is a python package for the quantification and analysis of cell shape 
dynamics from live-cell microscopy. 

Its main features are:
* importing dynamic shapes from segmented images (TIFF format), arrays or contours 
* extracting and aligning shape contours
* shape quantification in multivariate statistics
* time series analysis of shape descriptors
* (un)supervised dimensionality reduction and clustering using tensor decomposition methods or variational autoencoders.
* visualization of shape and morph space


# installation

In the near future, you can install `cellshapy` using pip or conda:

- Option 1: Install from git
```
pip install git+https://gitlab.com/wdeback/cellshapy.git
```

- Option 2: Clone and install
```
git clone https://gitlab.com/imb-dev/cellshapy.git cellshapy
cd cellshapy
pip install .
```

```
conda install cellshapy
```


# examples

Examples of basic usage of `cellshapy` can be found in the [example notebooks](https://gitlab.com/wdeback/cellshapy/-/tree/master/notebooks). These include video embedding and comparison between tensor decomposition and variational autoencoders, for various datasets.

More complete, but older and potentially outdated, examples can be found in the notebook names ``dataset*.ipynb``in [the notebooks/notebooks_old folder](https://gitlab.com/wdeback/cellshapy/-/tree/master/notebooks/notebooks_old). These include e.g. also static shape analysis, clustering, video reconstruction metrics, and much more.

# authors and contributors

- Walter de Back, Institute for Medical Informatics and Biometry, TU Dresden
- Sebastian Wagner, Institute for Medical Informatics and Biometry, TU Dresden
- Nico Scherf, Institute for Medical Informatics and Biometry, TU Dresden