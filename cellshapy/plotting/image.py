import matplotlib.pylab as plt

# ----------------------------------------------------------------

def _show_frame(frame,
                show_image,
                show_contour,
                cmap,
                ax,
                figsize):
    '''Plot binary image with/without contour

    Args:
        show_image (bool):
            Plot binary image
        show_contour (bool):
            Plot (unaligned) contour
        cmap (strings or matplotlib.colormap object):
            Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
            names:
            https://matplotlib.org/examples/color/colormaps_reference.html
        ax (matplotlib.Axes object):
            If provided, plots into this axes
        figsize (tuple of floats):
            Size of figure
    '''
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=figsize)

    if show_image:
        from cellshapy.data import Frame
        ax.imshow(Frame._bitarray_to_np(frame.mask), cmap=cmap)
    if show_contour:
        from ..utils.contours import close_contour
        if frame.contour_aligned is None:
            raise RuntimeError("Contours have not yet been aligned.")
        contour = frame.contour_aligned + frame.centroid
        close_contour(contour)
        #c = list(range(len(contour)))
        plt.scatter(contour[:, 1], contour[:, 0], s=100, lw=2.0, alpha=0.5) #c=c,
        plt.plot(contour[:, 1], contour[:, 0], '-', lw=2.0, alpha=0.5)
    ax.axis('off')
    plt.show()
    return fig


# ----------------------------------------------------------------

def _show_video(frames,
                duration,
                show_image,
                show_contour,
                cmap,
                ax,
                figsize,
                verbose):
    '''Create animation of video, with binary image and/or contour

    Example usage:
        ```
        from IPython.display import HTML
        HTML( videos.show() )
        ```

    Args:
        duration (float):
            Duration of video in seconds.
        show_image (bool):
            Plot binary image.
        show_contour (bool):
            Plot (unaligned) contour.
        cmap (strings or matplotlib.colormap object):
            Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
            names:
            https://matplotlib.org/examples/color/colormaps_reference.html
        ax (matplotlib.Axes object):
            If provided, plots into this axes.
        figsize (tuple of floats):
            Size of figure.
        verbose (bool):
            Show progress.
    '''
    import matplotlib.animation as animation

    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=figsize)
    else:
        fig = plt.gcf()

    nf = len(frames)
    if show_image:
        im = ax.imshow(frames[0].get_mask(), animated=True, cmap=cmap)
        ax.set_xlim([0, frames[0].get_mask().shape[0]])
        ax.set_ylim([0, frames[0].get_mask().shape[1]])
    if show_contour:
        import numpy as np
        contours = np.array([f.contour+f.centroid for f in frames])
        xmin, xmax = np.min(contours[...,1]), np.max(contours[...,1])
        ymin, ymax = np.min(contours[...,0]), np.max(contours[...,0])
        ax.set_xlim([xmin, xmax])
        ax.set_ylim([ymin, ymax])
        spline = frames[0].contour + frames[0].centroid
        line, = ax.plot(spline[:, 1], spline[:, 0], 'bo-', lw=2.0, alpha=0.5)



    ax.axis('off')

    if verbose:
        from ..config import tqdm
        nf = tqdm(range(nf))

    def update(t):
        if show_image:
            im.set_array(frames[t].get_mask())

        if show_contour:
            contour = frames[t].contour
            spline = frames[t].contour + frames[t].centroid
            line.set_data(spline[:, 1], spline[:, 0])

        if show_image and not show_contour:
            return im,
        elif not show_image and show_contour:
            return line,
        elif show_image and show_contour:
            return im, line

    anim = animation.FuncAnimation(fig,
                                   update,
                                   frames=nf,
                                   interval=((duration*1000.)/len(frames)),
                                   blit=True)

    plt.close()
    return anim

# ----------------------------------------------------------------

def _show_videos_in_row(
                videos,
                duration,
                cmap,
                ax,
                figsize,
                title=None,
                spacing = 100.0,
                shape_size=2.0,
                alpha_edge=1.0,
                alpha_face=0.7):
    '''Create animation of video, with binary image and/or contour

    Example usage:
        ```
        from IPython.display import HTML
        HTML( videos.show() )
        ```

    Args:
        videos (list):
            List of cellshapy.Video objects.
        duration (float):
            Duration of video in seconds.
        cmap (strings or matplotlib.colormap object):
            Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
            names:
            https://matplotlib.org/examples/color/colormaps_reference.html
        ax (matplotlib.Axes object):
            If provided, plots into this axes.
        figsize (tuple of floats):
            Size of figure.
        verbose (bool):
            Show progress.
    '''
    import matplotlib.animation as animation
    #from matplotlib.collections import PatchCollection
    from .polygon import polygon as plot_polygon
    from ..config import tqdm
    import numpy as np

    n_videos = len(videos)
    n_frames = len(videos[0].frames)
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=figsize)
        ax.axis('equal')
        ax.axis('off')
    else:
        fig = plt.gcf()

    positions = np.array(list(range(len(videos)))) * spacing
    positions -= np.mean(positions)
    values = list(range(len(videos)))
    vmin, vmax = min(values), max(values)
    xmin, xmax = vmin, vmax

    scale = shape_size * min(xmax-xmin, xmax-xmin) / 100.0
    xscale = scale*(xmax-xmin)
    yscale = scale*(xmax-xmin)

    all_contours = np.array([v.frames[t].contour_aligned for v in videos for t in range(n_frames)])
    xmin -= np.mean(all_contours[...,0])/2
    xmax += np.mean(all_contours[...,0])/2
    ymin, ymax = np.min(all_contours[...,1]), np.max(all_contours[...,1])


    print(all_contours.shape, xmin, xmax, ymin, ymax)
    xpad, ypad = 0., 0.

    def norm(val, vmin, vmax):
        return (float(val) - vmin) / float(vmax - vmin)

    def animate(t):
        ax.clear()

        if title is not None:
            ax.set_title(title)

        ax.set_xlim(left=xmin-xpad, right=xmax+xpad)
        ax.set_ylim(bottom=ymin-ypad, top=ymax+ypad)
        ax.axis('off')

        arrow = ax.arrow(positions[0], 0, positions[-1]-positions[0], 0,
                        linewidth=2.0, alpha=0.25, zorder=0)
        points = ax.plot(positions, [0.]*len(list(positions)),'o', color='black',
                        linewidth=2.0, alpha=0.25, zorder=0)

        patches = []
        contours = np.array([v.frames[t].contour_aligned for v in videos])

        #print(t, contours[0,1])
        for pos, value, contour in zip(positions, values, contours):

            color_edge = plt.cm.viridis(norm(value, vmin=vmin, vmax=vmax), alpha=alpha_edge)
            color_face = plt.cm.viridis(norm(value, vmin=vmin, vmax=vmax), alpha=alpha_face)

            polygon = plot_polygon(contour, [pos, 0], [xscale, yscale],
                        color_edge=color_edge, color_face=color_face)
            polygon.set_linewidth(1.0)

            _ = ax.add_patch(polygon)
            #print(contour)
            patches.append(polygon)
        return arrow, patches,

    interval = (duration/n_frames)*1000
    #print('interval = {:.2f} msec,  num_frames = {}, duration = {}'.format(interval, num_frames, duration))

    n_frames = tqdm(range(0, n_frames))

    anim = animation.FuncAnimation(fig, animate,
                                    frames=n_frames,
                                    interval=interval, blit=False)
    plt.close()
    return anim

# ----------------------------------------------------------------

def _show_videos_in_row_lines(
                videos,
                duration,
                show_image,
                show_contour,
                cmap,
                ax,
                figsize,
                verbose):
    '''Create animation of video, with binary image and/or contour

    Example usage:
        ```
        from IPython.display import HTML
        HTML( videos.show() )
        ```

    Args:
        videos (list):
            List of cellshapy.Video objects.
        duration (float):
            Duration of video in seconds.
        show_image (bool):
            Plot binary image.
        show_contour (bool):
            Plot (unaligned) contour.
        cmap (strings or matplotlib.colormap object):
            Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
            names:
            https://matplotlib.org/examples/color/colormaps_reference.html
        ax (matplotlib.Axes object):
            If provided, plots into this axes.
        figsize (tuple of floats):
            Size of figure.
        verbose (bool):
            Show progress.
    '''
    import matplotlib.animation as animation

    n_videos = len(videos)
    if ax is None:
        fig, ax = plt.subplots(1, n_videos, figsize=(figsize[0]*n_videos, figsize[1]), squeeze=False)
        ax = ax.flatten()
    else:
        fig = plt.gcf()

    if show_image:
        ims = []
        for a, v in zip(ax, videos):
            frames = v.frames
            nf = len(frames)
            im = a.imshow(frames[0].get_mask(), animated=True, cmap=cmap)
            a.set_xlim([0, frames[0].get_mask().shape[0]])
            a.set_ylim([0, frames[0].get_mask().shape[1]])
            ims.append(im)
    if show_contour:
        lines = []
        import numpy as np
        for a, v in zip(ax, videos):
            frames = v.frames
            print(v.annotations)
            nf = len(frames)
            contours = np.array([f.contour+f.centroid for f in frames])
            xmin, xmax = np.min(contours[...,1]), np.max(contours[...,1])
            ymin, ymax = np.min(contours[...,0]), np.max(contours[...,0])
            a.set_xlim([xmin, xmax])
            a.set_ylim([ymin, ymax])
            spline = frames[0].contour + frames[0].centroid
            line, = a.plot(spline[:, 1], spline[:, 0], 'bo-', lw=2.0, alpha=0.5)
            lines.append(line)

    for a in ax: a.axis('off')

    if verbose:
        from ..config import tqdm
        nf = tqdm(range(nf))

    def update(t):
        if show_image:
            for a, v, im in zip(ax, videos, ims):
                frames = v.frames
                im.set_array(frames[t].get_mask())

        if show_contour:
            for a, v, line in zip(ax, videos, lines):
                frames = v.frames
                contour = frames[t].contour
                spline = frames[t].contour + frames[t].centroid
                line.set_data(spline[:, 1], spline[:, 0])

        if show_image and not show_contour:
            return ims
        elif not show_image and show_contour:
            return lines
        elif show_image and show_contour:
            return ims, lines

    anim = animation.FuncAnimation(fig,
                                   update,
                                   frames=nf,
                                   interval=((duration*1000.)/len(frames)),
                                   blit=True)

    plt.close()
    return anim

# ----------------------------------------------------------------

def _show_videocollection(videos,
                          duration,
                          show_image=False,
                          show_contour=True,
                          cmap='gray_r',
                          figsize=(4,4),
                          ncols=None,
                          verbose=False):
    '''Creates an HTML5 table of animated videos for usage in Jupyter notebook.

    Example usage:
        ```
        from IPython.display import HTML
        HTML(videos.animate())
        ```

    Args:
        duration (float):
            Duration of video in seconds.
        show_image (bool):
            Plot binary image.
        show_contour (bool):
            Plot (unaligned) contour.
        cmap (strings or matplotlib.colormap object):
            Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
            names:
            https://matplotlib.org/examples/color/colormaps_reference.html
        figsize (tuple of floats):
            Size of individual videos.
        verbose (bool):
            Show progress

    Returns:
        string:
            An HTML5 table with the animated videos.
    '''
    from numpy import ceil, sqrt
    # import matplotlib.pylab as plt
    # import matplotlib.animation as animation

    n_videos = len(videos)
    if ncols is None:
        nr = int(ceil(sqrt(n_videos)))
        nc = nr
    else:
        nc = ncols
        nr = n_videos // nc

    from ..config import tqdm as tqdm

    pbar = tqdm(range(n_videos))

    # create html table with animated images
    html_str = "<table>\n<tr>\n"
    i = 0
    for row in range(nr):
        for col in range(nc):

            if i >= n_videos:
                break
            video = videos[i]
            i += 1

            pbar.update(1)

            html_video = video.show(show_image=show_image,
                                    show_contour=show_contour,
                                    duration=duration, cmap=cmap,
                                    figsize=figsize,
                                    verbose=False).to_html5_video()

            # html_video = '<video {} {}>'.format(row, col)
            html_str += '  <th>'+html_video+'</th>\n'

        html_str += '</tr>\n<tr>\n'

    return html_str


# ----------------------------------------------------------------

def _close_contour(contour):
    '''Closes a contour

    Args:
        contour:
            The contour to close.
    Returns:
        The closed contour.
    '''
    import numpy as np
    return np.concatenate([contour, contour[:1]])


def _show_aligned_samples(contours, contours_aligned, n_samples):
    '''Show a number of samples before and after alignment.

    Args:
        contours:
            The unaligned contours.
        contours_aligned:
            The aligned contours.
        n_samples (int):
            The number of samples to show.

    '''
    import numpy as np

    # pick number of random contours
    num_samples = min([n_samples, len(contours)])
    sample_indices = np.random.choice(len(contours), size=num_samples, replace=False)

    before = np.take(contours, sample_indices, axis=0)
    after = np.take(contours_aligned, sample_indices, axis=0)

    fig, (ax1, ax2) = plt.subplots(1, 2)

    def _plot(ax, contours):
        ax.axis('equal')
        ax.axis('off')
        for c in contours:
            c = _close_contour(c)
            ax.plot(c[:, 0], c[:, 1],
                    c='black',
                    alpha=0.5, zorder=0)
            ax.scatter(c[:, 0], c[:, 1],
                       c=np.arange(len(c)), s=50, cmap='viridis',
                       alpha=0.5, zorder=1)

    # plot samples before alignment
    ax1.set_title('Before alignment')
    _plot(ax1, before)

    # plot samples after alignment
    ax2.set_title('After alignment')
    _plot(ax2, after)

    return fig
