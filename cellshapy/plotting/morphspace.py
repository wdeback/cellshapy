import matplotlib.pylab as plt
import numpy as np
import warnings

from ..config import config

def plot_factors(factors, titles=None):
    '''
    Plot factors from tensor decomposition.

    Args:
        factors (list of arrays)
        titles (list of str)
    Returns:
        matplotlib figure
    '''
    import matplotlib.pyplot as plt
    nrows = factors[0].shape[-1]
    ncols = len(factors)
    row_height = 2
    col_width = 5
    fig, ax = plt.subplots(nrows, ncols,
                           sharex='col', sharey=False, squeeze=False,
                           figsize=(col_width*ncols,row_height*nrows))

    colors = 'brgcmyk'
    #colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
    #          '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
    #          '#bcbd22', '#17becf']

    for r in range(nrows):
        for c in range(ncols):
            if isinstance(factors[c], list):
                for ys in factors[c]:
                    xs = list(range(len(ys)))
                    ax[r,c].plot(xs, ys)
                continue
            else:
                ys = factors[c][:,r]
            xs = list(range(len(ys)))
            markerline, stemlines, baseline = ax[r,c].stem(xs, ys)
            if len(stemlines) <= 50:
                plt.setp(stemlines, 'color', colors[c])
                plt.setp(markerline, 'color', colors[c])
                plt.setp(baseline, 'color', 'k')
            else:
                plt.setp(stemlines, 'visible', False)
                plt.setp(baseline, 'visible', False)
                plt.setp(markerline, 'color', colors[c])
            ax[r,c].set_ylim(top=ys.max(), bottom=ys.min())

    # set column titles
    if titles is not None:
        for c in range(ncols):
            ax[0, c].set_title(titles[c])

    # set y labels
    for r in range(nrows):
        ax[r, 0].set_ylabel(f'Factor {r+1}', rotation=90, labelpad=0)
        #ax[r, 0].set_ylabel(f'Factor {r+1}', rotation=0, labelpad=30)

    # align y labels
    fig.align_ylabels(ax[:, 0])

    return fig

def plot_embedding_static(factors,
                          method,
                          contours,
                          colors,
                          components=None,
                          frames=None,
                          alpha_face=0.5,
                          alpha_edge=0.8,
                          shape_size=100,
                          projection=None,
                          labels=None,
                          figsize=(10,10)):

    import numpy as np
    print(f"components = {components}")
    if components is not None:
        warnings.warn(f"Using only components {components}")
        factors = factors[:, np.subtract(components, 1)]
    else:
        components = [1,2]

    projected = False
    if factors.shape[-1] > 2 or projection is not None:
        if factors.shape[-1] > 2 and projection is None:
            warnings.warn(f"Please choose projection method to project data from {factors.shape[-1]}D to 2D.")
        else:
            warnings.warn(f"Projecting from {factors.shape[-1]} components to 2D using {projection}.")
        projected = True
        from ..analysis.embedding import pca, umap, tsne, lda
        projections = ['pca', 'umap', 'tsne', 'lda']

        if labels is not None and projection in ['pca', 'tsne']:
            warnings.warn(f'Ignoring labels. Only `umap` and `lda` projections can take `labels` into account')

        if projection not in projections:
            raise ValueError(f'`projection` must be one of {projections}')
        if projection == 'pca':
            factors = pca(factors, n_components=2)
        elif projection == 'umap':
            factors = umap(factors, labels, n_components=2)
        elif projection == 'tsne':
            factors = tsne(factors, n_components=2)
        elif projection == 'lda':
            if labels is None:
                raise ValueError(f'For LDA projection (linear discriminant analysis), `labels` must be specified.')
            factors = lda(factors, labels, n_components=2)
            if factors.shape[-1] == 1:
                warnings.warn(f'Cannot create 2D output when performing LDA with {len(set(labels))} labels. Filling in factor2 with random data!')
                import numpy as np
                random_factors = np.random.random(len(factors))
                random_factors = random_factors[..., np.newaxis]
                factors = np.hstack([factors, random_factors])
    if not projected and projection is not None:
        warnings.warn(f"No need to perform {projection} projection. Data already 2D.")

    import numpy as np
    xmin = factors[:,0].min()
    xmax = factors[:,0].max()
    ymin = factors[:,1].min()
    ymax = factors[:,1].max()
    xpad = (xmax-xmin) * 0.15 # 15% extra space
    ypad = (ymax-ymin) * 0.15 # 15% extra space

    scale = shape_size * min(xmax-xmin, ymax-ymin) / 1000.0
    aspect_ratio = (xmax-xmin) / (ymax-ymin)
    xscale = scale*aspect_ratio
    yscale = scale

    values = colors
    vmin = np.min(values)
    vmax = np.max(values)
    def norm(val, vmin, vmax):
        return (float(val) - vmin) / float(vmax - vmin)

    fig, ax = plt.subplots(1, 1, figsize=figsize)
    fig.tight_layout()
    cs = [plt.cm.viridis(norm(v, vmin=vmin, vmax=vmax), alpha=alpha_face) for v in values]

    if frames is None or contours is None:
        # create simple scatter plot if no contours are given or no frames are specified
        ax.scatter(*factors.T, s=shape_size, c=cs)
        for i, pos in enumerate(factors):
            ax.text(*pos.T, s=i, horizontalalignment='center', verticalalignment='center')
    else:
        # show contours at specified frames
        from matplotlib.collections import PatchCollection
        from .polygon import polygon as plot_polygon

        for frame in frames:
            if contours.ndim == 1: # if videos are of unequal length
                contours_t = np.array([contour[frame % len(contour)] for contour in contours])
            else:
                contours_t = contours[:,frame] #np.array([frames[i, t].contour_aligned for i in range(frames.shape[0])])
            for pos, value, contour in zip(factors, values, contours_t):
                color_edge = plt.cm.viridis(norm(value, vmin=vmin, vmax=vmax), alpha=alpha_edge)
                color_edge = (0,0,0, alpha_edge)
                color_face = plt.cm.viridis(norm(value, vmin=vmin, vmax=vmax), alpha=alpha_face)

                polygon = plot_polygon(contour, pos, [xscale, yscale],
                            color_edge=color_edge, color_face=color_face)
                polygon.set_linewidth(1.0)
                ax.set_xlim(left=xmin-xpad, right=xmax+xpad)
                ax.set_ylim(bottom=ymin-ypad, top=ymax+ypad)
                #ax.text(p[c1], p[c2], '{:d}'.format(int(v)))
                _ = ax.add_patch(polygon)
                #patches.append(polygon)

    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')


    label = f"{method.upper()}" if not projected else f"{method.upper()} / {projection.upper()}"
    ax.set_xlabel(f"{label} {components[0]}")
    ax.set_ylabel(f"{label} {components[1]}")
    return fig

'''
def plot_embedding_dynamic(factors,
                            frames,
                            title,
                            show_shapes,
                            colors,
                            duration,
                            labels=None,
                            projection='pca',
                            shape_size=0.01,
                            alpha_edge=0.7,
                            alpha_face=0.3):

    import numpy as np
    from matplotlib import animation
    from datetime import timedelta
    from matplotlib.collections import PatchCollection
    from .polygon import polygon as plot_polygon

    values = colors
    vmin = np.min(values)
    vmax = np.max(values)
    def norm(val, vmin, vmax):
        return (float(val) - vmin) / float(vmax - vmin)

    num_videos = factors.shape[0]
    num_frames = frames.shape[1]

    print('num_videos = ', num_videos)
    print('num_frames = ', num_frames)

    if factors.shape[-1] > 2:
        warnings.warn(f"Projecting from {factors.shape[-1]} components to 2D using {projection}.")
        from ..analysis.embedding import pca, umap, tsne, lda
        projections = ['pca', 'umap', 'tsne', 'lda']

        if labels is not None and projection in ['pca', 'tsne']:
            warnings.warn(f'Ignoring labels. Only `umap` and `lda` projections can take `labels` into account')

        if projection not in projections:
            raise ValueError(f'`projection` must be one of {projections}')
        if projection == 'pca':
            factors = pca(factors, n_components=2)
        elif projection == 'umap':
            factors = umap(factors, labels, n_components=2)
        elif projection == 'tsne':
            factors = tsne(factors, n_components=2)
        elif projection == 'lda':
            if labels is None:
                raise ValueError(f'For LDA projection (linear discriminant analysis), `labels` must be specified.')
            factors = lda(factors, labels, n_components=2)
            if factors.shape[-1] == 1:
                warnings.warn(f'Cannot create 2D output when performing LDA with {len(set(labels))} labels. Filling in factor2 with random data!')
                import numpy as np
                random_factors = np.random.random(len(factors))
                random_factors = random_factors[..., np.newaxis]
                factors = np.hstack([factors, random_factors])


    xmin = factors[:,0].min()
    xmax = factors[:,0].max()
    ymin = factors[:,1].min()
    ymax = factors[:,1].max()
    xpad = (xmax-xmin) * 0.15 # 15% extra space
    ypad = (ymax-ymin) * 0.15 # 15% extra space

    scale = shape_size * min(xmax-xmin, ymax-ymin) / 10000.0
    xscale = scale*(xmax-xmin)
    yscale = scale*(ymax-ymin)
    #print(xscale, yscale)

    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    fig.tight_layout()

    ax.set_xlim(left=xmin-xpad, right=xmax+xpad)
    ax.set_ylim(bottom=ymin-ypad, top=ymax+ypad)
    ax.spines['left'].set_position('zero')
    ax.spines['right'].set_color('none')
    ax.spines['bottom'].set_position('zero')
    ax.spines['top'].set_color('none')
    ax.spines['left'].set_smart_bounds(True)
    ax.spines['bottom'].set_smart_bounds(True)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.set_xlabel('factor 1')
    ax.set_ylabel('factor 2')
    #ax.axis('scaled')

    def norm(val, vmin, vmax):
        return (float(val) - vmin) / float(vmax - vmin)

    def animate(t):
        ax.clear()
        patches = []
        contours = np.array([frames[i, t].contour_aligned for i in range(frames.shape[0])])

        colors_edges = []
        colors_faces = []
        patches = []

        for pos, value, contour in zip(factors, values, contours):
            print(contour.shape)
            color_edge = plt.cm.viridis(norm(value, vmin=vmin, vmax=vmax), alpha=alpha_edge)
            color_face = plt.cm.viridis(norm(value, vmin=vmin, vmax=vmax), alpha=alpha_face)
            #p = polygon(contour, pos, scale=scale, color=color_edge)

            polygon = plot_polygon(contour, pos, [xscale, yscale],
                        color_edge=color_edge, color_face=color_face)
            polygon.set_linewidth(1.0)
            ax.set_xlim(left=xmin-xpad, right=xmax+xpad)
            ax.set_ylim(bottom=ymin-ypad, top=ymax+ypad)
            #ax.text(p[c1], p[c2], '{:d}'.format(int(v)))
            _ = ax.add_patch(polygon)

            # add time annotation
            ax.text(x=xmax*0.9, y=ymax*0.95, s='{}'.format(t),
                    weight='ultralight', clip_on=True)
            patches.append(polygon)
            #colors_edges.append(color_edge)
            #colors_faces.append(color_face)

        #collection = PatchCollection(patches,
        #                        cmap=plt.cm.viridis,
        #                        edgecolors=colors_edges,
        #                        facecolors=colors_faces,
        #                        lw=1.0)
        return patches, #collection,

    interval = (duration/num_frames)*1000
    #print('interval = {:.2f} msec,  num_frames = {}, duration = {}'.format(interval, num_frames, duration))

    from ..config import tqdm as tqdm
    num_frames = tqdm(range(0, num_frames))

    anim = animation.FuncAnimation(fig, animate,
                                    frames=num_frames,
                                    interval=interval, blit=False)
    return anim
'''

def plot_embedding_dynamic(factors,
                            method,
                            contours,
                            colors,
                            duration,
                            title=None,
                            components=None,
                            projection=None,
                            labels=None,
                            shape_size=0.01,
                            alpha_edge=0.7,
                            alpha_face=0.3,
                            axis_labels=None,
                            figsize=(10,10)):
    '''Plot morph space with dynamically moving shapes'''

    import numpy as np
    from matplotlib import animation
    from datetime import timedelta
    from matplotlib.collections import PatchCollection
    from .polygon import polygon as plot_polygon

    values = colors
    vmin = np.min(values)
    vmax = np.max(values)
    def norm(val, vmin, vmax):
        return (float(val) - vmin) / float(vmax - vmin)

    if components is not None:
        warnings.warn(f"Using only components {components}")
        #factors = factors[:, np.subtract(components, 1)]
    else:
        components = [1,2]

    num_videos = factors.shape[0]
    if contours.ndim == 1: # if videos are of unequal length, take longest
        num_frames = max([len(contours[v]) for v in range(len(contours))])
    else:
        num_frames = contours.shape[1]
    print(f'num_frames = {num_frames}')
    num_components = factors.shape[-1]


    projected = False
    if factors.shape[-1] > 2 or projection is not None:
        if factors.shape[-1] > 2 and projection is None:
            warnings.warn(f"Please choose projection method to project data from {factors.shape[-1]}D to 2D.")
        else:
            warnings.warn(f"Projecting from {factors.shape[-1]} components to 2D using {projection}.")
        projected = True
        from ..analysis.embedding import pca, umap, tsne, lda
        projections = ['pca', 'umap', 'tsne', 'lda']

        if labels is not None and projection in ['pca', 'tsne']:
            warnings.warn(f'Ignoring labels. Only `umap` and `lda` projections can take `labels` into account')

        if projection not in projections:
            raise ValueError(f'`projection` must be one of {projections}')
        if projection == 'pca':
            factors = pca(factors, n_components=2)
        elif projection == 'umap':
            factors = umap(factors, labels, n_components=2)
        elif projection == 'tsne':
            factors = tsne(factors, n_components=2)
        elif projection == 'lda':
            if labels is None:
                raise ValueError(f'For LDA projection (linear discriminant analysis), `labels` must be specified.')
            factors = lda(factors, labels, n_components=2)
            if factors.shape[-1] == 1:
                warnings.warn(f'Cannot create 2D output when performing LDA with {len(set(labels))} labels. Filling in factor2 with random data!')
                import numpy as np
                random_factors = np.random.random(len(factors))
                random_factors = random_factors[..., np.newaxis]
                factors = np.hstack([factors, random_factors])

    xmin = factors[:,0].min()
    xmax = factors[:,0].max()
    ymin = factors[:,1].min()
    ymax = factors[:,1].max()
    xpad = (xmax-xmin) * 0.15 # 15% extra space
    ypad = (ymax-ymin) * 0.15 # 15% extra space

    scale = shape_size * min(xmax-xmin, ymax-ymin) / 1000.0
    aspect_ratio = (xmax-xmin) / (ymax-ymin)
    xscale = scale*aspect_ratio
    yscale = scale

    fig, ax = plt.subplots(1, 1, figsize=figsize)
    fig.tight_layout()
    fig.suptitle(title, fontsize=16, y=1.1)

    ax.set_xlim(left=xmin-xpad, right=xmax+xpad)
    ax.set_ylim(bottom=ymin-ypad, top=ymax+ypad)
    ax.spines['left'].set_position('zero')
    ax.spines['right'].set_color('none')
    ax.spines['bottom'].set_position('zero')
    ax.spines['top'].set_color('none')
    ax.spines['left'].set_smart_bounds(True)
    ax.spines['bottom'].set_smart_bounds(True)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    # X and Y axis labels
    if axis_labels is None:
        label = f"{method.upper()}" if not projected else f"{method.upper()} / {projection.upper()}"
        ax.set_xlabel(f"{label} {components[0]}")
        ax.set_ylabel(f"{label} {components[1]}")
    else:
        ax.set_xlabel(axis_labels[0])
        ax.set_ylabel(axis_labels[1])
    #ax.axis('scaled')

    def norm(val, vmin, vmax):
        return (float(val) - vmin) / float(vmax - vmin)

    def animate(t):
        ax.clear()
        if axis_labels is None:
            ax.set_xlabel(f"{label} {components[0]}")
            ax.set_ylabel(f"{label} {components[1]}")
        else:
            ax.set_xlabel(axis_labels[0])
            ax.set_ylabel(axis_labels[1])
        patches = []

        if contours.ndim == 1: # if videos are of unequal length, restart the shorter videos by taking the remainder
            contours_t = np.array([contour[t % len(contour)] for contour in contours])
        else:
            contours_t = contours[:,t]
        colors_edges = []
        colors_faces = []
        patches = []

        for pos, value, contour in zip(factors, values, contours_t):
            color_edge = plt.cm.viridis(norm(value, vmin=vmin, vmax=vmax), alpha=alpha_edge)
            color_face = plt.cm.viridis(norm(value, vmin=vmin, vmax=vmax), alpha=alpha_face)
            #p = polygon(contour, pos, scale=scale, color=color_edge)

            polygon = plot_polygon(contour, pos, [xscale, yscale],
                        color_edge=color_edge, color_face=color_face)
            polygon.set_linewidth(2.0)
            ax.set_xlim(left=xmin-xpad, right=xmax+xpad)
            ax.set_ylim(bottom=ymin-ypad, top=ymax+ypad)
            #ax.text(p[c1], p[c2], '{:d}'.format(int(v)))
            _ = ax.add_patch(polygon)
            patches.append(polygon)

            # add time annotation
            ax.annotate('{:>3d}'.format(t), arrowprops=None, xy=(0.85,0.95),
                          textcoords='figure fraction', xytext=(0.90, 0.95),
                          annotation_clip=False, weight='light')
            #colors_edges.append(color_edge)
            #colors_faces.append(color_face)

        #collection = PatchCollection(patches,
        #                        cmap=plt.cm.viridis,
        #                        edgecolors=colors_edges,
        #                        facecolors=colors_faces,
        #                        lw=1.0)
        return patches, #collection,

    interval = (duration/num_frames)*1000

    from ..config import tqdm as tqdm
    num_frames = tqdm(range(0, num_frames))

    anim = animation.FuncAnimation(fig, animate,
                                    frames=num_frames,
                                    interval=interval, blit=False)
    return anim
