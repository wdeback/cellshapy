import numpy as np
import matplotlib.pylab as plt

def plot_tensor_2D(tensor, cmap='viridis', filename=None, figsize=(10,10)):
    # plot tensor
    nr = int(np.ceil(np.sqrt(len(tensor))))
    nc = nr
    print(nr, nc)
    fig, ax = plt.subplots(nr, nc, figsize=figsize, sharex=True, sharey=True, squeeze=False)
    fig.tight_layout()
    ax = ax.flatten()
    for i,t in enumerate(tensor):
        print('.', end='')
        ax[i].set_title(f'Sample {i}')
        ax[i].matshow(t, cmap=cmap, aspect='auto')
        ax[i].axis('off')
    for j in range(i+1, len(ax)):
        ax[j].set_visible(False)

    # save to file
    if filename is not None:
        fig.savefig(filename, dpi=300, transparent=True)
        print('Figure saved to {}'.format(filename))

    return fig


def plot_tensor_3D(tensor, n_slices=5,
                       color_resolution=1.0, cmap='viridis',
                       title=None, labels=['', '', ''], format=None, alpha=1.0, fontsize=14, show_ticks=True,
                       figsize=(10,7.5), show_grid=True, show_white_background=True,
                       viewpoint_elev=25, viewpoint_azim=30,
                       filename=None):
    '''
    Plot 3D tensor as 2D slices in 3D space.

    Note: samples are assumed to be in first axis.

    https://stackoverflow.com/a/15583431/6198865

    Args:
    - tensor (ndarray, ndim=3): 3D tensor in `channel-last` format (last channel is plotted over z axis)
    - n_slices (int, 5): number of slices to plot
    - color_resolution (float, 1.0): resolution of color map, controls contourf's `levels` argument
    - title (str): title shown above figure
    - format (str): tensor format, one of ['std', 'stxy3d', 'stxy4d']
    - labels ([str, str, str]): labels for each axis, overwritten if `format` is specified
    - fontsize (float): size of labels and title (title fontsize is 25% larger)
    - figsize (tuple(width, height)): size of figure
    - show_grid (bool, False): if true, shows grid
    - show_white_background (bool, True): show white background panes
    - show_colored_panes (bool, False): show colored background panes
    - viewpoint_elev (int, 25): elevation of viewpoint
    - viewpoint_azim (int, 30): azimuth of viewpoint

    Returns:
    - matplotlib figure

    Example:
    ```
    plot_tensor_slices(tensor, n_slices=5, level_interval=5.0, figsize=(10,7.5))

    ```
    '''
    assert tensor.ndim == 3, 'Tensor must be 3D, but {:d}D tensor given'.format(tensor.ndim)

    from mpl_toolkits.mplot3d import Axes3D

    #### Plot ###

    fig = plt.figure(figsize=figsize)
    fig.tight_layout()

    ax = fig.gca(projection='3d')

    # Get rid of colored axes planes
    # First remove fill
    if show_white_background:
        ax.xaxis.pane.fill = False
        ax.yaxis.pane.fill = False
        ax.zaxis.pane.fill = False
        ax.xaxis.pane.set_edgecolor('w')
        ax.yaxis.pane.set_edgecolor('w')
        ax.zaxis.pane.set_edgecolor('w')

    # Get rid of the grid as well:
    if not show_grid:
        ax.grid(False)

    # get rid of ticks
    if not show_ticks:
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_zticks([])

    # Set view angle
    ax.view_init(elev=25, azim=30)

    # Set axis limits
    ax.set_xlim((0, tensor.shape[2]))
    ax.set_ylim((0, tensor.shape[1]))
    ax.set_zlim((0, tensor.shape[0]))

    if format == 'std':
        labels = ['descriptors', 'time', 'samples']
    if format == 'stxy3d':
        labels = ['(x,y)', 'time', 'samples']
    if format == 'stp':
        labels = ['pairwise distance', 'pairwise distance', 'time']

    # Set axis labels
    if show_ticks:
        pad_labels = 12.5  # move away from axis
    else:
        pad_labels = -10.0  # move closer to axis
    ax.set_xlabel(labels[0], fontsize=fontsize, labelpad=pad_labels)
    ax.set_ylabel(labels[1], fontsize=fontsize, labelpad=pad_labels)
    ax.set_zlabel(labels[2], fontsize=fontsize, labelpad=pad_labels, rotation=90)


    # Set figure title
    if title is not None:
        fig.suptitle(title, fontsize=fontsize*1.25, y=0.9)

    ### Data ###

    # set sequence of slices
    max_slice = tensor.shape[0]
    slices = np.round(np.linspace(start=0,
                        stop=max_slice-1,
                        num=n_slices)).astype(np.int)
    print('Slices: {}'.format(slices))

    # grid points
    X = np.arange(0, tensor.shape[2], 1)
    Y = np.arange(0, tensor.shape[1], 1)
    X, Y = np.meshgrid(X, Y)

    # plot slices with plot_surface
    for i in slices:
        # get slice
        tensor_slice = tensor[i]
        # Z = height of slice
        Z = i*np.ones(X.shape)
        # colors are normalized by maximum of whole tensor
        colors = plt.get_cmap(cmap)(tensor_slice/tensor.max())
        # plot slice
        ax.plot_surface(X, Y, Z,
                        rstride=1, cstride=1,
                        facecolors=colors,
                        shade=False, alpha=alpha)

    # save to file
    if filename is not None:
        fig.savefig(filename, dpi=300, transparent=True)
        print('Figure saved to {}'.format(filename))

    return fig
