import matplotlib.pylab as plt
import pandas as pd
import numpy as np
import warnings

def _norm(val, vmin, vmax):
    if vmin == vmax:
        return 1.0
    else:
        return (float(val) - vmin) / float(vmax - vmin)

#---------------------------------------------------------------

def plot_embedding(positions,
                   title,
                   method,
                   components=[1,2],
                   projection='pca',
                   show_shapes=True,
                   show_trajectories=False,
                   colors=None,
                   contours=None,
                   shape_size=0.002,
                   alpha_edge=0.7,
                   alpha_face=0.3,
                   axis_labels=None,
                   figsize=(10,10)):

    values = colors
    vmin = np.min(values)
    vmax = np.max(values)
    if show_shapes and contours is None:
        raise ValueError('If show_shapes is True, you must provide contours argument.')

    projected = False
    if positions.shape[-1] > 2:
        warnings.warn(f"Projecting from {positions.shape[-1]} components to 2D using {projection}.")
        projected = True
        from ..analysis.embedding import pca, umap, tsne

        projections = ['pca', 'umap', 'tsne']
        if projection not in projections:
            raise ValueError(f'`projection` must be one of {projections}')
        if projection == 'pca':
            positions = pca(positions, n_components=2)
        elif projection == 'umap':
            positions = umap(positions, n_components=2)
        elif projection == 'tsne':
            positions = tsne(positions, n_components=2)
    if not projected and projection is not None:
        warnings.warn(f"No need to perform {projection} projection. Data already 2D.")


    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=figsize, squeeze=True)
    fig.tight_layout()
    fig.suptitle(title, fontsize=16, y=1.1)

    ax.spines['left'].set_position('zero')
    ax.spines['right'].set_color('none')
    ax.spines['bottom'].set_position('zero')
    ax.spines['top'].set_color('none')
    ax.spines['left'].set_smart_bounds(True)
    ax.spines['bottom'].set_smart_bounds(True)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    # X and Y axis labels
    if axis_labels is None:
        ax.set_xlabel(f"{method.upper()} {components[0]}", fontsize=12)
        ax.set_ylabel(f"{method.upper()} {components[1]}", fontsize=12)
    else:
        ax.set_xlabel(axis_labels[0])
        ax.set_ylabel(axis_labels[1])

    xlabel = ax.xaxis.get_label()
    lpos = xlabel.get_position()
    xlabel.set_position((1.0, 0))
    ylabel = ax.yaxis.get_label()
    lpos = ylabel.get_position()
    ylabel.set_position((0.0, 1.0))

    #ax.set_axes('equal')
    ax.axis('scaled')

    import pandas as pd
    if isinstance(positions, pd.DataFrame):
        positions = positions.values

    factor = 1.25
    component_x = components[0]-1
    component_y = components[1]-1

    # select the positions for the requested components
    positions = positions[:, np.subtract(components, 1)]

    xmin, xmax = np.min(positions[:,0]) * factor, np.max(positions[:,0]) * factor
    ymin, ymax = np.min(positions[:,1]) * factor, np.max(positions[:,1]) * factor
    ax.set_xlim([xmin, xmax])
    ax.set_ylim([ymin, ymax])

    #print(show_shapes, show_trajectories)
    #print(positions.shape)
    #print(values.shape)
    #print(contours.shape)

    if show_shapes:
        from .polygon import polygon
        from matplotlib.collections import PatchCollection

        from ..config import tqdm as tqdm

        scale = shape_size * min(xmax-xmin, ymax-ymin) / 10000.0
        #print(shape_size, scale)
        colors_edges = []
        colors_faces = []
        patches = []
        for i, (pos, value, contour) in tqdm(list(enumerate(zip(positions, values, contours)))):
            # set color
            color_edge = plt.cm.viridis(_norm(value, vmin=vmin, vmax=vmax), alpha=alpha_edge)
            color_face = plt.cm.viridis(_norm(value, vmin=vmin, vmax=vmax), alpha=alpha_face)
            #print(f"value = {value}, color = {_norm(value, vmin=vmin, vmax=vmax)}")

            # create polygon
            p = polygon(contour, pos, scale=scale,
                        color_edge=color_edge, color_face=color_face)
            patches.append(p)
            colors_edges.append(color_edge)
            colors_faces.append(color_face)

        pc = PatchCollection(patches,
                             cmap=plt.cm.viridis,
                             edgecolors=colors_edges,
                             facecolors=colors_faces,
                             lw=1.0, zorder=10)
        #
        #pc.set_array(np.array(colors))
        ax.add_collection(pc)
    else:
        ax.scatter(positions[:,0],positions[:,1],c=values)


    if show_trajectories:

        from matplotlib.collections import LineCollection
        import pandas as pd

        def get_spline(points, n=100.):
            '''return spline through points'''
            from scipy.interpolate import splprep, splev
            tck,u = splprep(points.T, s=0.0, k=3)
            unew = np.arange(0, 1.0, 1./n)
            x, y = splev(unew, tck, der=0)
            p_new = np.array([x,y]).T
            return p_new

        # create dataframe from positions and values
        df = pd.DataFrame([values,positions]).T
        df.columns = ['values', 'positions']

        # for each group of values (colorcode), add a spline
        for val, group in df.groupby('values'):
            points = group['positions'].values
            points = np.array([np.array(p) for p in points])

            try:
                spline = get_spline(points[:,:2])
                spline = spline[0:,np.newaxis,:2]
                linesegments = np.concatenate([spline[:-1], spline[1:]], axis=1)
            except:
                warnings.warn('Unable to fit spline for trajectory, falling back to connecting line.')
                points = points.reshape(-1, 1, 2)
                linesegments = np.concatenate([points[:-1], points[1:]], axis=1)

            norm = plt.Normalize(values.min(), values.max())
            lc = LineCollection(linesegments, cmap='viridis', norm=norm, zorder=0)

            vals = np.array([np.array(v) for v in group['values'].values])
            lc.set_array( vals )
            lines = ax.add_collection(lc)

        print(points.shape)

    # add colorbar
    ## WdB: this does not work with opacity of the PatchCollection
    ## plt.colorbar() needs a call to `set_array`, but this override the opacity of the patches
    '''
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(ax)
    # create an axes on the right side of ax. The width of cax will be 5%
    # of ax and the padding between cax and ax will be fixed at 0.15 inch.
    cax = divider.append_axes("right", size="2%", pad=0.15)
    cbar = plt.colorbar(pc, cax=cax)
    cbar.set_label('label')
    '''

    return fig

#---------------------------------------------------------------

def plot_polar_embedding(embedding,
                        title,
                        contours,
                        features,
                        shape_size,
                        alpha_edge=1.0,
                        alpha_face=1.0,
                        num_bins=8,
                        num_quantiles=4,
                        label_threshold=0.5,
                        verbose=False,
                        figsize=(6,6)):
    '''
    Polar medding plot with shapes subdivided in angular bins, with correlations to features.

    Reference:
    - Gordonov et al.

    Args:

    Returns:
        - pandas dataframe with max pearson correlations of features with rotations
        - matplotlib figure

    '''

    #print('num_bins = ', num_bins)
    #print('num_quantiles = ', num_quantiles)

    import numpy as np
    from .polygon import polygon
    from matplotlib.collections import PatchCollection

    ## helper functions
    def rotate(points, degrees):
        '''Rotate 2D points around origin in CCW direction.'''
        theta = np.deg2rad(degrees)
        c, s = np.cos(theta), np.sin(theta)
        R = np.matrix([[c, -s], [s, c]]) # rotation matrix
        p_rot = np.dot(points, R.T)
        return np.array(p_rot)

    def cart2pol(x, y):
        '''Conversion from Cartesian to polar coordinates'''
        rho = np.sqrt(x**2 + y**2)
        phi = np.arctan2(y, x)
        return(rho, phi)

    def pol2cart(rho, phi):
        '''Conversion from Polar to Cartesian coordinates'''
        x = rho * np.cos(phi)
        y = rho * np.sin(phi)
        return(x, y)

    def cut_quantiles_indices(a, num):
        '''Return lists of indices for num quantiles'''
        a = np.array(a)
        if len(a) == 0 or a.max() == a.min():
            return [np.array([]) for i in range(num)]
        qs = np.linspace(a.min(), a.max(), num=num+1, endpoint=True)
        return [np.argwhere((a > qs[i]) & (a < qs[i+1])).ravel() for i,q in enumerate(qs[:-1])]


    ### 1. Polar embedding plot ####

    # - Get embeddings, and translate to origin, and convert to polar coordinates

    import pandas as pd
    if isinstance(embedding, pd.DataFrame):
        embedding = embedding.values
    # translate embedding to origin
    embedding -= embedding.mean(axis=0)
    # convert to polar coordinates
    c_rho, c_phi = cart2pol(embedding[:, 0], embedding[:, 1])

    # get min/max cooridinates (for polygon scaling)
    factor = 1.1
    xmin, xmax = -0.5,0.5
    ymin, ymax = -0.5,0.5
    #print(xmin, xmax)

    # - Divide into angular bins
    offset = np.pi / num_bins  # offset such that bins are aligned with embedding components
    bins = np.linspace(-np.pi+offset, np.pi+offset, num_bins+1, endpoint=True)
    bin_ind = np.digitize(c_phi, bins)

    contours_binned = []  # list of countours, each with samples for a section of polar space
    rho_binned = []  # list of rho cordinates (distance to origin), each with samples for a section of polar space
    n_samples = 0
    for i in range(num_bins):
        # get indices of samples that belong to bin
        sample_ind = np.where(bin_ind == i)
        # get contours of samples of the bin, add to list
        contours_bin = contours[sample_ind]
        contours_binned.append(contours_bin)
        # get embedding rho-coordinate of samples of the bin, add to list
        rho_bin = c_rho[sample_ind]
        rho_binned.append(rho_bin)

        if verbose:
            print("bin {} ({:.2f}) samples: {}".format(i, bins[i], len(contours_bin)))
        n_samples += len(contours_bin)

    #assert (n_samples == len(contours)), 'Not all samples were binned ({}/{})'.format(num_samples, len(contours))

    # - Subdivide each bin int number of percentiles

    colors_edges = []
    colors_faces = []
    patches = []

    for i, (contours, rhos) in enumerate(zip(contours_binned, rho_binned)):
        if len(rhos) == 0:
            continue
        # subdivide bin into quantiles according to rho (= distance to origin)
        indices_quantiles = cut_quantiles_indices(rhos, num=num_quantiles)

        for q, indices_quantile in enumerate(indices_quantiles):
            # sample from quantiles
            if len(indices_quantile) == 0: continue
            ind_middle = indices_quantile[int(len(indices_quantile)//2)]

            # get coordinates
            offset_from_center = 0.05
            pos=(bins[i]+(np.pi/num_bins), offset_from_center+(q+1)*(0.4/num_quantiles))
            pos_x, pos_y = pol2cart(pos[1], pos[0])
            if pos_x > xmax: xmax = pos_x
            if pos_y > ymax: ymax = pos_y
            pos = np.array([pos_x, pos_y])
            #print(i, pos)
            #ax.scatter(pos[0], pos[1])


            #pos = np.array([pos_x*0.99, pos_y])+np.array([0.5,0.5])

            scale = shape_size * min(xmax-xmin, ymax-ymin) / 10000.0
            color_edge = plt.cm.viridis(_norm(q, vmin=0, vmax=num_quantiles-1), alpha=alpha_edge)
            color_edge = 'black'
            color_face = plt.cm.viridis(_norm(q, vmin=0, vmax=num_quantiles-1), alpha=alpha_face)
            contour = np.squeeze(np.array(contours[ind_middle]))
            p = polygon(contour, pos, scale=scale,
                        color_edge=color_edge, color_face=color_face)
            patches.append(p)
            colors_edges.append(color_edge)
            colors_faces.append(color_face)

    # - Plot polar plot with shapes
    pc = PatchCollection(patches,
                            cmap=plt.cm.viridis,
                            edgecolors=colors_edges,
                            facecolors=colors_faces,
                            lw=1.0, zorder=10)


    fig, ax = plt.subplots(1,1, figsize=figsize)
    rect = [0.0, 0.0, 1.0, 1.0]
    # the cartesian axis
    ax = fig.add_axes(rect, frameon=True)
    ax.set_xlim([xmin, xmax])
    ax.set_ylim([ymin, ymax])
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_position('zero')
    ax.spines['right'].set_color('none')
    ax.spines['bottom'].set_position('zero')
    ax.spines['top'].set_color('none')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zorder(0)
    #ax.set_xlabel('PC 1')
    #ax.set_ylabel('PC 2')
    #ax.axis('off')

    ax.add_collection(pc)

    # the polar axis
    ax_polar = fig.add_axes(rect, polar=True, frameon=False, zorder=0)
    ax_polar.set_rmax( c_rho.max() )
    ax_polar.set_theta_direction(1)
    ax_polar.set_thetagrids(range(0, 360, int(360/num_bins)))
    #ax_polar.set_rlabel_position(-90+20)  # get radial labels away from plotted line
    #ax_polar.set_label('')
    ax_polar.set_rticks([])
    ax_polar.grid(True)
    ax_polar.set_zorder(0)
    ax_polar.set_xticklabels([])



    #### 2. Find correlations with features ####

    from scipy.stats import pearsonr, zscore
    import pandas as pd

    # normalize/standardize features columnwise
    features = (features - features.mean()) / (features.std() + 1e-6)

    # - Find correlations of members of bin
    df_corr = pd.DataFrame([], columns=features.columns)
    for rotation in np.linspace(0, stop=360, num=90, endpoint=False):
        embedding_rotated = rotate(embedding[:,:2], -rotation) ## CCW
        corrs = dict()
        for column in features.columns:
            if column in ['video', 'frame', 'orientation']:
                continue
            r, p = pearsonr(features[column], embedding_rotated[:,0])
            corrs[column] = r
            corrs['rotation'] = np.deg2rad(rotation)
        df_corr = df_corr.append(corrs, ignore_index=True)

    # for each feature (=col), compute average vector
    results_phi = dict()
    results_phi2 = dict()
    results_rho = dict()
    vectors = dict()
    for col in features.columns:
        vec = np.array([0.,0.])
        for a,v in zip(df_corr['rotation'], df_corr[col]):
            # convert from polar to cartesian coordinates
            x,y = pol2cart(v, a)
            # cumulative vector
            vec[0] += x
            vec[1] += y

        # mean vector
        vec /= len(df_corr['rotation'])
        # convert back to polar coordinates
        rho, phi = cart2pol(vec[0], vec[1])

        if not np.isfinite(rho) or not np.isfinite(phi) or rho < label_threshold:
            continue

        # annotate axis
        rotation = np.rad2deg(phi)
        if rotation > 90: rotation -= 180
        if rotation < -90: rotation += 180

        def camelCase(word):
            return ''.join(x.capitalize() or '_' for x in word.split('_'))

        text = '{} ({:.2f})'.format(camelCase(col), rho)
        text = '{}'.format(camelCase(col))
        #if verbose:
        #print('{:<25} : {:>10.2f} : {:>10.2f}'.format(col, phi, rho))
        results_phi[col] = phi
        results_phi2[col] = np.rad2deg(phi)
        results_rho[col] = rho
        color = plt.cm.gray_r(_norm(0.10+rho, 0, 0.5))
        ax_polar.annotate(text, va="center", ha="center",
                            xy=(phi, c_rho.max()*1.2), # position text outside of plot
                            color=color,
                            annotation_clip=False, # prevent annotation outside of plot from being invisible
                            rotation=rotation, # rotate text to polar coordinates, 180 turn if upside down
                            fontsize=8+10*rho)  # adjust size to variance


    df_results = pd.DataFrame([results_rho, results_phi2, results_phi]).T
    df_results.columns = ['pearsonr', 'rotation (degree)', 'rotation (rad)']
    df_results = df_results.sort_values(by='pearsonr', ascending=False)
    return df_results, fig

    ## second plot

    fig, ax = plt.subplots(1,1, figsize=(12,12))
    ax.set_visible(False)
    # the polar axis
    ax_polar = fig.add_axes(rect, polar=True, frameon=False, zorder=0)
    ax_polar.set_rmax( 0.8 )
    ax_polar.set_theta_direction(1)
    ax_polar.set_rticks([])
    ax_polar.grid(False)
    ax_polar.set_xticklabels([])
    #ax_polar.set_visible(False)

    vectors = dict()
    for col in features.columns:
        vec = np.array([0.,0.])
        for a,v in zip(df_corr['rotation'], df_corr[col]):
            # convert from polar to cartesian coordinates
            x,y = pol2cart(v, a)
            # cumulative vector
            vec[0] += x#/np.sqrt(x*x + y*y)
            vec[1] += y#/np.sqrt(x*x + y*y)
            #print('{:>5.2f} {:>5.2f} {:>10.2f} {:>5.2f}'.format(a,0.5,*vec))


        # mean vector
        vec /= len(df_corr['rotation'])
        # convert back to polar coordinates
        rho, phi = cart2pol(vec[0], vec[1])

        if not np.isfinite(rho) or not np.isfinite(phi) or rho < label_threshold:
            continue

        # annotate axis
        rotation = np.rad2deg(phi)
        if rotation > 90: rotation -= 180
        if rotation < -90: rotation += 180

        def camelCase(word):
            return ''.join(x.capitalize() or '_' for x in word.split('_'))

        text = '{} ({:.2f})'.format(camelCase(col), rho)
        text = '{}'.format(camelCase(col))
        #if verbose:
        print('{:<25} : {:>10.2f} : {:>10.2f}'.format(col, phi, rho))
        color = plt.cm.gray_r(_norm(0.25+rho, 0, 1.0))
        ax_polar.arrow(0,0,phi, rho, head_width=0., head_length=0., lw=1, color=color)
        ax_polar.annotate(text, va="center", ha="center",
                            xy=(phi,rho*1.05), # position text outside of plot
                            color=color,
                            annotation_clip=False, # prevent annotation outside of plot from being invisible
                            #rotation=rotation, # rotate text to polar coordinates, 180 turn if upside down
                            fontsize=10+8*rho)  # adjust size to variance

    return fig



