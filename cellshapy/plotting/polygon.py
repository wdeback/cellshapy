
def polygon(contour, position, scale,
            color_face, color_edge,
            polar=False):
    '''Create polygon object from contour.

    Args:
        scale (float or [x,y]):
            size of polygon
        color_edge (float):

        color_face (float):

    Return:
        polygon object
    '''

    import numpy as np
    from matplotlib.patches import Polygon

    if polar:
        def cart2pol(x, y):
            '''Conversion from Cartesian to polar coordinates'''
            rho = np.sqrt(x**2 + y**2)
            phi = np.arctan2(y, x)
            return(rho, phi)
        rho, phi = cart2pol(contour[:, 0], contour[:, 1])
        contour = np.array([phi, rho * 0.5]).T

    contour = (contour*scale)+position[:2]
    return Polygon(contour,
                   closed=True,
                   fill=True,
                   facecolor=color_face,
                   edgecolor=color_edge)

#-------------------------------------------------------------------------------------------

def animate_contour(contours,
                    duration,
                    contours2=None,
                    ax=None,
                    figsize=(6,6),
                    verbose=False):
    '''Create animation of contours

    Example usage:
        ```
        from IPython.display import HTML
        HTML( animate_contour(contours)) )
        ```

    Args:
        contours (3D array):
            Contours tensor in time-first format e.g. (100,48,2) for 100 time points and 48 x,y coordinated
        duration (float):
            Duration of video in seconds.
        contours2 (3D array, default=None):
            Another set of contours to be plotted in red.
        cmap (strings or matplotlib.colormap object):
            Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
            names:
            https://matplotlib.org/examples/color/colormaps_reference.html
        ax (matplotlib.Axes object):
            If provided, plots into this axes.
        figsize (tuple of floats):
            Size of figure.
        verbose (bool):
            Show progress.
    '''
    import matplotlib.pylab as plt
    import matplotlib.animation as animation
    import numpy as np

    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=figsize)
    else:
        fig = plt.gcf()

    nf = len(contours)
    spline = contours[0]
    line, = ax.plot(spline[:, 1], spline[:, 0], 'bo-', lw=1.0, alpha=1.0)
    ax.set_xlim([np.min(contours[:,:,1]), np.max(contours[:,:,1])])
    ax.set_ylim([np.min(contours[:,:,0]), np.max(contours[:,:,0])])

    if contours2 is not None:
        if len(contours2) != len(contours):
            raise ValueError('`contours2` must be of same length as `contours`.')
        spline2 = contours2[0]
        line2, = ax.plot(spline2[:, 1], spline2[:, 0], 'ro-', lw=1.0, alpha=0.25, zorder=0)
        ax.set_xlim([np.min(contours2[:,:,1]), np.max(contours2[:,:,1])])
        ax.set_ylim([np.min(contours2[:,:,0]), np.max(contours2[:,:,0])])

    #print([np.min(contours[:,:,0]), np.max(contours[:,:,0])])
    #print([np.min(contours[:,:,1]), np.max(contours[:,:,1])])
    ax.set_aspect('equal')
    ax.axis('off')

    try:
        from ..config import _is_run_from_ipython
        if verbose:
            if _is_run_from_ipython():
                from tqdm import tqdm_notebook as tqdm
            else:
                from tqdm import tqdm
            nf = tqdm(range(nf))
    except ImportError:
        UserWarning("Module tqdm is not installed. "
                    "You won't have fancy progressbars.")

    def update(t):
        spline = contours[t]
        line.set_data(spline[:, 1], spline[:, 0])
        if contours2 is not None:
            spline2 = contours2[t]
            line2.set_data(spline2[:, 1], spline2[:, 0])
            return line, line2
        return line,


    anim = animation.FuncAnimation(fig,
                                   update,
                                   frames=nf,
                                   interval=((duration*1000.)/len(contours)),
                                   blit=True)

    plt.close()
    return anim


# ----------------------------------------------------------------

