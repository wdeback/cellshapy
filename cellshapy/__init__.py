from .config import config, _is_run_from_ipython
from .data import *
from .utils import *
from .external import *

def quiet_hook(kind, message, traceback): #pragma: no cover
    '''Print simple exception without traceback'''
    print('{0}: {1}'.format(kind.__name__, message))

if not config['print', 'debug']:
    import sys
    sys.excepthook = quiet_hook

    # if we are running on an ipython kernel
    # unfortunately, this does not run well with warnings, if they are raised with joblib.Parallel
    # if _is_run_from_ipython(): #pragma: no cover
    #     # https://stackoverflow.com/a/46224586

    #     def hide_traceback(exc_tuple=None, filename=None, tb_offset=None,
    #                     exception_only=False, running_compiled_code=False):
    #         etype, value, tb = sys.exc_info()
    #         return ipython._showtraceback(etype, value, ipython.InteractiveTB.get_exception_only(etype, value))

    #     ipython = get_ipython()
    #     ipython.showtraceback = hide_traceback

