#! /usr/bin/env python3


def generate_ellipses(xys=None,
                      heights=None,
                      widths=None,
                      angles=None,
                      same_length=True):
    from matplotlib.patches import Ellipse
    import numpy as np

    length = 20
    for param in [xys, heights, widths, angles]:
        if param is not None:
            length = len(param)
            break

    if xys is None:
        xys = [(0, 0)] * length
    if heights is None:
        heights = [50] * length
    if widths is None:
        widths = [50] * length
    if angles is None:
        angles = [0] * length

    for param in [xys, heights, widths, angles]:
        if len(param) != length:
            raise ValueError("Parameter lists have to be of the same length")

    patches = [Ellipse(xy=xy, width=w, height=h, angle=a)
               for xy, w, h, a in zip(xys, widths, heights, angles)]

    paths = []
    for patch in patches:
        t = patch.get_patch_transform()
        p = patch.get_path()
        paths.append(p)

    if same_length:
        lengths = [len(p) for p in paths]
        lcm = np.lcm.reduce(lengths)

    ellipses = []
    for p in paths:
        if same_length:
            p = p.interpolated(lcm)
        p = p.cleaned(transform=t)
        v = p.vertices[:-2]
        ellipses.append(v)

    return np.array(ellipses)


class ShapeGenerator:
    '''Wrapper around a function that generates a shape.'''
    def __init__(self, func):
        self._generating_function = func
        self._kwargs = dict()

    def generate(self):
        '''Generates sequence of polygons'''
        return self._generating_function(**self._kwargs)

    def generateArray(self, size=(100, 100)):
        '''Generates sequence of arrays'''
        import numpy as np
        from skimage.draw import polygon

        shapes = self.generate()
        length = len(shapes)
        c_x, c_y = (size[0]//2, size[1]//2)
        dims = (length, size[0], size[1])
        out = np.zeros(dims, dtype=np.bool)

        for i, s in enumerate(shapes):
            #s = shape
            for idx, point in enumerate(s):
                x, y = point
                s[idx] = [int(x + c_x), int(y + c_y)]

            rr, cc = polygon(*s.T)
            out[i, rr, cc] = True

        return out

    def generateTiffVideo(self, filename, size=(100, 100)):
        '''Generates sequence of arrays, saved as multipage TIFF image'''
        from skimage.external import tifffile
        import numpy as np
        import warnings
        ar = self.generateArray(size)
        ar = ar.astype(np.uint8) * np.iinfo(np.uint8).max

        with warnings.catch_warnings(record=True):
                tifffile.imsave(filename, ar)


class EllipseGenerator(ShapeGenerator):
    def __init__(self, xys=None, heights=None, widths=None, angles=None):
        ShapeGenerator.__init__(self, generate_ellipses)
        self._kwargs = {
            "xys": xys,
            "heights": heights,
            "widths": widths,
            "angles": angles
        }


# Adding a new generator:
#
# def generate_cell(arg1=None, arg2=None):
#     """Do some fancy stuff that generates an array of shapes"""
#     pass
#
#
# class CellGenerator(ShapeGenerator):
#     def __init__(self, arg1=None, arg2=None):
#         ShapeGenerator.__init__(self, generate_cell)
#         self._kwargs = {
#             "arg1": arg1,
#             "arg2": arg2
#         }

def generateShape(shape_fun, n_points=None, **kwargs):
    patch = shape_fun(**kwargs)
    t = patch.get_patch_transform()
    p = patch.get_path()
    if n_points is not None:
        if n_points < len(p):
            raise ValueError("Can only increase the number of points")
        p = p.interpolated(n_points)
    p = p.transformed(t) #.cleaned(transform=t)
    v = p.vertices[:-1]
    return v


def generateClosestVertexMapping(polygon1, polygon2):
    import numpy as np
    cpoly1 = polygon1 - np.mean(polygon1, axis=0)
    cpoly2 = polygon2 - np.mean(polygon2, axis=0)

    l = len(cpoly1)
    min_dist = (0, 0, float("inf"))
    for i, (px, py) in enumerate(cpoly1):
        for j, (qx, qy) in enumerate(cpoly2):
            d = np.sqrt((px - qx)**2 + (py - qy)**2)
            if d < min_dist[2]:
                min_dist = (i, j, d)

    out = np.array([((i + min_dist[0]) % l,
                     (i + min_dist[1]) % l) for i in range(l)])

    return out


def closePolygon(poly):
    import numpy as np
    if (poly[0, :] != poly[-1, :]).any():
        poly = np.vstack((poly, poly[0, :]))
    return poly


def openPolygon(poly):
    if (poly[0, :] == poly[-1, :]).all():
        poly = poly[:-1, :]
    return poly


def expandPolygons(polygon1, polygon2, lcm=1):
    from matplotlib.path import Path
    import numpy as np

    # Close the polygons
    polygon1 = closePolygon(polygon1)
    polygon2 = closePolygon(polygon2)

    polygon1 = Path(polygon1).to_polygons()[0]
    polygon2 = Path(polygon2).to_polygons()[0]

    # Same number of edges for both polygons
    l1, l2 = len(polygon1)-1, len(polygon2)-1 # Number of edges
    lcm = np.lcm.reduce([l1, l2, lcm])
    e1, e2 = (lcm//l1), (lcm//l2)
    p1 = Path(polygon1).interpolated(e1)
    p2 = Path(polygon2).interpolated(e2)
    # print(f"Expanding poly1 from {l1} and poly2 from {l2} to {lcm} vertices.")

    # Remove last point
    p1 = p1.vertices[:-1]
    p2 = p2.vertices[:-1]

    return p1, p2


def morph(shape1, shape2, steps, lcm=1):
    import numpy as np
    # Expand the polygons to have the same amount of vertices
    polygon1, polygon2 = expandPolygons(shape1, shape2, lcm)

    # TODO: Find closest vertex in the other shape
    v_map = generateClosestVertexMapping(polygon1, polygon2)

    # Create array of dimension steps x N x 2
    out = np.zeros((steps, len(polygon1), 2))

    # First and last points are the original polygons
    out[0, :, :] = polygon1[v_map[:, 0]]
    out[-1, :, :] = polygon2[v_map[:, 1]]

    # Linear interpolation between the two shapes
    npts = len(polygon1)
    for v in range(npts):
        out[:, v, 0] = np.linspace(out[0, v, 0], out[-1, v, 0], steps)
        out[:, v, 1] = np.linspace(out[0, v, 1], out[-1, v, 1], steps)

    return out


class MorphGenerator(ShapeGenerator):
    def __init__(self, shape1=None, shape2=None, steps=None):
        self._generating_function = morph
        self._kwargs = {
            "shape1": shape1,
            "shape2": shape2,
            "steps": steps
        }


def morph_chain(shapes=None, steps=None, each_steps=None):
    import numpy as np

    shapes = [np.array(s) for s in shapes]
    shapes = [openPolygon(s) for s in shapes]

    len_shapes = len(shapes)
    ls = np.lcm.reduce([len(s) for s in shapes])
    if ls > 500:
        import warnings
        warnings.warn(f"The LCM of the number of points of the shapes is {ls},"
                      f"this might take a while")

    if len(shapes) < 2:
        raise ValueError("At least two shapes are needed")
    if steps is not None and each_steps is not None:
        raise ValueError("Only one of steps and each_steps may be given.")
    if steps is None and each_steps is None:
        raise ValueError("Either steps or each_steps must be given.")
    if steps is not None and each_steps is None:
        if isinstance(steps, list):
            if len(steps) != len(shapes) - 1:
                raise ValueError("len(steps) != len(shapes) - 1")
            else:
                # steps gives the length of each individual steps
                each_steps = steps
        elif isinstance(steps, int):
            # steps represents the total length of the chain
            each_steps = [int(np.ceil(steps/(len_shapes-1)))] * (len_shapes - 1)
    elif each_steps is not None and steps is None:
        if not isinstance(each_steps, int):
            raise ValueError("each_steps needs to be of type int.")
        else:
            # equidistant steps
            each_steps = [each_steps] * (len_shapes - 1)

    out = np.zeros((np.sum(each_steps), ls, 2))
    curr = 0
    for i in range(len(each_steps)):
        begin = curr
        length_segment = each_steps[i]
        end = begin + length_segment
        out[begin:end, :, :] = morph(shapes[i],
                                     shapes[i+1],
                                     length_segment,
                                     ls)
        curr = end

    return out


class MorphChainGenerator(ShapeGenerator):
    def __init__(self, shapes=None, steps=None, each_steps=None):
        self._generating_function = morph_chain
        self._kwargs = {
            "shapes": shapes,
            "steps": steps,
            "each_steps": each_steps
        }


class Morph():
    def __init__(self, shape):
        self._shapes = []
        self.add_shape(shape)
        self._times = []

    @staticmethod
    def _check_shape(shape):
        import numpy as np
        if not isinstance(shape, np.ndarray):
            raise ValueError("shape must be a numpy array")
        else:
            if len(shape.shape) != 2:
                raise ValueError("shape needs to have two dimensions.")
            if shape.shape[1] != 2:
                raise ValueError("The second dimension of shape needs to be of length 2.")
        return shape

    def add_shape(self, shape):
        shape = Morph._check_shape(shape)
        self._shapes.append(shape)
        return self

    def morph_to(self, shape, frames=50):
        shape = Morph._check_shape(shape)
        self._shapes.append(shape)
        self._times.append(frames)
        return self

    def generate(self):
        mcg = MorphChainGenerator(shapes=self._shapes,
                                  steps=self._times)
        return mcg.generate()

    def generateArray(self, size=(100, 100)):
        mcg = MorphChainGenerator(shapes=self._shapes,
                                  steps=self._times)
        return mcg.generateArray(size)

    def generateTiffVideo(self, filename, size=(100, 100)):
        mcg = MorphChainGenerator(shapes=self._shapes,
                                  steps=self._times)
        return mcg.generateTiffVideo(filename, size)


# Usage Example:
# from datasets.generate_datasets import generateShape, MorphGenerator, MorphChainGenerator
# from matplotlib.patches import Ellipse, RegularPolygon
# import numpy as np

# s1 = generateShape(RegularPolygon, xy=(0, 0), numVertices=3, radius=80)
# s2 = generateShape(RegularPolygon, xy=(0, 0), numVertices=4, radius=80, orientation=np.deg2rad(45))
# s3 = generateShape(Ellipse, xy=(0, 0), height=160, width=50)

# mg = MorphGenerator(shape1=s1, shape2=s3, steps=50)
# mg.generateTiffVideo("/Users/sebastian/Desktop/tri_to_elipse.tiff",
#                      size=(200, 200))

# mcg = MorphChainGenerator(shapes=[s1, s3, s2], each_steps=50)
# mcg.generateTiffVideo("/Users/sebastian/Desktop/morph_chain.tiff",
#                      size=(200, 200))


