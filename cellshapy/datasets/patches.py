from matplotlib.patches import Polygon
from matplotlib.path import Path


class Star(Polygon):
    @staticmethod
    def _point_on_circle(xy, rad, ang):
        import numpy as np
        p = (xy[0] + rad * np.cos(ang * np.pi/180.0),
             xy[1] + rad * np.sin(ang * np.pi/180.0))
        return p

    def __init__(self, xy=(0, 0), numSpikes=5, inner=20, outer=40):
        import numpy as np

        if numSpikes < 1:
            raise ValueError("At lest one spike required")

        ang = 360.0/numSpikes

        outer_p = [Star._point_on_circle(xy, outer, a) \
            for a in np.linspace(0, 360, num=numSpikes+1, endpoint=True)]
        inner_p = [Star._point_on_circle(xy, inner, a + ang/2.0) \
            for a in np.linspace(0, 360, num=numSpikes, endpoint=False)]

        ps = np.zeros(shape=(len(inner_p) + len(outer_p), 2))
        import itertools
        idx = 0
        # TODO: rewrite nicer?
        for i, o in itertools.zip_longest(inner_p, outer_p):
            ps[idx, :] = o
            idx += 1
            if (i is not None):
                ps[idx, :] = i
                idx += 1

        ps[-1, :] = ps[0, :]

        Polygon.__init__(self, xy=ps)
        self.set_closed(True)


