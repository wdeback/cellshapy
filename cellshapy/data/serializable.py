import pickle


class Serializable:
    max_bytes = 2**31 - 1

    def save(self, filename):
        with open(filename, "wb") as f:
            bytes_out = pickle.dumps(self, protocol=4)
            for idx in range(0, len(bytes_out), Serializable.max_bytes):
                f.write(bytes_out[idx:idx + Serializable.max_bytes])

    @staticmethod
    def load(filename):
        with open(filename, "rb") as f:
            import os
            input_size = os.path.getsize(filename)
            bytes_in = bytearray(0)
            for _ in range(0, input_size, Serializable.max_bytes):
                bytes_in += f.read(Serializable.max_bytes)
            return pickle.loads(bytes_in)
