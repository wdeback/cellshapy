class Annotatable:
    ''' An interface to allow classes to be annotateable.

    Basically provides a dict in which annotations about the object can be
    stored.
    '''

    def __init__(self):
        self.annotations = dict()

    def updateAnnotation(self, annotation):
        ''' Updates or adds the annotation(s) to the annotations of the
        Annotatable.

        Args:
            annotation (dict):
                The annotations to add.
        '''
        if not isinstance(annotation, dict):
            raise ValueError("The supplied annotation must be of type dict.")
        self.annotations = {**self.annotations, **annotation}

    def hasAnnotation(self, key):
        ''' Checks whether an annotation is present in an Annotatable.

        Args:
            key (str):
                The key that should be checked.

        Returns:
            bool:
                True, if the key is present in the Annotatable, False
                otherwise.
        '''
        return key in self.annotations

    def removeAnnotation(self, key):
        ''' Removes an annotation from an Annotatable.

        Args:
            key (str):
                The key of the annotation that should be removed.

        Returns:
            object:
                The value that was associated with the provided key.
        '''
        return self.annotations.pop(key)

    def clearAnnotations(self):
        ''' Removes all annotations from an Annotatable.
        '''
        self.annotations = {}

    def selectAnnotation(self, pred):
        ''' Recursively select Annotatables by a predicate.

        Args:
            pred:
                The predicate that selects, whether an Annotatable should be
                selected or not. This is a function and should have the
                following signature::
                  def func(Annotatable) -> bool

        Returns:
            Annotatable:
                An annotateable where all instances if Annotatables that don't
                match the predicate are filtered out.

        Note:
            You will receive a copy of the original object.
        '''
        import copy

        keepThis = pred(self)

        cpy = copy.deepcopy(self)
        for key in cpy.__dict__:
            o = cpy.__dict__[key]
            if isinstance(o, Annotatable):
                o = o.selectAnnotation(pred)
                keepThis |= cpy.__dict__[key] is not None
            elif isinstance(o, list) and \
                    all(isinstance(n, Annotatable) for n in o):
                o = [li.selectAnnotation(pred) for li in o]
                o = [li for li in o if li is not None]
                keepThis |= (len(o) > 0)
            cpy.__dict__[key] = o

        return cpy if keepThis else None
