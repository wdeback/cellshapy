#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from . import Video, Annotatable, Serializable
from ..config import config
import warnings


class VideoCollection(Annotatable, Serializable):
    '''A VideoCollection consists of one or multiple Videos.

    Attributes:
        videos (list): The videos contained in this video collection.
    '''
    def __init__(self):
        Annotatable.__init__(self)
        Serializable.__init__(self)
        self.videos = []
        self.aligned_contours = False
        self.tensor = dict()
        self.embedding = dict()
        self.embed_object = dict()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __getitem__(self, index):
        return self.videos[index]

    def __len__(self):
        return len(self.videos)

    def __str__(self):
        p_lim = config['print', 'limit.videocollection']
        out = f"VideoCollection: "
        out += f"{len(self)} videos; "
        out += f"aligned_contours: {self.aligned_contours is not None}\n"
        m = min(len(self), p_lim)
        out += "\n".join([f"  {l}"
                          for i in self[:m]
                          for l in i.__str__().split("\n")
                          ])
        out += f"  ..." if len(self) > p_lim else ""
        return out

    @staticmethod
    def from_glob(globExp, composite=False, annotations=None, min_size=None, normalize_area=None, n_jobs=-1):
        '''Creates a VideoCollection from a glob.

        Args:
            globExp (str):
                A glob that is expanded. The files or directories that the
                expanded glob points to are then used to create videos.
            composite (bool):
                Whether the glob points to composite (tiff) images.
            annotations (list of dict):
                Optional dictionaries with annotations associated with arrays. Must be equal length as list of arrays.
            min_size (int):
                The minimum size of objects in the images to not be removed.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!
            n_jobs (int):
                Frames of Videos in this VideoCollection are created in
                parallel. This argument controls how many jobs run
                concurrently.
        Returns:
            VideoCollection:
                A newly created VideoCollection with Videos that are created
                from the expanded glob.
        '''
        import os
        from glob import glob
        from ..utils import sort_human
        paths = sort_human(glob(globExp, recursive=True))

        print(f"Creating VideoCollection from glob expression {globExp}: "
              f"{len(paths)} video files or directories")

        return VideoCollection.from_filenames(paths, composite=composite,
                                annotations=annotations,
                                min_size=min_size,
                                normalize_area=normalize_area,
                                n_jobs=n_jobs)


    @staticmethod
    def from_filenames(paths, composite=False, annotations=None, min_size=None, normalize_area=None, n_jobs=-1):
        '''Creates a VideoCollection from a glob.

        Args:
            path (list of str):
                A list of paths/filenames.
            composite (bool):
                Whether the glob points to composite images.
            annotations (list of dict):
                Optional dictionaries with annotations associated with arrays. Must be equal length as list of arrays.
            min_size (int):
                The minimum size of objects in the images to not be removed.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!!
            n_jobs (int):
                Frames of Videos in this VideoCollection are created in
                parallel. This argument controls how many jobs run
                concurrently.
        Returns:
            VideoCollection:
                A newly created VideoCollection with Videos that are created
                from the expanded glob.
        '''
        import os

        print(f"Creating VideoCollection from list of paths. "
              f"{len(paths)} video files or directories")
        vc = VideoCollection()

        from ..config import tqdm as tqdm

        if annotations is not None:
            if len(annotations) != len(paths):
                raise ValueError('List of annotations must be of equal length as number of paths')

        for i, path in enumerate(tqdm(paths)):
            if os.path.isfile(path):
                if composite:
                    video = Video.from_composite(path, video_index=i,
                                             annotations=annotations[i] if annotations is not None else None,
                                             min_size=min_size,
                                             normalize_area=normalize_area,
                                             n_jobs=n_jobs)
                else:
                    raise UserWarning(f"Glob matches single file {path} but"
                                      " composite is not specified")
            elif os.path.isdir(path):
                video = Video.from_dir(path, video_index=i,
                                        annotations=annotations[i] if annotations is not None else None,
                                        min_size=min_size,
                                        normalize_area=normalize_area,
                                        n_jobs=n_jobs)

            vc.videos.append(video)

        # align all contours
        # print('\nAligning contours')
        # vc.align_contours(plot=False)

        return vc

# ----------------------------------------------------------------

    @staticmethod
    def from_arrays(list_of_arrays, annotations=None, min_size=None, normalize_area=None, n_jobs=-1):
        '''Creates a VideoCollection from list of numpy arrays.

        Args:
            list_of_arrays (list of array):
                A list of numpy arrays wth binary data where each array is (time x height x width).
            annotations (list of dict):
                Optional dictionaries with annotations associated with arrays. Must be equal length as list of arrays.
            min_size (int):
                The minimum size of objects in the images to not be removed.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!!
            n_jobs (int):
                Frames of Videos in this VideoCollection are created in
                parallel. This argument controls how many jobs run
                concurrently.
        Returns:
            VideoCollection:
                A newly created VideoCollection with Videos that are created
                from the expanded glob.
        '''
        import os
        print(f"Creating VideoCollection from list of {len(list_of_arrays)} arrays."
              f" video files or directories")
        vc = VideoCollection()

        if annotations is not None:
            if len(annotations) != len(list_of_arrays):
                raise ValueError('List of annotations must be of equal length as list of arrays')

        from ..config import tqdm as tqdm

        for i, array in enumerate(tqdm(list_of_arrays)):
            video = Video.from_array(array, video_index=i,
                                        annotations=annotations[i] if annotations is not None else None,
                                        min_size=min_size,
                                        normalize_area=normalize_area,
                                        n_jobs=n_jobs)
            vc.videos.append(video)

        # align all contours
        # print('\nAligning contours')
        # vc.align_contours(plot=False)

        return vc

# ----------------------------------------------------------------

    @staticmethod
    def from_contours(contours, composite=False, annotations=None, min_size=None, normalize_area=None, n_jobs=-1):
        '''Creates a VideoCollection from a list of contours.

        Args:
            contours (array or list of arrays):
                Array with contours of size (samples, time, num_contour_elements, 2).
                Of list of length samples with arrays of size (time, num_contour_elements, 2).
            composite (bool):
                Whether the glob points to composite images.
            annotations (list of dict):
                Optional dictionaries with annotations associated with arrays. Must be equal length as list of arrays.
            min_size (int):
                The minimum size of objects in the images to not be removed.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!!
            n_jobs (int):
                Frames of Videos in this VideoCollection are created in
                parallel. This argument controls how many jobs run
                concurrently.
        Returns:
            VideoCollection:
                A newly created VideoCollection with Videos that are created
                from the expanded glob.
        '''
        import os

        vc = VideoCollection()

        if annotations is not None:
            if len(annotations) != len(contours):
                raise ValueError(f'List of annotations ({len(annotations)}) must be of equal length as contours ({len(contours)}).')

        from ..config import tqdm as tqdm

        for i, contour in enumerate(tqdm(contours)):
            video = Video.from_contour(contour, video_index=i,
                                        annotations=annotations[i] if annotations is not None else None,
                                        min_size=min_size,
                                        normalize_area=normalize_area,
                                        n_jobs=n_jobs)
            vc.videos.append(video)

        # align all contours
        # print('\nAligning contours')
        # vc.align_contours(plot=False)

        return vc

# ----------------------------------------------------------------

    def get_annotations_frames(self, key=None):
        '''Returns the annotations per frame in a VideoCollection.

        Args:
            key (str, optional):
                if provided, return ndarray with specific annotation
        Returns:
            array or DataFrame:
                if key provided, returns array, else DataFrame with all annotations
        '''

        if key is not None:
            return np.array([f.annotations[key]
                            for video in self.videos
                            for f in video.frames])
        else:
            import pandas as pd
            return pd.DataFrame([f.annotations
                                for video in self.videos
                                for f in video.frames])

    def get_annotations_videos(self, key=None):
        '''Returns the annotations per video in a VideoCollection.

        Args:
            key (str, optional):
                if provided, return ndarray with specific annotation
        Returns:
            array or DataFrame:
                if key provided, returns array, else DataFrame with all annotations
        '''

        if key is not None:
            return np.array([v.annotations[key]
                            for v in self.videos])
        else:
            import pandas as pd
            return pd.DataFrame([v.annotations
                                for v in self.videos])

    def get_contours(self, per_sample=False, aligned=False):
        '''Returns all the contours in a VideoCollection as array'''
        if aligned:
            return self.get_aligned_contours(per_sample=per_sample)
        else:
            if per_sample: # separated by video
                all_contours = []
                for video in self.videos:
                    contours = []
                    for f in video.frames:
                        contours.append( f.contour )
                    all_contours.append( np.array(contours) )
                return np.array(all_contours)
            return np.array([f.contour
                            for video in self.videos
                            for f in video.frames])

    # WdB: TODO: How to check whether 'contour_aligned' or 'embeddings' exist?

    def get_aligned_contours(self, per_sample=False):
        '''Returns all the aligned contours in a VideoCollection as array.'''
        if per_sample: # separated by video
            all_contours = []
            for video in self.videos:
                contours = []
                for f in video.frames:
                    contours.append( f.get_contour_aligned() )
                all_contours.append( np.array(contours) )
            return np.array(all_contours)
        else: # all together
            return np.array([f.get_contour_aligned()
                            for video in self.videos
                            for f in video.frames])

    def get_embeddings(self, source, method):
        '''Returns all the embeddings in a VideoCollection.

        Args:
            method (str):
                As in VideoCollection.embed().
            source (str):
                As in VideoCollection.embed().
        '''
        import pandas as pd
        if not all([(source, method) in video.embedding.keys() for video in self]) \
        and not all([(source, method) in f.embedding.keys() for video in self for f in video]):
            raise UserWarning(f"Embeddings for {(source, method)} have not "
                              f"yet been calculated")
        if all([(source, method) in video.embedding.keys() for video in self]):
            return pd.DataFrame([video.get_embedding(source, method) for video in self])
        else:
            return pd.DataFrame([f.get_embedding(source, method)
                                for video in self
                                for f in video])

# ----------------------------------------------------------------

    def get_curvatures(self, signed=True):
        '''Return all (un)signed curvatures.

        Args:
            signed (bool):
                If true, return signed curvature (negative for concave)
        Returns:
            tensor (array):
                Tensor with curvature in 'samples_time_curvature' format
        '''

        return np.array([v.get_curvatures(signed=signed)
                        for v in self])

# ----------------------------------------------------------------

    def get_descriptors(self, video=None, add_videos_frame=False, filter_numeric=True, include=None, exclude=None):
        '''Get all region-based features in videocollection as a pandas
        DataFrame.

        Args:
            video (Video):
                If specified, in
            filter_numeric (bool):
                Whether to filter for only numeric data types in the resulting
                DataFrame.
            include (list of str):
                Names of descriptors to include
            exclude (list of str):
                Names of descriptors to exclude

        Returns:
            pandas.DataFrame:
                DataFrame with region-based features and video and frame ID.
        '''
        features = []
        if video is not None:
            videos = [video]
        else:
            videos = self.videos
        for i, video in enumerate(videos):
            for j, f in enumerate(video.frames):
                feat = f.features
                # filter descriptors, based no user-specified include and exclude list
                if include is not None:
                    feat = dict((k, v) for (k, v) in feat.items() if k in include)
                if exclude is not None:
                    feat = dict((k, v) for (k, v) in feat.items() if k not in exclude)
                if add_videos_frame:
                    feat['video'] = i
                    feat['frame'] = j
                features.append(feat)

        import pandas as pd
        df = pd.DataFrame(features)
        if not add_videos_frame:
            df = df.drop(['frame', 'video'], axis='columns', errors='ignore')
        if filter_numeric:
            df = df.select_dtypes(include='number')

        return df

# ----------------------------------------------------------------

    def align_contours(self,
                        tolerance=1e-4,
                        max_iterations=100,
                        show_samples=10):
        '''Align all contours in a VideoCollection.

        Args:
            show_samples (None or int):
                If int, show a number of samples before and after alignment.
            tolerance (float):
                The minimal tolerated error in terms of Procrustes distance.
            max_iterations (int):
                The maximum number of iterations to perform.
        '''
        import time
        from ..utils.contour_alignment import align_contours

        # get all contours in videocollection
        all_contours = self.get_contours()

        # align contours and measure time
        tic = time.time()
        all_contours_aligned = align_contours(all_contours, tolerance=tolerance, max_iterations=max_iterations)

        # assign the aligned contours back to frames
        i = 0
        for video in self.videos:
            for f in video.frames:
                f.contour_aligned = all_contours_aligned[i]
                i += 1

        toc = time.time()

        tdiff = toc - tic
        clen = len(all_contours_aligned)

        print(f"\n{clen} contours aligned in {tdiff:.2f} sec")

        self.aligned_contours = True

        # plot before/after samples
        if show_samples is not None and isinstance(show_samples, int):
            from ..plotting import _show_aligned_samples
            return _show_aligned_samples(all_contours,
                                         all_contours_aligned,
                                         show_samples)


    def reference_align_contours(self,
                                 references=0,
                                 n_jobs=-1,
                                 plot=False):
        '''Aligns contours in a more sensible way than align_contours.

        First, the contours are centered around the origin. Then a reference
        outline is selected from each video. The reference outlines are aligned
        (rotation and shift) among each other. Then the rotation that was
        applied to the reference outline is also applied to the rest of the
        video. In the end, the contour points are shifted in a way that the
        first point in the reference outline is as close as possible to the
        first point in the remaining frames.

        Args:
            references (int, list of contours, or function):
                The reference contours. If this is an integer, it represents
                the frame in the video to use. If this is given as a list of
                contours, those are directly used. If this is a function, it is
                applied to the videos to retrieve the reference frames (has to
                return a list of contours).

            n_jobs (int):
                The number of parallel jobs.
        '''
        import time
        import warnings
        from ..utils.contour_alignment import reference_align_contours

        if np.all([[f.contour_aligned is not None for f in v] for v in self]):
            warnings.warn("Contours are already aligned, realigning now.")

        vs = [[f.contour for f in v] for v in self.videos]
        vs_fixed = reference_align_contours(vs, references, n_jobs)

        for v_i, v in enumerate(self.videos):
            for f_i, f in enumerate(v):
                self[v_i][f_i].contour_aligned = vs_fixed[v_i][f_i]

        if plot:
            from cellshapy.plotting import _show_aligned_samples
            vid_id = 0
            vid_samples = 10
            ref_id = 0

            # Show alignment plot for the first 10 contours of the first video
            _ = _show_aligned_samples([f.contour for f_id, f in enumerate(self[vid_id]) if f_id < vid_samples],
                    [f.contour_aligned for f_id, f in enumerate(self[vid_id]) if f_id < vid_samples],
                    vid_samples)

            # Show alignment for the first contour from all videos
            _ = _show_aligned_samples(
                    [v[ref_id].contour for v in self],
                    [v[ref_id].contour_aligned for v in self],
                    len(self))


    def rotate_shapes(self, degrees):
        '''Rotate the shapes for all frames by the same amount. Only for visual purposes.

        Args:
            - degrees (float): degrees to rotate shapes in counter-clockwise (CCW) direction.
        '''

        def rotate(points, degrees):
            '''Rotate 2D points around origin in CCW direction.'''
            theta = np.deg2rad(degrees)
            c, s = np.cos(theta), np.sin(theta)
            R = np.matrix([[c, -s], [s, c]]) # rotation matrix
            p_rot = np.dot(points, R.T)
            return np.array(p_rot)

        def rotate_contours_video(video, degrees):
            for f in video.frames:
                f.contour = rotate(f.contour, degrees)
                if self.aligned_contours:
                    f.contour_aligned = rotate(f.contour_aligned, degrees)

        from ..config import tqdm
        [rotate_contours_video(v, degrees) for v in tqdm(self.videos)]

        #from joblib import Parallel, delayed
        #Parallel(n_jobs=-1)(delayed(rotate_contours_video)(v, degrees) for v in tqdm(self.videos))




# ----------------------------------------------------------------

    def embed_shapes(self,
                     source,
                     method='pca',
                     labels=None,
                     n_components=2,
                     include=None,
                     exclude=None,
                     verbose=True):
        '''Create embedding for frames using PCA, t-SNE, or UMAP.

        Args:
            source (str):
                One of ['descriptors', 'contours'].
            method (str):
                One of ['pca', 'tsne', 'umap', 'lda', 'parafac'].
            labels (list of ints):
                If method=umap, use labels for supervised dimensionality reduction (default=None).
            n_components (int):
                Number of components in embedding vector.
            include (list of str):
                List of names of descriptors to include (only used when source = 'descriptors')
            exclude (list of str):
                List of names of descriptors to exclude (only used when source = 'descriptors')
            verbose (bool):
                If true, print execution time
        '''
        from ..analysis import embedding
        import time

        sources = ['descriptors', 'contours']
        methods = ['pca', 'tsne', 'umap', 'lda', 'parafac']

        if source == 'all':
            for source in sources:
                self.embed_shapes(source=source, labels=labels, method=method, n_components=n_components)
            return
        if method == 'all':
            for method in methods:
                self.embed_shapes(source=source, labels=labels, method=method, n_components=n_components)
            return

        if source not in sources:
            raise ValueError(f'source argument must be one of {sources}.')
        if method not in methods:
            raise ValueError(f'method argument must be one of {methods}.')

        if verbose:
            print('`{}` embedding for `{}`'.format(method, source), end='')
            tic = time.time()

        # get and preprocess data ###
        if source == 'descriptors':
            # get all features as a dataframe
            data = self.get_descriptors(include=include, exclude=exclude)
            # remove columns: video and frame
            data = data.drop(['video', 'frame'], axis=1, errors='ignore').fillna(0.0)
            # standardization
            data_standard = (data - data.mean(axis=0)) / (data.std(axis=0)+1e-6)

            # reset the columns that should not be standardized, only zo zero-centering
            standardization = self.videos[0].frames[0].features_standardization

            if include is not None:
                standardization = dict((k, v) for (k, v) in standardization.items() if k in include)
            if exclude is not None:
                standardization = dict((k, v) for (k, v) in standardization.items() if k not in exclude)

            for key, val in standardization.items():
                if standardization[key] == False:
                    data_standard[key] = data[key] - data[key].mean(axis=0)
                    if verbose:
                        print('reset `{}` to mean-subtracted, not standardized'.format(key))
            data = data_standard


        elif source == 'contours':
            # get all aligned contours
            data = self.get_aligned_contours()
            # reshape data as 1D vector per sample
            numframes = data.shape[0]
            numpoints = data.shape[1]
            data = data.reshape(numframes, numpoints*2)
        else:
            warnings.warn(f'source must be one of {sources}')

        # check whether labels are
        supervised_methods=['umap', 'lda']
        if method not in supervised_methods and labels is not None:
            warnings.warn(f"Ignoring `labels` argument: `labels` can only be used with {supervised_methods}, for supervised dimensionality reduction.")
        # check correct length of labels:
        elif method in supervised_methods and labels is not None:
            if len(labels) != len(data):
                raise ValueError(f"Length of `labels` ({len(labels)}) does not correspond to number of data points ({len(data)}).")


        # perform embedding ###
        embed_object = None
        if method == 'pca':
            output, embed_object = embedding.pca(data, n_components=n_components, return_object=True)
        elif method == 'tsne':
            output, embed_object = embedding.tsne(data, n_components=n_components, return_object=True)
        elif method == 'parafac':
            output = embedding.parafac(data, n_components=n_components)[0]
            output -= output.mean(axis=0)
        elif method == 'umap':
            output, embed_object = embedding.umap(data, labels=labels, n_components=n_components, return_object=True)
        elif method == 'lda':
            output, embed_object = embedding.lda(data, labels=labels, n_components=n_components, return_object=True)
        else:
            UserWarning(f'method must be one of {methods}')

        # assign embedding and object to video collection
        self.embedding[(source, method)] = output
        if embed_object is not None:
            self.embed_object[(source, method)] = embed_object

        # assign the embedding vector back to frames ###
        i = 0
        for video in self:
            for f in video:
                f.embedding[(source, method)] = output[i]
                i += 1

        if verbose:
            toc = time.time()
            print(' done ({:.2f} s).'.format((toc-tic)))

# ----------------------------------------------------------------

    def embed_morphs(self,
                     format,
                     method='parafac',
                     n_components=2,
                     save_embedding=True,
                     include=None,
                     exclude=None,
                     plot=False,
                     verbose=True):
        '''Create embedding for videos using tensor decomposition,
        specifically parallel factorization (= canonical decomposition).

        Args:
            format (str):
                One of ['samples_time_descriptors' or 'std',
                        'samples_time_xy' or 'stxy',
                        'samples_time_pairwisedistance' or 'stp',
                        'samples_time_elliptic_fourier' or 'stef'].
            method (str):
                One of ['parafac', 'tucker'].
            n_components (int):
                Number of components in embedding vector.
            save_embedding (bool):
                If True, save embedding to videocollection and videos.
                If False, return embedding.
            include (list of str):
                List of names of descriptors to include (only used when format='std')
            exclude (list of str):
                List of names of descriptors to exclude (only used when formar='std')
            verbose (bool):
                If true, print execution time
        Returns:
            embedding (list of arrays):
                If `save_embedding` is False, return output of embedding `method`
        '''
        from ..analysis import embedding
        import time

        formats_dict = {'std' :'samples_time_descriptors',
                        'stxy':'samples_time_xy',
                        'stp' :'samples_time_pairwisedistance',
                        'stc' :'samples_time_curvature',
                        'stm' :'samples_time_mask',
                        'stef':'samples_time_elliptic_fourier'}

        formats = ['samples_time_descriptors', 'std',
                   'samples_time_xy', 'stxy',
                   'samples_time_pairwisedistance', 'stp',
                   'samples_time_curvature', 'stc',
                   'samples_time_mask', 'stm',
                   'samples_time_elliptic_fourier', 'stef']

        if format not in formats:
            raise ValueError(f'format argument must be one of [{formats}]')

        available_methods = ['parafac', 'parafac2', 'tucker', 'vae']
        if method not in available_methods:
            raise ValueError(f'method argument must be one of {available_methods}')

        if verbose:
            print('`{}` embedding for `{}`'.format(method, format), end='')
            tic = time.time()

        # compute tensor in correct format, if not already available
        if format in self.tensor:
            tensor = self.tensor[format]
        else:
            # compute tensor in format
            tensor = embedding.videos_to_tensor(self, format, include=include, exclude=exclude)
            # save tensor in videocollection
            self.tensor[format] = tensor
            # save tensor slices to videos
            #for video, tensor_slice in zip(self.videos, tensor):
            #    video.tensor_slice[format] = tensor_slice

        #print(tensor.shape)
        #for t in tensor:
        #    print(t.shape)

        # perform tensor decomposition
        embed_object = None
        if method == 'parafac':
            output = embedding.parafac(tensor, n_components)
        if method == 'parafac2':
            warnings.warn('Parafac2 is experimental! Limited compatibility.')
            output = embedding.parafac2(tensor, n_components)
        elif method == 'tucker':
            output = embedding.tucker(tensor, n_components)
        elif method == 'vae':
            warnings.warn('VAE is experimental! Limited compatibility.')
            output, embed_object = embedding.vae(tensor, n_components, verbose=verbose, return_object=True)
        
        # save embed_object
        if embed_object is not None:
            self.embed_object[(format, method)] = embed_object

        # save embedding to video collection
        if save_embedding:
            self.embedding[(format, method)] = output

            # assign the embedding vector back to videos ###
            for i, video in enumerate(self.videos):
                if method == 'parafac':
                    video.embedding[(format, method)] = output[0][i]
                if method == 'tucker':
                    #print(output[i])
                    video.embedding[(format, method)] = output[0][0][i]
                if method == 'vae':
                    video.embedding[(format, method)] = output[i]

        #print(output.shape)

        if plot:
            if method in ['parafac', 'parafac2', 'vae']:
                factors = output
            elif method == 'tucker':
                factors, core = output
            # get titles
            if format in formats_dict:
                format = formats_dict[format]
            titles = [t.capitalize() for t in format.split('_')]
            # plot factors
            from ..plotting.morphspace import plot_factors
            plot_factors(factors, titles=titles)

        if verbose:
            toc = time.time()
            print(' done ({:.2f} s).'.format((toc-tic)))

        if not save_embedding:
            return output

#----------------------------------------------------------------

    def to_tensor(self, format):
        '''Get tensor in specific format. Returns tensor and stores it internally.

        Args:
            format (str): One of 'stxy', 'std', 'stp', ...

        Returns:
            tensor (array)
        '''

        from ..analysis import embedding
        import time

        formats_dict = {'std' :'samples_time_descriptors',
                        'stxy':'samples_time_xy',
                        'stp' :'samples_time_pairwisedistance',
                        'stc' :'samples_time_curvature',
                        'stm' :'samples_time_mask',
                        'stef':'samples_time_elliptic_fourier'}

        formats = ['samples_time_descriptors', 'std',
                   'samples_time_xy', 'stxy',
                   'samples_time_pairwisedistance', 'stp',
                   'samples_time_curvature', 'stc',
                   'samples_time_mask', 'stm',
                   'samples_time_elliptic_fourier', 'stef']

        if format not in formats:
            raise ValueError(f'format argument must be one of [{formats}]')

        tensor = embedding.videos_to_tensor(self, format)
        # save tensor in videocollection
        self.tensor[format] = tensor

        return tensor

# ----------------------------------------------------------------

    def shape_space(self,
                    source,
                    method,
                    components=[1,2],
                    projection='pca',
                    show_shapes=True,
                    show_trajectories=False,
                    colorcode = 'video',
                    shape_size=1.0,
                    alpha_edge=0.5,
                    alpha_face=0.2,
                    figsize=(10,10)):
        '''Plot the embedding of a VideoCollection in shape space.

        Args:
            source (str):
                As in VideoCollection.embed_frames().
            method (str):
                As in VideoCollection.embed_frames().
            components (list of ints):
                Components to plot. Default is [1,2] for 1st and 2nd component.
            projection (str):
                If case embedding > 2 factors, perform 2D projection using ['pca', 'umap' or 'tsne']
            show_shapes (bool):
                Whether to show shapes in the embedding.
            show_trajectories (bool):
                Whether to show trajectories in the embedding.
            shape_size (float):
                Size of the shapes.
            colorcode (str):
                Name of annotation to use for colorcoding of shapes.
            alpha_edge (float):
                Opacity of shape edges.
            alpha_face (float):
                Opacity of shape faces.
        '''

        positions = self.get_embeddings(source, method)
        contours = self.get_aligned_contours()
        if colorcode in self.get_descriptors().columns:
            colors = self.get_descriptors()[colorcode]
        else:
            colors = self.get_annotations_frames(key=colorcode)

        for comp in components:
            if comp > positions.shape[-1]:
                raise ValueError(f"Component {comp} is not available. Embedding for `{source}` and `{method}` contains was calculated for {positions.shape[-1]} components.")

        from ..plotting.shapespace import plot_embedding

        title = '{} {}'.format(method, source)

        fig = plot_embedding(positions,
                        title=title,
                        method=method,
                        components=components,
                        projection=projection,
                        show_shapes=show_shapes,
                        show_trajectories=show_trajectories,
                        colors=colors,
                        contours=contours,
                        shape_size=shape_size,
                        alpha_edge=alpha_edge,
                        alpha_face=alpha_face,
                        figsize=figsize)
        #else:
        #    plot_embedding(embedding,
        #                   title,
        #                   show_shapes=False)

        return fig

# ----------------------------------------------------------------

    def shape_space_reconstruction(self,
                    source,
                    method,
                    num_points,
                    components=[1,2],
                    sigma=2.0,
                    colorcode=None,
                    shape_size=1.0,
                    alpha_edge=0.5,
                    alpha_face=0.2,
                    return_contours=False):
        '''Plot the embedding of a VideoCollection in shape space.

        Args:
            source (str):
                As in VideoCollection.embed().
            method (str):
                As in VideoCollection.embed().
            num_points (int or list or ints):
                Number of reconstructed points per dimension/factor/component.
                If int, same number of points for each dimension.
                If list, number of points can be specified per dimension. In this case, length must be equal to `num_dimensions`.
            sigma (float, default=2.0):
                Number of standard deviations used to determine range of reconstruction points.
            colorcode (str):
                Shape descriptor or annotation to use for color coding, default=None.
            shape_size (float):
                Size of the shapes.
            alpha_edge (float):
                Opacity of shape edges.
            alpha_face (float):
                Opacity of shape faces.
            return_contours (bool):
                If true, reconstructed contours and their coordinates are returned.
        '''

        if source != 'contours':
            raise ValueError(f"Reconstruction cannot be made for `source={source}`. Please choose 'contours' instead.")

        available_methods = ['pca', 'umap']
        if method not in available_methods:
            raise ValueError(f"Reconstruction cannot be made for `method={method}`. Please choose one of {available_methods} instead.")

        from ..analysis.embedding import reconstruct_shapes
        contours, positions = reconstruct_shapes(self, source=source, method=method,
                                 components=components,
                                 num_points=num_points,
                                 sigma=sigma,
                                 probe_along_axes=False)

        if colorcode is None:
            colors = np.array(list(range(len(contours))))
        else:
            # get shape descriptors for reconstructed shapes
            from ..utils.contours import extract_shape_descriptors
            import pandas as pd
            df_descriptors = pd.DataFrame([extract_shape_descriptors(c, return_standardization=False) for c in contours])
            try:
                colors = df_descriptors[colorcode]
            except:
                raise ValueError(f"No such shape descriptor: '{colorcode}'")


        from ..plotting.shapespace import plot_embedding

        if colorcode is None:
            title = f"{method} {source} reconstruction"
        else:
            title = f"{method} {source} reconstruction\n(color = {colorcode})"

        fig = plot_embedding(positions,
                        title,
                        show_shapes=True,
                        show_trajectories=False,
                        method=method,
                        colors=colors,
                        contours=contours,
                        shape_size=shape_size,
                        alpha_edge=alpha_edge,
                        alpha_face=alpha_face,
                        axis_labels=[f"{method.upper()} {components[0]}", f"{method.upper()} {components[1]}"])

        if return_contours:
            return fig, contours, positions
        else:
            return fig

# ----------------------------------------------------------------

    def shape_space_reconstruction_descriptors(self, source,
                                                     method,
                                                     num_points,
                                                     sigma=2.0,
                                                     num_dimensions=None,
                                                     shape_size=1.0,
                                                     alpha_edge=0.5,
                                                     alpha_face=0.2,
                                                     figsize=(20,20)):
        import numpy as np
        import matplotlib.pylab as plt
        from ..config import tqdm as tqdm

        # reconstruct shapes by sampling shape space
        fig, recon_shapes, recon_pos = self.shape_space_reconstruction(source=source,
                                                                     method=method,
                                                                     num_points=num_points,
                                                                     sigma=sigma,
                                                                     num_dimensions=num_dimensions,
                                                                     shape_size=shape_size,
                                                                     alpha_edge=alpha_edge,
                                                                     alpha_face=alpha_face,
                                                                     return_contours=True)

        # create new video collection from reconstructed shapes
        recon_morphs = np.expand_dims(recon_shapes, 1) # add empty time dimension
        annotations = [{'pos':pos} for pos in recon_pos] # store positions as annotation
        recon = VideoCollection.from_contours(recon_morphs, annotations=annotations) # creating video collection (with feature extraction)
        recon.align_contours(show_samples=None) # align contours

        # embed contours using PCA
        recon.embed_shapes('contours', 'pca', n_components=2)

        # get annotations and shape descriptors
        import pandas as pd
        df = pd.concat([recon.get_annotations_videos(), recon.get_descriptors()], axis=1)

        # define a subplot figure
        n_columns = len(df.columns)-3
        nr = int(np.ceil(np.sqrt(n_columns)))
        nc = nr
        fig, axes = plt.subplots(nrows=nr, ncols=nc, figsize=figsize,
                                 sharex=True, sharey=True, squeeze=False)
        axes = axes.flatten()

        # render contour plots
        count_ax = 0
        for col in tqdm(df.columns):

            if col in ['pos', 'frame', 'video']:
                continue

            ax = axes[count_ax]
            count_ax += 1

            data = np.array([[row['pos'][0], row['pos'][1], row[col]]
                              for index, row in df.iterrows()])

            from scipy.interpolate import griddata
            x = data[:,0]
            y = data[:,1]
            z = data[:,2]
            xi = np.linspace(x.min(),x.max(),len(x)*10)
            yi = np.linspace(y.min(),y.max(),len(y)*10)
            zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')

            ax.contourf(xi,yi,zi,100)
            cs = ax.contour(xi,yi,zi,levels=3,colors='black')
            ax.clabel(cs, cs.levels, inline=True, fmt='%.3f', fontsize=10)

            #ax.scatter(data[:,0], data[:,1], c=data[:,2])
            ax.set_title(col)

        # remove subplots
        for ax in axes[count_ax:]: ax.set_visible(False)

        return fig


# ----------------------------------------------------------------

    def shape_space_correlate_with_features(self,
                                            source,
                                            method,
                                            num_points,
                                            sigma=2.0,
                                            correlation_method='pearson'):
        '''
        Compute correlations coefficients of shape descriptors to principle components.

        First,

        Args:
            source (str):
                As in VideoCollection.embed().
            method (str):
                As in VideoCollection.embed().
            num_points (int or list or ints):
                Number of reconstructed points per dimension/factor/component.
                If int, same number of points for each dimension.
                If list, number of points can be specified per dimension. In this case, length must be equal to `num_dimensions`.
            sigma (float, default=2.0):
                Number of standard deviations used to determine range of reconstruction points.
            correlation_method (str):
                Method to calculate correlation ['pearson', 'kendall', 'spearman']
        Returns:
            df_corr (pandas.DataFrame):
                DataFrame with correlation coefficients for each principle components
        '''

        if source == 'descriptors':
            raise ValueError(f"Reconstruction cannot be made for `source={source}`. Please choose 'contours' instead.")

        if method != 'pca':
            raise ValueError(f"Reconstruction cannot be made for `method={method}`. Please choose `method=pca` instead.")

        from ..analysis.embedding import reconstruct_shapes
        recon_contours, recon_positions = reconstruct_shapes(self,
                                 source=source, method=method,
                                 num_points=num_points,
                                 components=None,
                                 sigma=sigma,
                                 probe_along_axes=True)

        # extract features from contours
        from ..utils.contours import extract_shape_descriptors
        recon_features = [extract_shape_descriptors(contour, return_standardization=False) for contour in recon_contours]

        #print(recon_features)
        # convert list of dicts to pandas dataframe
        import pandas as pd
        df = pd.DataFrame(recon_features)

        # add columns for principle components
        columns_pcs = []
        for i in range(recon_positions.shape[-1]):
            col = f'PC{i+1}'
            df[col] = recon_positions[:,i]
            columns_pcs.append(col)

        # compute correlations of features to PCs
        correlations_pcs = dict()
        for pc_col in columns_pcs:
            correlations = dict()
            for col in df.columns:
                if not col.startswith('PC'):
                    correlations[col] = df[pc_col].corr(df[col], method=correlation_method)
            correlations_pcs[pc_col] = correlations
        df_corr = pd.DataFrame( correlations_pcs )

        df_corr = df_corr.style.background_gradient(cmap='bwr', axis=0)
        return df_corr

# ----------------------------------------------------------------

    def shape_space_polar(self,
                        source,
                        method,
                        num_bins = 8,
                        num_quantiles = 4,
                        shape_size=1.0,
                        alpha_edge=1.0,
                        alpha_face=0.6,
                        label_threshold=0.25):
        '''Plot the embedding of a VideoCollection in shape space.

        Args:
            source (str):
                As in VideoCollection.embed().
            method (str):
                As in VideoCollection.embed().
            shape_size (float):
                Size of the shapes.
            colorcode (str):
                Name of annotation to use for colorcoding of shapes.
            alpha_edge (float):
                Opacity of shape edges.
            alpha_face (float):
                Opacity of shape faces.

        Returns:
            dataframe:
                max pearson correlations of features with rotations
            figure:
                matplotlib figure with polar spaceshape space

        '''
        embedding = self.get_embeddings(source, method)

        from ..plotting.shapespace import plot_polar_embedding

        title = '{} {}'.format(method, source)

        contours = self.get_aligned_contours()
        features = self.get_descriptors(filter_numeric=True)
        df, fig = plot_polar_embedding(embedding=embedding,
                        title=title,
                        contours=contours,
                        features=features,
                        num_bins=num_bins,
                        num_quantiles=num_quantiles,
                        shape_size=shape_size,
                        alpha_edge=alpha_edge,
                        alpha_face=alpha_face,
                        label_threshold=label_threshold)
        return df, fig

# ----------------------------------------------------------------

    def morph_space(self,
                    format,
                    method,
                    components=None,
                    projection=None,
                    labels=None,
                    animate=True,
                    frames=None,
                    colorcode='video',
                    duration=config['plotting', 'duration'],
                    shape_size=1.0,
                    alpha_edge=0.7,
                    alpha_face=0.5,
                    figsize=(10,10)):
        '''Plot the embedding of a VideoCollection in shape space.

        Args:
            format (str):
                As in VideoCollection.embed_morphs().
            method (str):
                One of ['parafac', 'tucker']
            components (list of int):
                Components to plot. If `None` (default), use all components and perform projection in case n_components > 2.
            projection (str):
                If case parafac embedding > 2 factors, perform 2D projection using ['pca', 'umap' or 'tsne']
            labels (array or list):
                Use supervised dimensionality reduction to project onto 2D, only used if `projection` is 'lda' or 'umap'.
            animate (bool):
                Whether to create animation.
            frames (None or list of ints):
                If not None, plot superimposed contours instead of animation (applicable only if animate=False).
            colorcode (str):
                Name of annotation to use for colorcoding of shapes.
            duration (float):
                Duration of animated cell shapes in seconds.
            shape_size (float):
                Size of the shapes.
            alpha_edge (float):
                Opacity of shape edges.
            alpha_face (float):
                Opacity of shape faces.
            figsize (tuple):
                Figure size.
        '''

        #embedding = np.array([v.embedding[(format, method)] for v in self.videos])

        available_methods = ['parafac', 'tucker', 'parafac2', 'vae']
        if method not in available_methods:
            raise ValueError(f"Reconstruction not available for `method={method}`. Please choose one of {available_methods} instead.")

        if method in ['parafac','parafac2'] :
            factors = self.embedding[(format, method)][0] # first factor
        elif method == 'tucker':
            factors = self.embedding[(format, method)][0][0] # first factor, excluding core
        elif method == 'vae':
            factors = self.embedding[(format, method)]

        # check components
        n_components = factors.shape[-1]
        if components is not None:
            if len(components) != 2:
                raise ValueError(f"Length of `components` must be 2 ({components}).")
            for c in components:
                if c > n_components:
                    raise ValueError(f"`component` cannot be larger than `n_components`({c} > {n_components}).")

        contours = self.get_aligned_contours(per_sample=True)
        #contours = self.get_contours(per_sample=True, aligned=True)

        #TODO:
        # - get colors for videos from shape descriptors
        # - because these may change over time, we should account for this during plotting
        #if colorcode in self.get_descriptors().columns:
        #    colors = self.get_descriptors()[colorcode]
        #else:
        colors = self.get_annotations_videos(key=colorcode)

        #title = '{} {}'.format(method, source)
        title = 'video embedding'
        if animate:
            from ..plotting.morphspace import plot_embedding_dynamic
            fig = plot_embedding_dynamic(factors,
                           method=method,
                           contours=contours,
                           components=components,
                           projection=projection,
                           labels=labels,
                           title=title,
                           colors=colors,
                           duration=duration,
                           shape_size=shape_size,
                           alpha_edge=alpha_edge,
                           alpha_face=alpha_face,
                           figsize=figsize)
        else:
            from ..plotting.morphspace import plot_embedding_static
            fig = plot_embedding_static(factors,
                                        method=method,
                                        contours=contours,
                                        colors=colors,
                                        components=components,
                                        frames=frames,
                                        projection=projection,
                                        labels=labels,
                                        alpha_face=alpha_face,
                                        shape_size=shape_size,
                                        figsize=figsize)

        return fig

# ----------------------------------------------------------------

    def morph_space_reconstruction(self,
                    format,
                    method,
                    num_points,
                    components=[1,2],
                    sigma=2.0,
                    colorcode=None,
                    duration=config['plotting', 'duration'],
                    shape_size=1.0,
                    alpha_edge=0.7,
                    alpha_face=0.5,
                    plot_tensor=False,
                    return_contours=False):
        '''Plot the embedding of a VideoCollection in morph space.

        Args:
            format (str):
                As in VideoCollection.embed_morphs().
            method (str):
                As in VideoCollection.embed_morphs(). One of ['parafac', 'tucker']
            components (list of int, length 2):
                Components to plot (default=[1,2]).
            num_points (int or list or ints):
                Number of reconstructed points per dimension/factor/component.
                If int, same number of points for each dimension.
                If list, number of points can be specified per dimension. In this case, length must be equal to `num_dimensions`.
            sigma (float, default=2.0):
                Number of standard deviations used to determine range of reconstruction points.
            colorcode (str):
                Name of descriptor to use for color coding (default=None).
            duration (float):
                Duration of animated cell shapes in seconds.
            shape_size (float):
                Size of the shapes.
            alpha_edge (float):
                Opacity of shape edges.
            alpha_face (float):
                Opacity of shape faces.
            plot_tensor (bool):
                If true, plot reconstructed tensor.
            return_contours (bool):
                If true, reconstructed contours and their coordinates are returned.
        Returns:
            animation:
                Matplotlib animation object. Display with HTML(anim.to_html5_video())
            contours (array):
                Reconstructed contours, if return_contours=True
            positions (array)
                Positions of reconstructed contours, if return_contours=True
            num_points (list):
                Number of reconstructued contours along each dimension, if return_contours=True
        '''

        available_formats = ['stxy', 'stp', 'stef']
        if format not in available_formats:
            raise ValueError(f"Reconstruction not available for `format={format}`. Please choose one of {available_formats} instead.")

        available_methods = ['parafac', 'tucker', 'vae']
        if method not in available_methods:
            raise ValueError(f"Reconstruction not available for `method={method}`. Please choose one of {available_methods} instead.")

        from ..analysis.embedding import reconstruct_morphs
        contours, positions, num_points = reconstruct_morphs(self,
                                                           format=format,
                                                           method=method,
                                                           components=components,
                                                           num_points=num_points,
                                                           sigma=sigma,
                                                           probe_along_axes=False)

        if colorcode is None:
            colors = np.array(list(range(len(contours))))
        else:
            # get descriptors over shapes over time for reconstructed morphs
            vc = VideoCollection.from_contours(contours, annotations=[{'x':p[0], 'y':p[1]} for p in positions])
            vc.align_contours(show_samples=None)
            df_descriptors = vc.analyse_features_time_series()
            try:
                colors = df_descriptors[colorcode]
            except:
                raise ValueError(f"No such time-dependent shape descriptor: '{colorcode}'")

        if colorcode is None:
            title = f"{method} {format} reconstruction"
        else:
            title = f"{method} {format} reconstruction\n(color = {colorcode})"

        from ..plotting.morphspace import plot_embedding_dynamic
        fig = plot_embedding_dynamic(positions,
                        contours=contours,
                        title=title,
                        components=components,
                        method=method,
                        colors=colors,
                        duration=duration,
                        shape_size=shape_size,
                        alpha_edge=alpha_edge,
                        alpha_face=alpha_face,
                        axis_labels=[f"{method.upper()} {components[0]}", f"{method.upper()} {components[1]}"])
        if return_contours:
            return fig, contours, positions, num_points
        else:
            return fig

# ----------------------------------------------------------------

    def morph_space_reconstruction_features(self,
                                            contours,
                                            coordinates,
                                            num_points,
                                            feature_name,
                                            sharey):
        '''Plot morph space with, for each reconstructed morph, an inset with the time course of
        the feature specified with `feature_name`.

        Input is output of `morph_space_reconstruction()` with `return_contours=True`.

        Args:
            contours (array):

            coordinates (array):

            num_points (array):

            feature_name (str):

            sharey (bool):
                If true, use same y axis limits for all insets.

        Returns:
            matplotlib figure
        '''

        import matplotlib.pylab as plt
        from mpl_toolkits.axes_grid1.inset_locator import inset_axes

        # construct new video collection for reconstructed morphs
        vc = VideoCollection.from_contours(contours, annotations=[{'x':p[0], 'y':p[1]} for p in coordinates])

        fig, ax = plt.subplots(1,1,figsize=(10,10), sharex=True, sharey=True)
        ax.scatter(*coordinates.T, s=0)
        ax.set_xlabel('factor 1', labelpad=25, fontsize=14)
        ax.set_ylabel('factor 2', labelpad=25, fontsize=14)
        ax.spines['left'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.set_xticks([])
        ax.set_yticks([])

        xmin, xmax = np.min(coordinates[:,0]), np.max(coordinates[:,0])
        ymin, ymax = np.min(coordinates[:,1]), np.max(coordinates[:,1])
        size_inset = (abs(xmax-xmin) / (num_points[0]+3), abs(ymax-ymin) / (num_points[1]+3))
        #print(size_inset)

        if sharey:
            minimum = np.min([vid.get_descriptors()[feature_name].min() for vid in vc])
            maximum = np.max([vid.get_descriptors()[feature_name].max() for vid in vc])


        for vid in vc:

            # get coordinates
            x = vid.annotations['x']
            y = vid.annotations['y']

            # get features from contour
            df_features = vid.get_descriptors()
            a = df_features[feature_name]

            inset = inset_axes(ax, width="100%", height="100%",
                            bbox_to_anchor=(x-size_inset[0]/2, y-size_inset[1]/2, size_inset[0], size_inset[1]),
                            bbox_transform=ax.transData, loc=1, borderpad=0)

            inset.plot(a)
            inset.xaxis.set_ticks([])
            inset.yaxis.set_ticks([])
            inset.set_xlabel('time')
            inset.set_ylabel(feature_name)
            if sharey:
                inset.set_ylim(bottom=minimum, top=maximum)

        return fig

# ----------------------------------------------------------------

    def morph_space_correlate_with_features(self,
                                            source,
                                            method,
                                            num_points,
                                            sigma=2.0,
                                            correlation_method='pearson',
                                            return_videocollection=False,
                                            plot=True):
        '''
        Compute correlations coefficients of shape descriptors to principle components

        Args:
            source (str):
                As in VideoCollection.embed().
            method (str):
                As in VideoCollection.embed().
            num_points (int or list or ints):
                Number of reconstructed points per dimension/factor/component.
                If int, same number of points for each dimension.
                If list, number of points can be specified per dimension. In this case, length must be equal to `num_dimensions`.
            sigma (float, default=2.0):
                Number of standard deviations used to determine range of reconstruction points.
            correlation_method (str):
                Method to calculate correlation ['pearson', 'kendall', 'spearman']
            return_videocollection (bool):
                If true, returns VideoCollection with reconstructed videos
        Returns:
            df_corr (pandas.DataFrame):
                DataFrame with correlation coefficients for each principle components
            VideoCollection:
                If `return_videocollection` is True, return VideoCollection with reconstructed videos
        '''
        import pandas as pd
        from ..analysis.embedding import reconstruct_morphs

        if source == 'descriptors':
            raise ValueError(f"Reconstruction cannot be made for `source={source}`. Please choose 'contours' instead.")

        available_methods = ['parafac', 'tucker']
        if method not in available_methods:
            raise ValueError(f"Reconstruction cannot be made for `method={method}`. Please choose one of {available_methods} instead.")

        recon_contours, recon_positions, num_points = reconstruct_morphs(self,
                                                           format=source,
                                                           method=method,
                                                           components=None,
                                                           num_points=num_points,
                                                           sigma=sigma,
                                                           probe_along_axes=True)

        # set annotations
        annotations = []
        for dim, num in enumerate(num_points):
            for n in range(num):
                annotation = dict()
                annotation['dimension'] = dim+1
                annotation['number'] = n
                annotations.append(annotation)

        # create new videocollection from reconstructed contours
        vc = VideoCollection.from_contours(recon_contours, annotations=annotations)
        # align contours
        vc.align_contours(show_samples=None)

        # do contour / pca embedding
        vc.embed_shapes('contours', 'pca')
        df_features1 = vc.analyse_shape_space_trajectories()
        df_features2 = vc.analyse_features_time_series()
        df_features = pd.concat([df_features1, df_features2], axis=1)

        # perform PCA and get loadings
        from sklearn.decomposition import PCA
        df_features = (df_features - df_features.mean()) / df_features.std()
        n_components = vc.embedding[('contours', 'pca')].shape[-1]
        pca = PCA(n_components=n_components, whiten=True).fit(df_features)
        df_loadings = pd.DataFrame(pca.components_, columns=df_features.columns)
        import matplotlib.pylab as plt
        plt.matshow(df_loadings.values)

        # add columns for factors
        columns_factors = []
        for i in range(recon_positions.shape[-1]):
            col = f'Factor {i+1}'
            df_features[col] = recon_positions[:,i]
            columns_factors.append(col)

        # compute correlations of features to PCs
        correlations_factors = dict()
        for factor_col in columns_factors:
            correlations = dict()
            for col in df_features.columns:
                if not col.startswith('Factor'):
                    correlations[col] = df_features[factor_col].corr(df_features[col], correlation_method)
            correlations_factors[factor_col] = correlations
        df_corr = pd.DataFrame( correlations_factors )
        # remove rows with only NaNs
        df_corr = df_corr.dropna(axis='index', how='all')

#        print(plot)
#        if plot:
#            from ..plotting.image import _show_videos_in_row
#            anim = _show_videos_in_row
#            anim = vc.show(ncols=num_points[0])

        df_corr = df_corr.sort_values(by='Factor 1', ascending=False)
        df_corr = df_corr.style.background_gradient(cmap='bwr', axis=0)

        if return_videocollection:
#            if plot:
#                return df_corr, vc, anim
#            else:
            return df_corr, vc
        else:
            return df_corr


# ----------------------------------------------------------------

    def parafac_diagnostics(self,
                            format,
                            max_components,
                            plot=True,
                            explained_variance=True,
                            core_consistency=True):
        '''
        Diagnostics to determine the number of components in a PARAFAC decomposition.

        Computes the Explained Variance and Core Consistency (CORCONDIA) for the decomposition for different number of components.
        For the latter, it uses the algorithm proposed by Papalexakis et al. (2015)

        References:
        - Bro, R. and Kiers, H.A., 2003. A new efficient method for determining the number of components in PARAFAC models. Journal of Chemometrics: A Journal of the Chemometrics Society, 17(5), pp.274-286.
        - Papalexakis, E.E. and Faloutsos, C., 2015, April. Fast efficient and scalable core consistency diagnostic for the parafac decomposition for big sparse tensors. In 2015 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP) (pp. 5441-5445). IEEE.

        Args:
            format (str): format of tensor to decompose/diagnose, one of ['stxy', 'stp', ...]
            max_components (int): maximum number of components
            explained_variance (bool): if True, compute explained variance
            core_consistency (bool): if True, compute core consistency

        Returns:
            df (pd.DataFrame): return data frame with results
            fig: if plot=True, return matplotlib figure
        '''
        from ..analysis.embedding import parafac, variance_explained, corcondia
        import pandas as pd
        from ..config import tqdm as tqdm

        X = self.tensor[format]
        results = []
        for c in tqdm(range(1, max_components)):
            #print(f"Number of components = {c}")
            factors = parafac(X, n_components=c)
            ve, cc = None, None
            if explained_variance:
                ve = variance_explained(X, factors)
            if core_consistency:
                cc = corcondia(X, factors)

            results.append({'n_components' : c,
                        'explained variance' : ve,
                        'core consistency': cc})

        df = pd.DataFrame(results)
        df = df.set_index('n_components', drop=True)

        if plot:
            import matplotlib.pyplot as plt
            fig, ax1 = plt.subplots()
            ax2 = ax1.twinx()
            ax1.plot(df.index, df['explained variance'], 'o-', c='b')
            ax1.set_ylabel('explained variance (%)', color='b')
            ax2.plot(df.index, df['core consistency'], 'o-', c='g')
            ax2.set_ylabel('core consistency (%)', color='g')
            ax1.grid(axis='x')
            ax1.set_xlabel('Number of components')
            plt.show()

        if plot:
            return df, fig
        else:
            return df


# ----------------------------------------------------------------

    def show(self,
             duration=config['plotting', 'duration'],
             show_image=False,
             show_contour=True,
             cmap='gray_r',
             figsize=(2, 2),
             ncols=None,
             verbose=True):
        '''Creates an HTML5 table of animated videos for usage in Jupyter
        notebook.

        Example usage:
            ```
            from IPython.display import HTML
            HTML(videos.animate())
            ```

        Args:
            duration (float):
                Duration of video in seconds.
            show_image (bool):
                Plot binary image.
            show_contour (bool):
                Plot (unaligned) contour.
            cmap (strings or matplotlib.colormap object):
                Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
                names:
                https://matplotlib.org/examples/color/colormaps_reference.html
            figsize (tuple of 
                Size of individual videos.
            verbose (bool):
                Show progress

        Returns:
            string:
                An HTML5 table with the animated videos.
        '''

        if not (show_image or show_contour):
            raise ValueError("show_image and/or show_contour must be True")

        from ..plotting import _show_videocollection

        return _show_videocollection(self.videos,
                                     duration=duration,
                                     show_image=show_image,
                                     show_contour=show_contour,
                                     cmap=cmap,
                                     figsize=figsize,
                                     ncols=ncols,
                                     verbose=verbose)

# ----------------------------------------------------------------

    def cluster_shapes(self, source, method, cl_method, n_clusters=None, plot=False):
        '''
        Cluster shapes in shape space

        Args:
            source (str): one of ['descriptors', 'contours']
            method (str): one of ['pca', 'tsne', 'umap']
            cl_method (str): one of ['kmeans', 'hdbscan', 'minibatchkmeans',
                                     'affinitypropagation', 'meanshift',
                                     'agglomerativeclustering',
                                     'spectral']
            n_clusters (str): number of clusters (interpreted as 'min_cluster_size' in 'hdbscan')
            plot (bool): 2D plot of clusters
        '''

        from ..analysis.cluster import cluster
        import pandas as pd

        embeddings = self.get_embeddings(source, method)

        labels = cluster(data=embeddings,
                         cl_method=cl_method,
                         n_clusters=n_clusters)

        if plot:
            import matplotlib.pyplot as plt
            p = plt.scatter(embeddings[:, 0], embeddings[:, 1], c=labels)
            plt.colorbar(p)
            plt.show()


        # assign cluster labels back to frames and prepare dataframe output
        i = 0
        results = []
        for video in self:
            for frame in video:
                frame.annotations['cluster'] = labels[i]
                i += 1
                result = dict()
                result['video'] = video.annotations['video']
                result['frame'] = frame.annotations['frame']
                result['cluster'] = frame.annotations['cluster']
                results.append(result)

        return pd.DataFrame(results)

# ----------------------------------------------------------------

    def cluster_morphs(self, format, cl_method, method, n_clusters=None, plot=False):
        '''
        Cluster morphs in morph space.

        Args:
            format (str): one of ['std', 'stxy', 'stp']
            cl_method (str): one of ['kmeans', 'hdbscan', 'minibatchkmeans',
                                     'affinitypropagation', 'meanshift',
                                     'agglomerativeclustering',
                                     'spectral']
            method (str): one of ['parafac', 'tucker']
            n_clusters (str): number of clusters (interpreted as 'min_cluster_size' in 'hdbscan')
            plot (bool): 2D plot of clusters
        '''

        from ..analysis.cluster import cluster
        import pandas as pd

        embeddings = self.get_embeddings(format, method)

        print(embeddings.shape)
        labels = cluster(data=embeddings,
                         cl_method=cl_method,
                         n_clusters=n_clusters)

        if plot:
            import matplotlib.pyplot as plt
            fig, ax = plt.subplots(1,2,figsize=(10,4))
            fig.tight_layout()

            from ..analysis.embedding import pca
            data = pca(embeddings)
            ax[0].set_title('PCA')
            ax[0].scatter(*data.T, c=labels)
            ax[0].set_xlabel('PC1')
            ax[0].set_ylabel('PC2')

            from ..analysis.embedding import umap
            data = umap(embeddings)
            ax[1].set_title('UMAP')
            p = ax[1].scatter(*data.T, c=labels)
            ax[1].set_xlabel('UMAP1')
            ax[1].set_ylabel('UMAP2')

            fig.colorbar(p, ax=ax.ravel().tolist())

            plt.show()

        # assign cluster labels back to frames and prepare dataframe output
        i = 0
        results = []
        for video in self:
            video.annotations['cluster'] = labels[i]
            i += 1
            result = dict()
            result['video'] = video.annotations['video']
            result['cluster'] = video.annotations['cluster']
            results.append(result)

        return pd.DataFrame(results)


# ----------------------------------------------------------------

    def analyse_shape_space_trajectories(self,
                                         source='contours',
                                         method='pca'):

        '''Analyse the trajectories in shape space.

        Args:
            source (str): source of data ['descriptors', 'contours']
            method (str): method of embedding ['pca', 'tsne', 'umap']
        '''

        from ..analysis.timeseries import analyse_trajectories
        from ..config import tqdm as tqdm

        results = []
        for video in tqdm(self):

            #[video.frames[f].embedding[()]
            trajectory = np.array([frame.get_embedding(source, method)
                                   for frame in video])
            result = analyse_trajectories(trajectory)
            result['video'] = video.annotations['video']
            results.append(result)

        import pandas as pd
        df_stats = pd.DataFrame(results)
        df_stats = df_stats.set_index('video')
        return df_stats

# ----------------------------------------------------------------

    def analyse_features_time_series(self, check_periodicity=False):
        '''
        Time series analysis of shape features.

        Performs STL decomposition of time series to decompose trend, seasonality, residual.
        Periodicity is detected using peak detection based on the autocorrelation profile

        Args:
            check_periodicity (bool):
                If true, detect periodicity in time series and performs STL decomposition

        Returns:
            dataframe with stats for each feature: mean, std, periodicity, period, noise
        '''

        from ..analysis import analyse_series
        import pandas as pd
        from ..config import tqdm as tqdm

        # get numeric features for all frames and all videos
        #df_features = self.get_descriptors(filter_numeric=True)

        results = []
        # iterate through videos
        for video in tqdm(self):
            df_features = self.get_descriptors(video=video, filter_numeric=True)
            df_features = df_features.sort_values('frame')
            result = dict()
            keys = df_features.columns.values
            # exclude 'frame' and 'video' columns
            keys = [key for key in keys if key not in ['frame', 'video']]

            # compute time series statistics for all columns
            for i, key in enumerate(keys):
                series = df_features[key].values
                result_temp = analyse_series(series, check_periodicity=check_periodicity)
                # renaming keys
                for resultkey in result_temp.keys():
                    result[resultkey+' of '+key] = result_temp[resultkey]

            # add colum with video index
            result['video'] = video.annotations['video']
            # add to list
            results.append(result)

            # convert into dataframe
            df_stats = pd.DataFrame(results)
            df_stats = df_stats.set_index('video')

        return df_stats


# ----------------------------------------------------------------

# ----------------------------------------------------------------

    def kruskal_test(self, df_clusters, alpha, multipletest_corr='bonf'):
        '''Perform statistical Kruskal test
        Compare multiple groups, rank-based, does not assume normal distribution.

        Args:
            df_clusters (dict of DataFrame):
                dictionary of dataframes, one for each cluster
        Returns:
            p_values (dict):
                dictionary with (corrected) p-value for each column in df_clusters
        '''
        from scipy.stats import kruskal
        from statsmodels.stats import multitest

        small_group_error = False
        for cluster, group in df_clusters.groupby('label'):
        #for k, v in df_clusters.items():
        #    print('Cluster {}, size = {}'.format(k, len(v)))
            print('Cluster {}, size = {}'.format(cluster, len(group)))
            if len(group) < 2:
                small_group_error = True

        if small_group_error:
            raise ValueError('Cannot compare group of size {}.'.format(len(group)))

        p_values = dict()
        #for col in list(df_clusters.values())[0].columns:
        for col in df_clusters.columns:
            if col == 'label': continue

            # get data from column
            #series = [df[col].values for df in list(df_clusters.values())]
            series = [group[col].values for _, group in df_clusters.groupby('label')]

            # skip data with only constant values, stdev close to 0.0
            if np.all([np.isclose(s.std(), 0.0) for s in series]):
                continue

            # do kruskal test
            _, p = kruskal(*series)
            p_values[col] = p

        ## Multiple testing adjustment
        multipletest_corr_formats = ['bonferroni', 'bonf', 'benjamin-hochberg', 'bh']
        if multipletest_corr not in multipletest_corr_formats:
            raise ValueError(f'multipletest_corr must be one of {multipletest_corr_formats}')

        if multipletest_corr.lower() == 'bonferroni' or multipletest_corr.lower() == 'bonf':
            method = 'bonferroni'
        elif multipletest_corr.lower() == 'benjamin-hochberg' or multipletest_corr.lower() == 'bh':
            method = 'fdr_bh'
        reject, p_values_corrected, _, alpha_corrected = multitest.multipletests(list(p_values.values()), alpha=alpha, method=method)
        print('Adjusted alpha ({}): {}'.format(multipletest_corr, alpha_corrected))


        # sort the features according to their p-values
        sorted_indices = np.argsort(p_values_corrected)
        p_values_corrected = np.array(p_values_corrected)[sorted_indices]
        cols = np.array(list(p_values.keys()))[sorted_indices]
        reject = np.array(reject)[sorted_indices]

        p_values_corrected = dict(zip(cols, p_values_corrected))
        significant_features = np.array(cols)[reject].tolist()

        return p_values_corrected, significant_features




    def cluster_comparison(self,
                            cluster_label='cluster_video',
                            multipletest_corr='benjamin-hochberg',
                            alpha=0.05,
                            check_periodicity=False,
                            plot=True):
        '''

        Args:
            alpha (float):
                Significance level, probability of rejecting null hypothesis while it is true.
                Default: 0.05
            cluster_label (str):
                Name of annotation to split videos into clusters.
                Default: 'cluster_video'
            multipletest_corr (str):
                Alpha correction for multiple testing.
                One of ['bonferroni', 'benjamin-hochberg'] or ['bonf', 'bh'].
                Default: 'benjamin-hochberg'

        '''

        import pandas as pd
        import numpy as np
        from ..config import tqdm

        df_clustering = pd.DataFrame( [{'video': video.annotations['video'],
                                        'cluster_video': video.annotations[cluster_label]}
                                        for video in self])

        clusters_indices = [df_clustering.query('cluster_video == {}'.format(i)).index.values for i in df_clustering['cluster_video'].unique()]

        # create Dataframe with data for clusters/labels
        df_clusters = pd.DataFrame()
        for i, indices in enumerate(clusters_indices):
            cluster = VideoCollection()
            cluster.videos = [self.videos[i] for i in indices] #
            df1 = cluster.analyse_shape_space_trajectories()
            df2 = cluster.analyse_features_time_series(check_periodicity=check_periodicity)
            assert(len(df1) == len(df2))
            df_cluster = pd.concat([df1, df2], axis=1)
            df_cluster['label'] = i
            df_clusters = df_clusters.append( df_cluster )

        # Kruskal test with multiple testing correction
        p_values_corrected, significant_features = self.kruskal_test(df_clusters, alpha=alpha, multipletest_corr=multipletest_corr)

        # filter the significant features
        significant_features.append('label')
        df_clusters_significant = df_clusters[significant_features]


        if plot:
            import matplotlib.pylab as plt
            import seaborn as sns
            from ..external.statannot.statannot import add_stat_annotation

            num_significant_features = len(df_clusters_significant.columns)
            nr = int(np.ceil(np.sqrt(num_significant_features)))
            nc = nr
            fig, axes = plt.subplots(nr, nc, figsize=(3*nc, 3*nr), sharex=True, squeeze=False)
            fig.tight_layout(h_pad=7.5, w_pad=4.0)
            axes = axes.flatten()

            for i, col in enumerate(tqdm(df_clusters_significant.columns)):
                ax = axes[i]
                if col == 'label': print('skipped: ', i); continue
                data = df_clusters_significant
                x = 'label'
                y = col
                # swarm plot
                ax.set_ylim( [data[col].min(), data[col].max()] ) # fix for swarmplots when values are small
                ax = sns.swarmplot(data=data, x=x, y=y, ax=ax, zorder=0)
                # boxplot
                ax = sns.boxplot(data=data, x=x, y=y, showcaps=False,boxprops={'facecolor':'None'},
                                showfliers=False,whiskerprops={'linewidth':0}, ax=ax, zorder=10)
                # bars showing significance between groups
                num_clusters = len(data['label'].unique())
                pvalueThresholds = [[1.,"ns"], [0.05,"*"], [0.01,"**"], [0.001,"***"], [0.0001,"****"]]
                if i == 0: print(pvalueThresholds)
                import itertools
                boxPairList = list(itertools.combinations(data['label'].unique(), 2))
                add_stat_annotation(ax, data=data, x=x, y=y,
                                    boxPairList=boxPairList,
                                    pvalueThresholds=pvalueThresholds,
                                    test='Mann-Whitney', textFormat='star',
                                    loc='outside', verbose=0)

            _ = [a.set_visible(False) for a in axes[i:]]

        return pd.Series(p_values_corrected)

# ----------------------------------------------------------------


    def cluster_comparison_old(self,
                            cluster_label='cluster_video',
                            multipletest_corr='benjamin-hochberg',
                            alpha=0.05,
                            check_periodicity=False,
                            plot=True):
        '''

        Args:
            alpha (float):
                Significance level, probability of rejecting null hypothesis while it is true.
                Default: 0.05
            cluster_label (str):
                Name of annotation to split videos into clusters.
                Default: 'cluster_video'
            multipletest_corr (str):
                Alpha correction for multiple testing.
                One of ['bonferroni', 'benjamin-hochberg'] or ['bonf', 'bh'].
                Default: 'benjamin-hochberg'

        '''

        import pandas as pd
        import numpy as np
        from scipy.stats import kruskal
        from statsmodels.stats import multitest

        df_clustering = pd.DataFrame( [{'video': video.annotations['video'],
                                        'cluster_video': video.annotations[cluster_label]}
                                        for video in self])

        #print(df_clustering)

        clusters_indices = [df_clustering.query('cluster_video == {}'.format(i)).index.values for i in df_clustering['cluster_video'].unique()]
        #print(clusters_indices)

        df_clusters = dict() # distionary of DataFrames, one for each cluster
        for i, indices in enumerate(clusters_indices):
            cluster = VideoCollection()
            cluster.videos = [self.videos[i] for i in indices] #
            df1 = cluster.analyse_shape_space_trajectories()
            df2 = cluster.analyse_features_time_series(check_periodicity=check_periodicity)
            assert(len(df1) == len(df2))
            df_cluster = pd.concat([df1, df2], axis=1)
            #print(df_cluster)
            df_clusters[i] = df_cluster

        # drop columns with NaNs
        #for key, df in df_clusters.items():
        #   df = df.dropna(axis=1)

        ###  Kruskal test ###
        # This test compares multiple groups
        # and rank-based test does not assume normal distribution.

        small_group_error = False
        for k, v in df_clusters.items():
            print('Cluster {}, size = {}'.format(k, len(v)))
            if len(v) < 2:
                small_group_error = True

        if small_group_error:
            raise ValueError('Cannot compare group of size {}.'.format(len(v)))

        p_vals = dict()
        for col in list(df_clusters.values())[0].columns:
            # get data from column
            series = [df[col].values for df in list(df_clusters.values())]
            # skip data with only constant values, stdev close to 0.0
            #if np.any([np.isclose( s.std(), 0) for s in series]):
            #    continue
            if all([len(np.unique(s)) < 2 for s in series]):
                continue

            # do kruskal test
            _, p = kruskal(*series)
            p_vals[col] = p

        ## Multiple testing adjustment
        if multipletest_corr.lower() == 'bonferroni' or multipletest_corr.lower() == 'bonf':
            method = 'bonferroni'
        elif multipletest_corr.lower() == 'benjamin-hochberg' or multipletest_corr.lower() == 'bh':
            method = 'fdr_bh'
        reject, p_vals_corr, _, alpha_corrected = multitest.multipletests(list(p_vals.values()), alpha=alpha, method=method)
        print('Adjusted alpha ({}): {}'.format(multipletest_corr, alpha_corrected))

        #print(p_vals)
        #print(p_vals_corr)
        p_vals_corr = dict(zip(list(p_vals.keys()), p_vals_corr))
        significant_features = dict(zip(list(p_vals.keys()), reject))

        if plot:
            import matplotlib.pylab as plt
            num_significant_features = np.sum(list(significant_features.values()))
            nr = int(np.ceil(np.sqrt(num_significant_features)))
            nc = nr
            fig, ax = plt.subplots(nr, nc, figsize=(3*nc, 3*nr), sharex=True, squeeze=False)
            fig.tight_layout()#h_pad=1.0)
            ax = ax.flatten()

        ## Show significant results
        c = 0
        results = dict()
        for (col, p), (col, significant) in zip(p_vals_corr.items(), significant_features.items()):

            if significant:
                results[col] = p
                #print('{} : {:<20.3f}'.format(col, p))
                series = [df[col] for df in df_clusters.values()]

                tics = list(range(1, len(series)+1))
                means = [s.values.mean() for s in series]
                std = [s.values.std() for s in series]

                if plot:
                    _ = ax[c].boxplot( series,
                                        notch=False,  # notch shape
                                        vert=True,   # vertical box aligmnent
                                        patch_artist=True)   # fill with color )
                    ax[c].set_xticks(list(range(1, len(series)+1)), df_clusters.keys())
                    _ = ax[c].set_title('{}, p = {:.4f}'.format(col, p))

                    c += 1

                def _stars(p):
                    if p < 0.0001:
                        return "****"
                    elif (p < 0.001):
                        return "***"
                    elif (p < 0.01):
                        return "**"
                    elif (p < 0.05):
                        return "*"
                    else:
                        return "-"

                if len(df_clusters) > 2:
                    print('\n === {} === \n'.format(col))
                    # Post-hoc test: mann whitley U test per group pairs
                    # per significant feature, test groups
                    import itertools
                    combinations_indices = list(itertools.combinations(df_clusters.keys(), 2))
                    combinations_data = list(itertools.combinations(series, 2))
                    print(combinations_indices)

                    from scipy.stats import mannwhitneyu
                    p_values  = dict()
                    for ind, groups in zip(combinations_indices, combinations_data):
                        try:
                            _, p = mannwhitneyu(*groups, alternative='two-sided')
                            p_values[ind] = p
                        except ValueError as e:
                            print('Cannot perform posthoc test for {}: {}'.format(col, e))
                            continue


                        if p < alpha:
                            print('Groups {} are different {:.3f}'.format(ind, p))
                        else:
                            print('Groups {} are NOT significantly different: {:.3f}'.format(ind, p))

                    '''
                    if plot:

                        for i, (ind, groups) in enumerate(zip(combinations_indices, combinations_data)):
                            print((ind[0]+0.5)/len(ind), -0.1*i)

                            ax[c].annotate("",
                                            xy=((ind[0]+0.5)/len(ind), -0.1*i), xycoords='axes fraction',
                                            xytext=((ind[1]+0.5)/len(ind), -0.1*i), textcoords='axes fraction',
                                            arrowprops=dict(arrowstyle="-", ec='black',
                                            connectionstyle="bar,fraction=0.2"))
                            ax[c].annotate(_stars(p),
                                            xy=((ind[0]+0.5)/len(ind), -0.1*i), xycoords='axes fraction',
                                            xytext=((ind[1])/len(ind), -0.1*i), textcoords='axes fraction')

                            c += 1
                    '''

        if plot:
            _ = [a.set_visible(False) for a in ax[c:].flatten()]

        return pd.Series(results).sort_values()

    def compare_embeddings(self, embedding1, embedding2, method='kabsch', plot=True):
        '''
        Compute the minimal distance (RSME) between two embeddings, independent of their scale and rotation.

        This uses the Kabsch algorithm (https://en.wikipedia.org/wiki/Kabsch_algorithm)
        to find the optimal rotation, scaling and translation between two paired point sets.

        TODO: Re-evaluate Umeyama's algorithm...

        Args:
            embedding1 (tuple, (str, str)):
                Format and method for embedding, e.g. ('stxy', 'parafac') or ('descriptors', 'pca')
            embedding2 (tuple, (str, str)):
                Format and method for embedding, e.g. ('stxy', 'tucker') or ('contours', 'pca').
            method (str): 'kabsch' (without scaling) or 'umeyama' (with scaling)
            plot (bool):
                Whether or not to plot the difference between embeddings.

        Returns:
            RSME (float):
                Root square mean error between the two embeddings.

        '''

        from ..analysis.embedding import compare_embeddings

        embeddings_names = [embedding1, embedding2]
        embeddings = [self.embedding[name] for name in embeddings_names]

        def get_data(data, e):
            (source, format) = e
            if source in ['descriptors', 'contours']:
                return data
            else:
                if format in ['parafac','parafac2']:
                    return data[0]
                elif format == 'tucker':
                    return data[0][0]
                else:
                    return data

        embeddings = [get_data(embedding, embeddings_name) for embedding, embeddings_name in zip(embeddings, embeddings_names)]

        if plot:
            rsme, (X, Y_hat) = compare_embeddings(*embeddings, method=method, return_reconstruction=True)
            import matplotlib.pyplot as plt
            import re
            fig, ax = plt.subplots(1,1,figsize=(8,8))

            # draw lines between points
            for p1, p2 in zip(X, Y_hat):
                d = p2[:2]-p1[:2]
                ax.arrow(*p1[:2], *d, linestyle=':', lw=0.1)# head_width=.1, color='grey', length_includes_head=True) #

            # draw points
            ax.scatter(*X[:,:2].T, alpha=.5, s=5, label=re.sub(r'\W+', ' ', str(embedding1)), zorder=1)
            ax.scatter(*Y_hat[:,:2].T, alpha=.5, s=5, label=re.sub(r'\W+', ' ', str(embedding2)), zorder=2)
            ax.legend()

            ax.set_xticks([])
            ax.set_yticks([])
            plt.show()
        else:
            rsme = compare_embeddings(*embeddings, method=method, return_reconstruction=False)

        return rsme

