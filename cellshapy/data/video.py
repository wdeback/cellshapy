#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from . import Frame, Annotatable, Serializable
from ..config import config


class Video(Annotatable, Serializable):
    '''A Video is a collection of frames.

    Args:
        index (int): Index of the video.

    Attributes:
        index (int):
            Index of the video.
        frame (list):
            List of Frame objects.
        annotation (dict):
            Dict of annotations.
    '''
    def __init__(self, index, filename=None):
        Annotatable.__init__(self)
        Serializable.__init__(self)
        self.index = index
        self.filename = filename
        self.frames = []
        self.annotations['video'] = index

        # will be added by videocollection.embed_morphs()
        self.tensor_slice = dict()
        # will be added by videocollection.embed_morphs()
        self.embedding = dict()

    # def from_glob(globExp, indexm min_size=None):
    #    """Creates a video from a glob"""
    #    from glob import glob
    #    print(f"\rCreating video from glob \"{glob}\"", end='')
    #    files = glob(globExp, recursive=True)
    #    return Video(files, min_size) # wdb: what does this do??

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __getitem__(self, index):
        return self.frames[index]

    def __len__(self):
        return len(self.frames)

    def __str__(self):
        p_lim = config['print', 'limit.video']
        out = f"Video: "
        out += f"{len(self)} frames; "
        out += f"filename: {self.filename}\n"
        m = min(len(self), p_lim)
        out += "\n".join([f"  {l}"
                          for i in self[:m]
                          for l in i.__str__().split("\n")
                          ])
        out += f"  ..." if len(self) > p_lim else ""
        return out

    @staticmethod
    def from_dir(path, video_index, annotations=None, min_size=None, normalize_area=None, n_jobs=-1):
        '''Creates a video from a directory of images.

        Args:
            path (str):
                A path to the directory containing the images for this video.
            index (int):
                The index of this video.
            min_size (int):
                The minimum size of objects in the Frames of this video.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!
            n_jobs (int):
                Frames are created in parallel to leverage multicore CPUs. Use
                this argument to tune the number of parallel processes.
        Returns:
            Video:
                A newly created Video object that contains Frames that are
                created from the files in the directory.
        '''
        import os
        from joblib import Parallel, delayed
        from ..utils import sort_human

        assert os.path.isdir(path), "path needs to be a path"

        print(f"\rCreating video from directory \"{path}\"", end='')
        v = Video(video_index)

        files = os.listdir(path)
        files = [os.path.join(path, file) for file in files]
        files = sort_human(files)

        # add annotations
        if annotations is not None:
            for key, value in annotations.items():
                v.annotations[key] = value

        v.filename = path
        v.frames = Parallel(n_jobs=n_jobs)(
                delayed(Frame.from_file)(file,
                                         frame_index=frame_index,
                                         video_index=video_index,
                                         annotations=annotations,
                                         min_size=min_size,
                                         normalize_area=normalize_area)
                for frame_index, file in enumerate(files))
        # v.frames = [Frame.from_file(file) for file in files]
        return v

    @staticmethod
    def from_composite(path, video_index, annotations=None, min_size=None, normalize_area=None, n_jobs=-1):
        '''Creates a video from a composite image.

        Args:
            path (str):
                A path to the composite image, i.e. an image with multiple
                pages.
            index (int):
                The index of this video.
            min_size (int):
                The minimum size of objects in the Frames of this video.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!!
            n_jobs (int):
                Frames are created in parallel to leverage multicore CPUs. Use
                this argument to tune the number of parallel processes.
        Returns:
            Video:
                A newly created Video object that contains Frames that are
                created from the pages in the multi-page image file.
            '''
        import os
        import numpy as np
        import warnings
        from skimage.external import tifffile
        from joblib import Parallel, delayed

        assert os.path.isfile(path), "path needs to be a regular file"
        #print("\rCreating video from tiff file: {}".format(path), end='')

        ims = tifffile.imread(path)
        ims = np.squeeze(ims)
        v = Video(video_index)
        v.filename = path

        if not np.issubdtype(ims.dtype, np.integer):
            warnings.warn(f"Tiff image had datatype {ims.dtype}, but "
                          "integer required. Converting now.")
            ims = ims.astype(np.integer)

        # add annotations
        if annotations is not None:
            for key, value in annotations.items():
                v.annotations[key] = value

        v.frames = Parallel(n_jobs=n_jobs)(
                delayed(Frame)(im,
                               frame_index=frame_index,
                               video_index=video_index,
                               annotations=annotations,
                               min_size=min_size,
                               normalize_area=normalize_area,
                               filename=path) for frame_index, im in enumerate(ims))

        return v

    @staticmethod
    def from_array(array, video_index, annotations=None, min_size=None, normalize_area=None, n_jobs=-1):
        '''Creates a video from a numpy array in (time x height x width) format.

        Args:
            array (array):
                A numpy array in (time x height x width) format.
            index (int):
                The index of this video.
            min_size (int):
                The minimum size of objects in the Frames of this video.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!
            n_jobs (int):
                Frames are created in parallel to leverage multicore CPUs. Use
                this argument to tune the number of parallel processes.
        Returns:
            Video:
                A newly created Video object that contains Frames that are
                created from the files in the directory.
        '''
        import os
        from joblib import Parallel, delayed

        #print(f"\rCreating video from array", end='')
        v = Video(video_index)

        # add annotations
        if annotations is not None:
            for key, value in annotations.items():
                v.annotations[key] = value

        v.filename = None
        v.frames = Parallel(n_jobs=n_jobs)(
                delayed(Frame.from_array)(array[frame_index],
                                         frame_index=frame_index,
                                         video_index=video_index,
                                         annotations=annotations,
                                         min_size=min_size,
                                         normalize_area=normalize_area)
                for frame_index in range(len(array)))
        # v.frames = [Frame.from_file(file) for file in files]
        return v

    @staticmethod
    def from_contour(contour, video_index, annotations=None, min_size=None, normalize_area=None, n_jobs=-1):
        '''Creates a video from a contour in (time x contour_elements x 2) format.

        Args:
            contour (array):
                A numpy array in (time x contour_elements x 2) format.
            index (int):
                The index of this video.
            min_size (int):
                The minimum size of objects in the Frames of this video.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!
            n_jobs (int):
                Frames are created in parallel to leverage multicore CPUs. Use
                this argument to tune the number of parallel processes.
        Returns:
            Video:
                A newly created Video object that contains Frames that are
                created from the files in the directory.
        '''
        import os
        from joblib import Parallel, delayed

        #print(f"\rCreating video from array", end='')
        v = Video(video_index)

        # add annotations
        if annotations is not None:
            for key, value in annotations.items():
                v.annotations[key] = value

        v.filename = None
        v.frames = Parallel(n_jobs=n_jobs)(
                delayed(Frame.from_contour)(contour[frame_index],
                                         frame_index=frame_index,
                                         video_index=video_index,
                                         annotations=annotations,
                                         min_size=min_size,
                                         normalize_area=normalize_area)
                    for frame_index in range(len(contour)))
        return v

    def get_descriptors(self):
        '''Get shape descriptor over time as pandas dataframe'''

        import pandas as pd
        features = []
        for i, f in enumerate(self.frames):
            feat = f.features
            features.append(feat)

        # convert to dataframe
        df = pd.DataFrame(features)
        # drop non-feature columns
        df = df.drop(['frame', 'video'], axis='columns', errors='ignore')
        return df

    def get_curvatures(self, signed):
        '''Get curvatore over time as numpy array'''
        import numpy as np
        from ..utils.contours import get_curvature
        curvatures = np.array([get_curvature(f.contour, signed=signed) for i, f in enumerate(self.frames)])
        return curvatures

    def get_aligned_contours(self):
        '''Returns all the aligned contours in a Video.'''
        import numpy as np
        return np.array([f.get_contour_aligned() for f in self.frames])

    def get_embedding(self, source, method):
        '''Returns embedding for a given source and method, if it exists.

        Args:
            source (str): source of shape representation: ['std', 'stxy', 'stp']
            method (str): method of dimensionality reduction: ['parafac', 'tucker']
        Returns:
            embedding coordinates (ndarray)

        Raises:
            ValueError if embedding does not exist
        '''
        try:
            self.embedding[source, method]
        except KeyError:
            raise ValueError("Embedding for `{}` is not yet calculated. "
                             "First run `VideoCollection.embed_morphs()`.".format((source, method)))
        return self.embedding[source, method]

    def show(self,
             duration=config['plotting', 'duration'],
             show_image=False,
             show_contour=True,
             cmap='gray_r',
             ax=None,
             figsize=(4, 4),
             verbose=True):
        '''Create animation of video, with binary image and/or contour

         Example usage:
         ```
         from IPython.display import HTML
         HTML(videos[0].show())
         ```

        Args:
            duration (float):
                Duration of video in seconds.
            show_image (bool):
                Plot binary image.
            show_contour (bool):
                Plot (unaligned) contour.
            cmap (strings or matplotlib.colormap object):
                Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
                names:
                https://matplotlib.org/examples/color/colormaps_reference.html
            ax (matplotlib.Axes object):
                If provided, plots into this axes.
            figsize (tuple of floats):
                Size of figure.
            verbose (bool):
                Show progress.
        '''

        if not (show_image or show_contour):
            raise ValueError("show_image and/or show_contour must be True")

        from ..plotting import _show_video

        return _show_video(self.frames,
                           duration=duration,
                           show_image=show_image,
                           show_contour=show_contour,
                           cmap=cmap,
                           ax=ax,
                           figsize=figsize,
                           verbose=verbose)


    def plot_timeseries_features(self, clustermap=False, figsize=(8,0.5)):
        '''Plot time series of descriptors'''

        import matplotlib.pylab as plt
        import numpy as np

        features = self.get_descriptors()
        # sort columns alphabetically
        features = features.sort_index(axis=1)

        nr = len(features.columns)
        fig, ax = plt.subplots(nrows=int(np.ceil(nr/2)), ncols=2, figsize=(figsize[0],figsize[1]*nr), sharex=True, squeeze=False)
        ax = ax.flatten()
        fig.tight_layout()
        fig.subplots_adjust(hspace=0.)
        for i, (a, col) in enumerate(zip(ax, features.columns)):
            a.plot(features[col], 'o-')
            a.legend(labels=[col], handles=[], markerscale=0., handlelength=0., loc='best', frameon=False)
        for a in ax[i+1:]:
            a.set_visible(False)

        if clustermap:
            features = self.get_descriptors()
            df_corr = features.corr()
            import seaborn as sns
            sns.clustermap(data=df_corr, cmap="bwr")

        return fig


    def plot_clustermap(self, figsize=(8,0.5)):
        '''Plot clustermap of features'''

        features = self.get_descriptors()
        df_corr = features.corr()
        import seaborn as sns
        sns.clustermap(data=df_corr, cmap="bwr")


#----------------------------------------------------------------

    def plot_curvature_kymograph(self, signed=True, interpolation=True, cmap='bwr', figsize=(4,16)):

        import matplotlib.pylab as plt
        import numpy as np

        # get signed curvatures
        curvatures = self.get_curvatures(signed=signed)
        # calculate 5% and 95% percentiles (for robust colormapping)
        vmin = np.percentile(curvatures, 5)
        vmax = np.percentile(curvatures, 95)

        fig, ax = plt.subplots(1,1,figsize=figsize)

        im = ax.imshow(curvatures,
                        vmin=vmin, vmax=vmax,
                        cmap=cmap,
                        interpolation='bicubic' if interpolation else None)
        ax.set_xlabel('Contour element', fontsize=14)
        ax.set_ylabel('Time', fontsize=14)

        # create colorbar on correct location
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        # create an axes on the right side of ax. The width of cax will be 5%
        # of ax and the padding between cax and ax will be fixed at 0.05 inch.
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(im, cax=cax)
        cb.set_label('Curvature', fontsize=14)

