#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import warnings

from . import Annotatable, Serializable
from ..config import config


class Frame(Annotatable, Serializable):
    '''A Frame contains image information and related extracted metrics.

    Args:
        image (array):
            The image that is the foundation for this frame
        min_size (int, optional):
            If None, small objects are not removed. If not None, specifies the
            minimum size, an objects needs to have to not be removed.
        normalize_area (str or None):
            A string describing, how contours should be normalized. Options
            include:
                * None: No normalization is performed
                * "pairwise": Contours are normalized to the mean pairwise
                  distance
                * "area": Contours are normalized so that the area of the
                  contours is 1.
                * "rmsd": Contours are normalized so that the root mean square
                  distance (RMSD) to the centroid is 1.
            Note: This ignores information on shape sizes!

    Attributes:
        mask (array):
            A binary mask that is generated from the image.
        features (dict):
            A collection of different features. They are calculated when the
            Frame is created.
        centroid (ndarray):
            The centroid of the image. Note: if there are more than one objects
            in the image, the different objects will be treated as one big
            object.
        contour (ndarray):
            The (unaligned) contour of the biggest object in the image.
        contour_aligned (ndarray):
            The alligned contour of the biggest object or None if alignment was
            not yet calculated.
        embedding (dict):
            The embedding for this frame.
    '''

    def __init__(self,
                 image=None,
                 contour=None,
                 frame_index=None,
                 video_index=None,
                 annotations=None,
                 min_size=None,
                 normalize_area=None,
                 filename=None):
        Annotatable.__init__(self)
        Serializable.__init__(self)

        self.filename = filename

        self.annotations['video'] = video_index
        self.annotations['frame'] = frame_index

        # add annotations
        if annotations is not None:
            for key, value in annotations.items():
                self.annotations[key] = value

        if image is None and contour is None:
            raise ValueError("Cannot initialize Frame: no `image` or `contour` data has been provided.")

        if image is not None:
            if len(image.shape) != 2:
                raise ValueError(f"Cannot initialize Frame: Wrong dimensions for image {image.shape}")

            if not all(image.shape):
                raise ValueError(f"Cannot initialize Frame: At least one dimension of the input image has "
                                "length zero {image.shape}")

            num_unique_values = len(np.unique(image))
            if num_unique_values != 2:
                warnings.warn("Image must contain 2 unique values "
                            "(fore and background). Found {} unique "
                            "values.".format(num_unique_values))

            self.mask = image.astype(np.bool)
            del(image)

            if min_size is not None:
                self.mask = self._remove_small(min_size)

            self.centroid = np.mean(np.nonzero(self.mask), axis=1)

            self.contour = self.extract_contour(num_points_polygon=config['contours', 'n_points'],
                                                smoothing=config['contours', 'smoothing'],
                                                plot=False)

            self.mask = Frame._np_to_bitarray(self.mask)

        else:
            from ..utils.contours import contour_centroid, _bezier_spline
            self.mask = None

            # if contour is closed, open it
            if np.all(contour[-1] == contour[0]):
                contour = contour[1:]

            # extract spline from contour to ensure equidistance points
            contour = _bezier_spline(contour,
                                     plot=False,
                                     num_points=config['contours', 'n_points'],
                                     smoothing=config['contours', 'smoothing'])

            if contour.shape[-1] != 2 or contour.ndim != 2:
                raise ValueError(f'Cannot initialize Frame: contour has wrong shape {contour.shape}')

            self.contour = contour
            self.centroid = contour_centroid(self.contour)

        # extract shape descriptors
        # FIXME: Something in extract_shape_descriptors doesn't work with
        # zero-centered contours
        self.features, self.features_standardization = self.extract_shape_descriptors()

        # Zero-center the contour. The centroid is saved, so this is reversible
        self.contour = self.contour - self.centroid

        # Normalize the area using one of three methods
        if normalize_area is not None:
            from ..utils.contours import normalize_area as norm_area
            self.contour = norm_area(contour=self.contour,
                                     method=normalize_area)

        # will be added by videocollection.align_contours()
        self.contour_aligned = None

        # will be added by videocollection.embed_shapes()
        self.embedding = dict()

        # will be added by videocollection.plot_embedding()
        self.polygon = None

        # self.post_hooks = [self.remove_small]

    def __eq__(self, other):
        import numpy as np
        if not isinstance(other, self.__class__):
            return False

        for key in self.__dict__.keys():
            if isinstance(self.__dict__[key], np.ndarray):
                ret = np.all(self.__dict__[key] == other.__dict__[key])
            else:
                ret = self.__dict__[key] == other.__dict__[key]
            if not ret:
                return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        out = "Frame: "
        out += f"{Frame._bitarray_to_np(self.mask).shape}" \
               if self.mask is not None else "no image"
        return out

    @staticmethod
    def from_array(image, frame_index, video_index, annotations=None, min_size=64, normalize_area=None):
        '''Creates a Frame from an array of image data.

        Args:
            image (ndarray):
                The image data.
            min_size (int):
                The minimum size of objects to not be removed.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!
            Returns:
                The newly created Frame object.
        '''
        return Frame(image, contour=None,
                     frame_index=frame_index, video_index=video_index,
                     annotations=annotations,
                     min_size=min_size,
                     normalize_area=normalize_area)

    @staticmethod
    def from_file(imagePath, frame_index, video_index, annotations=None, min_size=64, normalize_area=None):
        '''Creates a Frame from an image file.

        Note:
            The image file should not contain multiple pages. Image files with
            multiple pages should be handled by Video.
        Args:
            imagePath (str): The path to the image file.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!
        Returns:
            The newly created Frame object.
        '''
        import skimage.io

        i = skimage.io.imread(imagePath)

        if (len(i.shape) == 3):
            _, _, d = i.shape
            if d == 3:
                i = skimage.color.rgb2gray(i)
            else:
                raise ValueError("3D image, but only two color components.")
        elif (len(i.shape) != 2):
            raise ValueError("Image has too many dimensions.")

        # from PIL import Image
        # i = Image.open(imagePath).convert('L')

        image = np.array(i)
        f = Frame(image=image, contour=None,
                    frame_index=frame_index, video_index=video_index,
                    annotations=annotations,
                    min_size=min_size,
                    normalize_area=normalize_area)
        f.filename = imagePath
        return f

    @staticmethod
    def from_contour(contour, frame_index, video_index, annotations=None, min_size=None, normalize_area=None):
        '''Creates a Frame from a contour.

        Args:
            contour (ndarray):
                The contour in (contour_element, 2) format.
            min_size (int):
                The minimum size of objects to not be removed.
            normalize_area (str or None):
                A string describing, how contours should be normalized. Options
                include:
                    * None: No normalization is performed
                    * "pairwise": Contours are normalized to the mean pairwise
                      distance
                    * "area": Contours are normalized so that the area of the
                      contours is 1.
                    * "rmsd": Contours are normalized so that the root mean
                      square distance (RMSD) to the centroid is 1.
                Note: This ignores information on shape sizes!
        Returns:
            The newly created Frame object.
        '''
        contour = remove_duplicate_rows(contour)
        return Frame(image=None, contour=contour,
                     frame_index=frame_index, video_index=video_index,
                     annotations=annotations,
                     min_size=min_size,
                     normalize_area=normalize_area)


    def _remove_small(self, min_size=None):
        '''Remove small objects from image

        Args:
            im (ndarray, bool): image as numpy array
            min_size (int): minimum size of objects
        Returns:
            ndarray, bool: image with small objects removed
        '''
        if min_size is not None:
            from skimage.morphology import remove_small_objects, remove_small_holes

            mask = Frame._bitarray_to_np(self.mask)
            mask = remove_small_objects(mask,
                                        min_size=min_size,
                                        connectivity=2,
                                        in_place=False)
            mask = remove_small_holes(mask,
                                      area_threshold=min_size,
                                      connectivity=2,
                                      in_place=False)
        return mask

    @staticmethod
    def _np_to_bitarray(e):
        from bitarray import bitarray
        import numpy as np
        return [bitarray(row) for row in e.astype(np.bool).tolist()]

    @staticmethod
    def _bitarray_to_np(e):
        import bitarray
        import numpy as np
        return np.array([row.tolist() for row in e], dtype=np.bool)

    def get_mask(self):
        return Frame._bitarray_to_np(self.mask)

    def get_contour_aligned(self):
        '''Returns aligned contour from frame, if it exists.

        Returns:
            aligned contour (ndarray)

        Raises:
            ValueError if contours are not yet aligned
        '''
        if self.contour_aligned is None:
            raise ValueError('Contours are not yet aligned. First run `align_contours()`.')
        return self.contour_aligned

    def get_embedding(self, source, method):
        '''Returns embedding for a given source and method, if it exists.

        Args:
            method (str): method of dimensionality reduction, one of: ['pca', 'tsne', 'umap']
            source (str): source of shape representation ['descriptors' or 'contours']
        Returns:
            embedding coordinates (ndarray)

        Raises:
            ValueError if embedding does not exist
        '''
        try:
            self.embedding[source, method]
        except KeyError:
            raise ValueError("Embedding for `{}` is not yet calculated. "
                             "First run `VideoCollection.embed_shapes()`.".format((source, method)))
        return self.embedding[source, method]

    def show(self,
             show_image=True,
             show_contour=False,
             cmap='gray_r',
             ax=None,
             figsize=(4, 4)):
        '''Plot binary image with/without contour

        Args:
            show_image (bool):
                Plot binary image
            show_contour (bool):
                Plot (unaligned) contour
            cmap (strings or matplotlib.colormap object):
                Name or object of colormap (e.g. 'gray' or 'gray_r'). See for
                names:
                https://matplotlib.org/examples/color/colormaps_reference.html
            ax (matplotlib.Axes object):
                If provided, plots into this axes
            figsize (tuple of floats):
                Size of figure
        '''

        if not (show_image or show_contour):
            raise ValueError('show_image and/or show_contour must be True')
        if show_contour:
            assert self.contour is not None
        if show_image:
            assert self.mask is not None

        from ..plotting import _show_frame

        return _show_frame(self,
                           show_image=show_image,
                           show_contour=show_contour,
                           cmap=cmap,
                           ax=ax,
                           figsize=figsize)

    def plot_contour_xy(self):
        '''Plot contour with elements color-coded as x and y values.
        Useful to explain how shapes are encoded in tensor.
        First contour element is indicated by a circle.
        '''

        fig, ax = plt.subplots(2,1,figsize=(5,10))

        contour = self.contour_aligned
        ax[0].plot(*contour.T, lw=5, zorder=0)
        ax[0].scatter(*contour.T, c=contour[:,0], s=100, cmap='viridis')
        ax[0].scatter(*contour[0].T, s=300, facecolor='none', edgecolor='black')
        ax[1].plot(*contour.T, lw=5, zorder=0)
        ax[1].scatter(*contour.T, c=contour[:,1], s=100, cmap='viridis')
        ax[1].scatter(*contour[0].T, s=300, facecolor='none', edgecolor='black')
        for a in ax: a.axis('off')

        return fig

    def extract_contour(self,
                        num_points_polygon=config['contours', 'n_points'],
                        smoothing=config['contours', 'smoothing'],
                        plot=False):
        '''Extract contour from binary image.

        Args:
            num_points_polygon (int):
                How many points the contour should consist of.
            plot (bool):
                Whether to also plot.
        Returns:
            ndarray:
                A 2 x num_points_polygon ndarray describing the points of the
                contour.
        '''

        from ..utils.contours import extract_contour
        return extract_contour(Frame._bitarray_to_np(self.mask),
                               num_points_polygon=num_points_polygon,
                               smoothing=smoothing,
                               plot=plot)

    def extract_shape_descriptors(self, skeleton=False, plot=False):
        '''Éxtract features for the Frame object.

        Returns:
            dict: A dict of features that were extracted
        '''
        # extract shape descriptors from binary images (using skimage's regionprops)
        #from ..utils.features import extract_shape_descriptors
        #import numpy as np
        #return extract_shape_descriptors(Frame._bitarray_to_np(self.mask).astype(np.integer))

        # extract shape descriptors from contours
        from ..utils.contours import extract_shape_descriptors
        return extract_shape_descriptors(self.contour, skeleton=skeleton, plot=plot)

    def get_polygon(self):
        '''Converts the contour to a matplotlib Polygon.

        Returns:
            Polygon:
                A matplotlib Polygon representing the contour.
        '''
        if self.polygon is None:
            from ..plotting.polygon import polygon

            if self.contour_aligned is None:
                raise ValueError("Aligned contour has not been calculated.")
            self.polygon = polygon(self.contour_aligned,
                                   position=(0, 0),
                                   scale=1.0,
                                   color_face=(0., 0., 1.),
                                   color_edge=(1., 1., 1.))
        return self.polygon


def remove_duplicate_rows(pts):
    u, ind = np.unique(pts, axis=0, return_index=True)
    return u[np.argsort(ind)]
