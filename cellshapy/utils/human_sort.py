

# sort files in a human/natural way, i.e. 1.tif, 2.tif, 3.tif instead of 1.tif, 10.tif, 2.tif
def atoi(text):
    return int(text) if text.isdigit() else text

def human_keys(text):
    import re
    return [ atoi(c) for c in re.split('(\d+)', text) ]

def sort_human(strings):
    '''Sort list of strings in human way.
    
    E.g. a list of filenames:
    
    1.tif
    10.tif
    100.tif
    2.tif
    
    will be sorted like:
    
    1.tif
    2.tif
    10.tif 
    100.tif
    
    Args:
    - string: list of strings
    
    Returns:
    - sorted list of strings
    '''
    strings.sort(key=human_keys)
    return strings
