import numpy as np


def extract_features(image):
    '''Extract region-based features from binary image. Extends the features
    are based on `skimage.measure.regionprops`.

    Args:
        image (ndarray, np.bool, or np.uint8):
            The image for which features should be extracted.

    Returns:
        dict:
            A dict of features for the given image.
    '''

    from skimage import measure
    from functools import reduce

    # FIXME: This will break in newer versions of skimage
    rps = measure.regionprops(label_image=image,
                              intensity_image=image,
                              coordinates='xy')

    # use the object with the maximum area
    mrp = reduce(lambda x, y: x if x.area > y.area else y, rps)

    props = [prop for prop in dir(mrp) if
             not prop.startswith('_') and
             not isinstance(mrp[prop], np.ndarray)]

    props_largest = [mrp[prop] for prop in props]

    features = dict()
    for key, val in zip(props, props_largest):
        features[key] = val

    image = mrp.image
    convex_hull_image = mrp.convex_image

    # area
    A = features['area']

    # perimeter
    P = features['perimeter']

    # perimeter of convex hull
    P_hull = measure.perimeter(convex_hull_image, neighbourhood=8)
    features['perimeter convex hull'] = P_hull

    # Area to Perimeter Ratio: A/P
    features['area_to_perimeter_ratio'] = A / P

    # Elongation: sqrt( major axis / minor axis )
    # https://en.wikipedia.org/wiki/Shape_factor_(image_analysis_and_microscopy)
    min_len = mrp['minor_axis_length']
    maj_len = mrp['major_axis_length']
    features['elongation'] = np.sqrt( maj_len / max(1, min_len) )

#    # Compactness:
#    # https://en.wikipedia.org/wiki/Shape_factor_(image_analysis_and_microscopy)
#    features['compactness'] = (A*A) / (2*np.pi*np.sqrt(min_len*min_len + maj_len*maj_len))

    # Circularity: 4A*pi / P^2
    # https://en.wikipedia.org/wiki/Shape_factor_(image_analysis_and_microscopy)
    features['circularity'] = (4.0*A*np.pi) / (P*P)

    # Thinnes ratio: 4pi*A/P^2
    # https://imagej.net/Shape_Filter
    #features['thinnes'] = 4.0*np.pi*A / (P*P)

    # Waviness:
    # https://en.wikipedia.org/wiki/Shape_factor_(image_analysis_and_microscopy)
    features['waviness'] = P / P_hull

    # Convexity based on Area: 1.0 - (A_hull-A_orig) / (A_hull)
    # A_hull = Area of Convex Hull
    #A_hull = np.count_nonzero(convex_hull_image)
    #features['area convex hull'] = A_hull

    #A_orig = np.count_nonzero(image)
    #features['convexity area'] = 1.0 - (A_hull-A_orig) / (A_hull)

    # Contour temperature
    # https://imagej.net/Shape_Filter
    eps = 1e-6
    if P_hull < P:
        features['contour temperature'] = 1.0 / (np.log2( (2.0*P)/(P-P_hull+eps))+eps)
    else:
        UserWarning('P_hull should not be smaller than P')
        features['contour temperature'] = np.nan

    # Feret diameter: https://en.wikipedia.org/wiki/Feret_diameter
    features['max_feret'] = _maxFeret(convex_hull_image)

    features.pop('bbox_area')
    features.pop('orientation')

    return features


def _maxFeret(im, plot=False, hull=False):
    '''Get Feret diameter: maximum distance between points.

    Note:
        This assumes there is only a single object is present in binary image.
        See also: https://en.wikipedia.org/wiki/Feret_diameter

    Args:
        im (ndarray):
            Image with binary (nonzero) object.
        plot (bool):
            Draw binary image with Feret diameter.

    Returns:
        float:
            The feret diameter.
    '''
    from skimage.measure import regionprops
    props = regionprops(im.astype(np.uint8))
    im_hull = props[0]['convex_image']  # Note: takes first object!

    # get edges (to make distance calculation more efficient)
    from scipy.ndimage.morphology import binary_erosion
    im_hull_erode = binary_erosion(im_hull)
    im_hull_edge = im_hull ^ im_hull_erode
    x, y = np.where(im_hull_edge > 0)
    coords = np.array([x, y]).T

    if hull:
        # to further reduce the number of points to calculate
        # pairwise distances, we take the convex hull
        from scipy.spatial import ConvexHull
        coords = np.array([x, y]).T
        hull = ConvexHull(coords)
        coords = np.array([coords[hull.vertices, 0],
                           coords[hull.vertices, 1]]).T

    # get largest distance = Feret distance
    from scipy.spatial.distance import pdist
    maxferet = pdist(coords).max()

    if plot:
        import matplotlib.pylab as plt
        # to plot the Feret diameter, get maximally distant points
        from scipy.spatial.distance import squareform
        m = squareform(pdist(coords))
        i, j = np.unravel_index(np.argmax(m), m.shape)
        p1 = np.flip(coords[i], axis=0)
        p2 = np.flip(coords[j], axis=0)
        plt.figure()
        plt.imshow(im_hull, cmap='gray_r')
        x1, y1 = [p1[0], p2[0]], [p1[1], p2[1]]
        plt.plot(x1, y1, marker='>', c='red', lw=4)

    return maxferet
