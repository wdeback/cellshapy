import numpy as np

def equalize_lengths(vids, length, imsize=None, dtype=np.uint8, n_jobs=-1):
    '''
    Trim/stretch videos and/or resize images.

    If video length < `length`, video will be streched
    If video length >= `length`, video will be trimmed to length

    Args:
        vid (ndarray): list of image stacks (videos, length, height, width)
        length (int): new length of video
        imsize (None or tuple of int): height and width. If none, does not change image size.
        dtype (numpy dtype): convert to this data type
        n_jobs (int): use multiple threads (default=-1 ('all'))
    Returns:
        resized video (ndarray) of size (length, *imsize)

    '''

    from ..config import tqdm
    from skimage.transform import resize


    def resize_video(vid, length, imsize):
        print(f'old = {len(vid)}, new = {length}')
        if imsize is None:
            imsize = vid.shape[1:]
        else:
            if len(imsize) != 2:
                raise ValueError(f'`imsize` must be a tuple of length 2 instead of {imsize}' )
        if len(vid) < length:
            # if video is shorter than length, stretch video
            print(f'Stretching video from {len(vid)} to {length} ({(length-len(vid))/len(vid):%})')
            im_equal = resize(vid, output_shape=(length,*imsize), order=0, mode='reflect', anti_aliasing=False, preserve_range=True).astype(dtype)
        else:
            print(f'Trimming video from {len(vid)} to {length} ({(length-len(vid))/len(vid):%})')
            # if video is longer than length, trim video and optionally resize height and width
            if vid.shape[1:] == imsize:
                im_equal = vid[:length]
            else:
                im_equal = resize(vid[:length], output_shape=(length,*imsize), order=0, mode='reflect', anti_aliasing=False, preserve_range=True).astype(dtype)
        return im_equal

    # run sequential or in parallel over videos
    if n_jobs == 1:
        ims_equal = [resize_video(vid, length, imsize) for vid in tqdm(vids)]
    else:
        from joblib import Parallel, delayed
        ims_equal = Parallel(n_jobs=n_jobs)(delayed(resize_video)(vid, length, imsize) for vid in tqdm(vids))

    return ims_equal