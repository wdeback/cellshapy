import numpy as np


def extract_contour(im, num_points_polygon, smoothing=0.0, plot=False):
    '''Extracts the contour from a given image.

    Note:
        If the image contains more than one connected domain, the largest one
        will be used.

    Args:
        im (array):
            The image for which the contour should be extracted.
        num_points_polygon (int):
            How many points the contour should consist of.
        plot (bool):
            Whether to also plot.

    Returns:
        ndarray:
            A 2 x num_points_ploygon ndarray describing the points of the
            contour.
    '''
    from skimage import measure

    if num_points_polygon < 3:
        raise ValueError("num_points_polygon should be at least 3")

    # zero-pad image to avoid boundary effects
    im = np.pad(im, 1, mode='constant')

    im_label = measure.label(im, background=0, connectivity=2)
    if np.max(im_label) > 1:
        import warnings
        warnings.warn('Frame contains more than one connected domain... '
                      'choosing largest')
        sizes = np.bincount(im_label.ravel())[1:]  # get blob sizes, excluding 0
        largest = np.argmax(sizes)
        im = im_label == largest+1
    else:
        im = im_label == 1

    coords = _get_ordered_coords_from_edge(im, plot=False).astype(np.float32)

    # center coordinates
    centroid = _get_centroid_image(im)
    coords -= centroid

    if coords.shape[0] > 0:
        spline = _bezier_spline(coords,
                                plot=False,
                                smoothing=smoothing,
                                num_points=num_points_polygon + 2)
    else:
        spline = coords

    if plot and coords.shape[0] > 0:
        import matplotlib.pylab as plt
        fig, ax = plt.subplots(figsize=(10, 10))
        ax.imshow(im, cmap=plt.get_cmap('gray_r'), alpha=1.0)
        spline2 = spline + centroid
        ax.plot(spline2[:, 1], spline2[:, 0], 'bo-', lw=2.0, alpha=0.5)
        ax.axis('equal')
        ax.axis('off')

    return spline[:-1]


def _get_centroid_image(im):
    '''Returns the centroid for the given image.

    Note:
        All nonzero pixels are treated as if they belong to the object.

    Args:
        im (array):
            The image for which the centroid should be calculated.

    Returns:
        ndarray:
            A 2 x 1 ndarray describing the centroid of this image.
    '''
    return np.mean(np.nonzero(im), axis=1)


def _check_clockwise(contour):
    '''Check whether contour is in clockwise order.

    See:
        https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order

    Args:
        contour (Nx2 ndarray):
            x,y coordinates along the polygon

    Returns:
        bool:
            True if the contours is in clockwise order.
    '''
    p1 = contour
    p2 = np.roll(p1, 1, axis=0)
    s = np.sum(np.multiply((p2[:, 0]+p1[:, 0]), (p2[:, 1]-p1[:, 1])))
    if s > 0:
        return True
    else:
        return False


def _make_clockwise(contour):
    '''Make the contour go in clockwise direction. Flips direction if it is
    counterclockwise.

    Args:
        contour (Nx2 ndarray):
            x,y coordinates along the polygon

    Returns:
        ndarray (Nx2):
            ndarray with x,y coordinates in clockwise order
    '''
    cw = _check_clockwise(contour)
    if not cw:
        # flip direction
        contour = contour[::-1]
    return contour

def _make_anticlockwise(contour):
    '''Make the contour go in anti-clockwise direction. Flips direction if it is
    counterclockwise.

    Args:
        contour (Nx2 ndarray):
            x,y coordinates along the polygon

    Returns:
        ndarray (Nx2):
            ndarray with x,y coordinates in clockwise order
    '''
    cw = _check_clockwise(contour)
    if cw:
        # flip direction
        contour = contour[::-1]
    return contour

def _bwperim_own(im):
    '''Get image with boundary of binary object. No longer dependent on
    mahotas.labeled.bwperim. However, computing EDT is relatively slow...

    Args:
        im (array):
            The image for which to calculate.

    Returns:
        array:
            ???
    '''
    from scipy.ndimage.morphology import distance_transform_edt
    distance = distance_transform_edt(im)
    distance[distance != 1] = 0
    return np.array(distance, np.bool)


def _get_ordered_coords_from_edge(im, plot=False):
    '''Get the coordinates of the edge of a binary blob, in clockwise order.

    Args:
        im (ndarray):
            The binary image.
        plot (bool):
            Whether to also plot.

    Returns:
        array:
            The ordered coordinates.
    '''

    # pad image with zeros to avoid detection errors at border
    # note: subtract 1 from coordinates at the end
    padwidth = 1
    im = np.pad(im, pad_width=padwidth, mode='constant', constant_values=0)
    # dilate image to prevent regions with single-point width
    # TODO: this is a brute solution to the issue of single-point width
    #       regions. Please reconsider!
    from skimage.morphology import dilation
    im = dilation(im)

    # get edge from binary image
    # use mahotas if available, otherwise, use slower bwperim function above
    try:
        from mahotas.labeled import bwperim
        im_edge = bwperim(im)
    except ImportError: #pragma: no cover
        im_edge = _bwperim_own(im)

    # get coordinates of edge
    edge_coords = np.array(np.where(im_edge > 0)).T

    if edge_coords.shape[0] == 0:
        return np.array([])
    # get pairwise distances between all coordinates of edge points
    from scipy.spatial.distance import pdist, squareform
    edge_dist = squareform(pdist(edge_coords))
    edge_dist[edge_dist == 0] = np.inf

    # list of ordered coordinates
    edge_coords_ordered = []
    # list of indices that were already visited
    visited = [0]
    max_dist_betw_points = 10.0

    dist = edge_dist[0]
    while len(edge_coords_ordered) < edge_coords.shape[0]:
        # get all visited coords to INF
        if len(visited):
            dist[np.array(visited)] = np.repeat(np.inf, len(visited))
        # get index of closest non-visited neighbor
        nb_index = np.argmin(dist)
        if dist[nb_index] > max_dist_betw_points:
            break
        # get coordinate of closest neighbor
        nb_coord = edge_coords[nb_index]
        # add the neighbor coords to list
        edge_coords_ordered.append(nb_coord.tolist())
        # add the index to the list of visited points
        visited.append(nb_index)

        dist = edge_dist[nb_index]

    # close the polygon
    edge_coords_ordered.append(edge_coords_ordered[0])
    # convert to ndarray
    edge_coords_ordered = np.array(edge_coords_ordered)
    # ensure clockwise order
    edge_coords_ordered = _make_clockwise(edge_coords_ordered)

    if plot:
        import matplotlib.pylab as plt
        fig, ax = plt.subplots(figsize=(10, 10))
        ax.imshow(im, cmap=plt.get_cmap('gray_r'))
        ax.plot(edge_coords_ordered[:, 1], edge_coords_ordered[:, 0], lw=4)
        plt.setp(ax.get_yticklabels(), visible=False)
        plt.setp(ax.get_xticklabels(), visible=False)
        # ax.invert_yaxis()

    # subtract 1 from each x,y coordinate to compensate for zero-padding
    edge_coords_ordered = np.subtract(edge_coords_ordered, padwidth)

    return edge_coords_ordered


def _bezier_spline(points, smoothing, num_points, plot=False):
    '''Construct a bezier spline from points.

    Ref:
        https://stackoverflow.com/questions/31464345/fitting-a-closed-curve-to-a-set-of-points


    Args:
        points (ndarray):
            Coordinate to fit spline.
        smoothing (float):
            ???
        num_points (int):
            Number of points in spline.
        plot (bool):
            Whether to also plot.
    Returns:
        ndarray (2 x num_points):
            coordinates of spline

    '''

    points = _make_anticlockwise(points)

    points = close_contour(points)

    from scipy.interpolate import splprep, splev
    tck, u = splprep(points.T, u=None, s=smoothing, per=1, k=3, quiet=True)
    u_new = np.linspace(u.min(), u.max(), num_points-1)
    x_new, y_new = splev(u_new, tck, der=0)
    spline = np.array([x_new, y_new]).T

    if plot:
        import matplotlib.pylab as plt
        # plt.plot(pts[:,0], pts[:,1], 'ro')
        plt.plot(x_new, y_new, 'b--')
        plt.plot(x_new, y_new, 'ro')

        labels = range(u_new.shape[0])
        for label, x, y in zip(labels, x_new, y_new):
            plt.annotate(
                label,
                xy=(x, y), xytext=(0, 5),
                textcoords='offset points', ha='right', va='bottom')
    return spline

# ----------------------------------------------------------------

def extract_shape_descriptors(contour, skeleton=False, fourier_order=-1, return_standardization=True, plot=False):
    '''
    Extract shape descriptors from a contour.

    Descriptors:
    - Contour:
        - area: area within contour (Shoelace algorithm)
        - perimeter: perimeter of contour (contour length)
        - perimeter to area ratio: ratio of perimeter to area

    - Bounding box approximation: smallest (rotated) rectangle containing contour
        - maximum caliper (=Feter) diameter: maximum distance between contour elements
        - minimum caliper (=Feter) diameter: distance between contour elements orthogonal to maximmum caliper diameter
        - aspect ratio: ratio of maximum and minimum caliper diameters
        - area of bounding box: product of maximum and minimum caliper diameters
        - perimeter of bounding box: twice the sum of maximum and minimum caliper diameters

    - Convex hull approximation: set of outer contour points
        - area of convex hull: area within convex hull
        - perimeter of convex hull: length of contour of convex hull
        - waviness: ratio of perimeter of convex hull and perimeter of contour, interval <0,1>
        - solidity: ratio of area of contour and area of convex hull, interval <0,1>

    - Circular approximation: fitted circle
        - equivalent diameter: diameter of circle with same area as contour
        - circularity (=isoperimetric quotient): ratio of the contour area to the area of a circle with same perimeter
        - maximum inscribing circle diameter: diameter of largest inscribing circle (calculated from Voronoi diagram)

    - Elliptic approximation: fitted ellipse to region within contours, calculated with moments of inertia
        - major axis length: length of the major (long) axis of ellipse
        - minor axis length: length of the major (short) axis of ellipse
        - eccentricity: ratio of the distance between the foci and the length of the major axis
        - elongation: ratio of the length of the major and minor axes, minus one

    - (Optional) Skeleton approximation: medial axis of contour, pruned using Discrete Skeleton Evolution
        - length of skeleton: total length of skeleton
        - number of branches: number of branches of skeleton
        - mean distance: mean of minimal distances of skeleton elements to contour

    - Miscellaneous:
        - contour temperature: shape descriptor related to fractal dimension (L. da Fontoura Costa, R. Marcondes Cesar, Shape Classification and Analysis, 2009.)
        - (mean curvature: currently disables)

    Args:
        - contour (array):
            Nx2 array with clockwise-ordered coordinates of contour
        - skeleton (bool, default=False):
            If true, compute medial axis and derived descriptors (note: slow!)
        - fourier_order (int, default=-1):
            Order of elliptic Fourier descriptors to extract. If fourier_order <= 0, no elliptic Fourier decomposition is performed.
        - plot (book, default=False):
            If true, generates plots with shape descriptions
    Returns:
        dict
    '''

    # check ordered
    if not _check_clockwise(contour):
        contour = _make_clockwise(contour)
    # check open/closed polygon
    contour = close_contour(contour)

    if plot:
        import matplotlib.pylab as plt
        fig, ax = plt.subplots(4,4,figsize=(16,16))
        fig.tight_layout()
        ax = ax.flatten()

    ## Contour descriptors ##
    A = contour_area(contour, plot=plot, ax=ax[0] if plot else None)
    P = contour_perimeter(contour, plot=plot, ax=ax[1] if plot else None)
    perimeter_to_area_ratio = P / A

    ## Bounding box approximation ##
    min_caliper, max_caliper = caliper_diameter(contour, plot=plot, ax=ax[8] if plot else None)
    aspect_ratio = max_caliper / min_caliper
    area_bbox = max_caliper * min_caliper
    perimeter_bbox = 2.0 * (max_caliper+min_caliper)

    ## Convex hull approximation
    # convex hull area and perimeter
    hull_contour, A_hull, P_hull = convex_hull_area_perimeter(contour, plot=plot, axes=ax[2:4] if plot else None)
    # waviness: https://en.wikipedia.org/wiki/Shape_factor_(image_analysis_and_microscopy)
    waviness = 1.0 - (P_hull / P)
    if plot: plot_waviness(contour, ax=ax[4] if plot else None)
    # solidity: ratio of area over convex hull area
    solidity = (A / A_hull)
    if plot: plot_solidity(contour, ax=ax[5] if plot else None)
    # Number of points on the convex hull
    hull_points = len(hull_contour)

    ## Circular approximation
    # equivalent_diameter: diameter of circle with same area
    equivalent_diameter = 2.0 * np.sqrt(A / np.pi)
    if plot: plot_equivalent_diameter(contour, equivalent_diameter, ax=ax[6] if plot else None)
    # circularity: https://en.wikipedia.org/wiki/Shape_factor_(image_analysis_and_microscopy)
    circularity = (4.0*A*np.pi) / (P*P)
    # diameter largest inscribing circle
    inscribing_circle_diameter = inscribing_circle(contour, plot=plot, ax=ax[7] if plot else None)

    ## Elliptic approximation
    major_axis_length, minor_axis_length, eccentricity = moments_of_inertia(contour, plot=plot, ax=ax[9] if plot else None)
    # elongation: https://en.wikipedia.org/wiki/Shape_factor_(image_analysis_and_microscopy)
    eps = 1e-6
    elongation = (major_axis_length / (minor_axis_length+eps))-1.0

    # Triangular approximation using O'Rourke's algorithm (see external/minimum_enclosing_triangle)
    try:
        from ..external.minimum_enclosing_triangle.min_triangle import minTriangleWrapper
        minimal_enclosing_triangle = minTriangleWrapper(hull_contour)
        triangularity_area = 2.0 * ((A / contour_area(minimal_enclosing_triangle)+1e-12) - 0.5)
        if np.isinf(triangularity_area) or np.isnan(triangularity_area):
            triangularity_area = 0.0
    except:
        import warnings
        warnings.warn('minTriangleWrapper failed... Setting triangularity_area=0.0')
        triangularity_area = 0.0

    if plot:
        plot_minimal_enclosing_triangle(contour, minimal_enclosing_triangle, ax=ax[10])
    #end = time.time()
    #print('triangularity exec time = {:.3f}s'.format(end-start))

    ## Skeleton approximation
    if skeleton:
        from ..utils.skeleton import extract_skeleton
        _, skeleton_length, skeleton_branches, skeleton_mean_distance \
                = extract_skeleton(contour, plot=plot, prune_threshold = 2e-3, ax=ax[12] if plot else None)

    # contour temperature: https://imagej.net/Shape_Filter
    #contour_temperature = 1.0 /(np.log((2.0*P)/(abs(P-P_hull)))/np.log(2.0))

    # mean local curvature
    mean_curvature = np.mean(get_curvature(contour, signed=False))
    if plot:
        plot_curvature(contour, ax=ax[11])

    if fourier_order > 0:
        # Elliptic Fourier descriptors (requires pyefd)
        try:
            import pyefd
            elliptic_fourier = efd_features(contour, order=fourier_order)
            if plot:
                plot_elliptic_fourier(contour, order=fourier_order, ax=ax[11])
        except ImportError:
            warnings.warn('Computing elliptic Fourier descriptors requires `pyefd` package. Install with: `pip install pyefd`.')

    if plot:
        for a in ax[13:]:
            a.set_visible(False)
        if not skeleton:
            ax[12].set_visible(False)

    # ------------------------

    features = dict()
    standardization = dict() # whether or not variable should be standardized

    # region properties
    features['area'] = A
    features['perimeter'] = P
    features['perimeter to area ratio'] = perimeter_to_area_ratio

    # bounding box approximation
    features['area bbox'] = area_bbox
    features['perimeter bbox'] = perimeter_bbox
    features['max caliper diameter'] = max_caliper
    features['min caliper diameter'] = min_caliper
    features['aspect ratio'] = aspect_ratio

    # convex hull approximation
    features['area hull'] = A_hull
    features['perimeter hull'] = P_hull
    features['solidity'] = solidity
    standardization['solidity'] = False
    features['waviness'] = waviness
    standardization['waviness'] = False
    #features['hull points'] = hull_points
    #standardization['hull points'] = False

    # circular approximation
    features['equivalent diameter'] = equivalent_diameter
    features['circularity'] = circularity
    standardization['circularity'] = False
    features['inscribing circle diameter'] = inscribing_circle_diameter

    # elliptic approximation
    features['major axis length'] = major_axis_length
    features['minor axis length'] = minor_axis_length
    features['eccentricity'] = eccentricity
    standardization['eccentricity'] = False
    features['elongation'] = elongation

    # triangular approximation
    features['triangularity'] = triangularity_area

    # skeleton approximation
    if skeleton:
        features['length skeleton'] = skeleton_length
        features['number branches skeleton'] = skeleton_branches
        features['mean distance skeleton'] = skeleton_mean_distance

    # miscellaneous
    #features['contour temperature'] = contour_temperature
    features['mean curvature'] = mean_curvature

    if fourier_order > 0:
        for i, efd in enumerate(elliptic_fourier):
            features[f'elliptic fourier {i}'] = efd

    # set standardization of all columns to True, except those already set
    for f in set(features.keys()) - set(standardization.keys()):
        standardization[f] = True

    if return_standardization:
        return features, standardization
    else:
        return features

def plot_features(contour, extract_skeleton=False):
    _ = extract_features(contour, extract_skeleton=False, plot=True)


# ----------------------------------------------------------------

def contour_area(contour, plot=False, ax=None):
    '''
    Calculates the area of a given contour, based on the shoelace formula (https://en.wikipedia.org/wiki/Shoelace_formula)
    Numpy implementation: https://stackoverflow.com/a/30408825

    Note: This method assumes contours (1) do not self-intertersect and (2) do not have holes.

    Args:
        contour (array, Nx2):
            Contour described by an *ordered* list of 2D coordinates
    Returns:
        area (float):
            Area
    '''
    import numpy as np
    x, y = contour[:,0], contour[:,1]
    area = 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))
    # TODO: fallback for area in case of 4-point contours
    #area += 1e-12

    if plot:
        import matplotlib.pylab as plt
        if ax is None:
            fig, ax = plt.subplots(nrows=1, ncols=1)
        c = close_contour(contour)
        ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)
        ax.fill(*c.T, 'red', alpha=1.0)
        ax.axis('equal')
        ax.axis('off')
        ax.set_title('Area', fontsize=18)


    return area

# ----------------------------------------------------------------

def contour_perimeter(contour, plot=False, ax=None):
    '''
    Calculate the perimeter of a given contour.
    Note: assumes an ordered list of coordinates.
    Note: also works for closed polygons.

    Args:
        contour (array, Nx2):
            Contour described by an *ordered* list of 2D coordinates
    Returns:
        perimeter (float):
            Perimeter
    '''
    a = contour
    b = np.roll(contour,1,axis=0)
    perimeter = np.sum(np.linalg.norm(a - b, axis=1))

    if plot:
        import matplotlib.pylab as plt
        if ax is None:
            fig, ax = plt.subplots(nrows=1, ncols=1)
        c = close_contour(contour)
        ax.plot(*c.T, 'r-', lw=5, alpha=1.0)
        ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)
        ax.axis('equal')
        ax.axis('off')
        ax.set_title('Perimeter', fontsize=18)

    return perimeter

# ----------------------------------------------------------------

def get_hull(contour):
    from scipy.spatial import ConvexHull
    hull = ConvexHull(contour)
    return contour[hull.vertices]


# ----------------------------------------------------------------


def convex_hull_area_perimeter(contour, plot=False, axes=None):

    from scipy.spatial import ConvexHull
    hull = ConvexHull(contour)
    hull_contour = contour[hull.vertices]
    hull_area = hull.volume
    hull_perimeter = hull.area

    if plot:
        import matplotlib.pylab as plt
        if axes is None:
            fig, ax = plt.subplots(nrows=1, ncols=1)
        else:
            ax = axes[0]
        c = close_contour(contour)
        hc = close_contour(hull_contour)
        ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)
        ax.fill(*hc.T, 'red', alpha=1.0)
        ax.axis('equal')
        ax.axis('off')
        ax.set_title('Area convex hull', fontsize=18)

        if axes is None:
            fig, ax = plt.subplots(nrows=1, ncols=1)
        else:
            ax = axes[1]
        ax.plot(*hc.T, 'r-', lw=5, alpha=1.0)
        ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)
        ax.axis('equal')
        ax.axis('off')
        ax.set_title('Perimeter convex hull', fontsize=18)

    return hull_contour, hull_area, hull_perimeter


def caliper_diameter(contour, plot=False, ax=None):

    hull = get_hull(contour)

    from scipy.spatial.distance import pdist, squareform
    # pairwise distance matrix
    distances = squareform(pdist(hull))
    # maximum caliper distance is maximum in distance matrix
    max_caliper_distance = np.max(distances)

    # get indices for points with the maximum distance
    indices = np.array(np.unravel_index(np.argmax(distances), distances.shape))
    # get angle between points and y-axis
    angle = angle_between((0.,1.), hull[indices][0]-hull[indices][1])
    # rotate the hull such that the max caliper distance is vertical
    hull_rot = rotate(hull, (0.,1.), -angle)

    # minimum caliper: perpendicular to max caliper distance
    # get the min caliper distance as the diff between the min and max x coordinates of rotated hull
    min_caliper_distance = np.max(hull_rot[:,0]) - np.min(hull_rot[:,0])


    if plot:
        import matplotlib.pylab as plt
        if ax is None:
            fig, ax = plt.subplots(nrows=1, ncols=1)

        # plot bounding box
        xmin, ymin = np.min(hull_rot[:,0]), np.min(hull_rot[:,1])
        ax = draw_rectangle(xmin, ymin, width=max_caliper_distance,
                                        height=min_caliper_distance,
                                        angle=angle,
                                        ax=ax)

        # plot contour
        c = close_contour(contour)
        ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)

        # plot maximum caliper distance
        max_caliper = np.array([hull[indices,0], hull[indices,1]])
        ax.plot(*max_caliper, 'r-', lw=4)

        # plot minimum caliper distance
        y = np.mean(hull_rot[indices,1])
        xmin, xmax = np.min(hull_rot[:,0]), np.max(hull_rot[:,0])
        min_caliper = np.array([[xmin, y], [xmax, y]])
        min_caliper = rotate(min_caliper, (0., 1.), angle)
        ax.plot(*min_caliper.T, 'g-', lw=4)
        ax.axis('equal')
        ax.axis('off')
        ax.set_title('Max/min caliper distance', fontsize=18)

    return min_caliper_distance, max_caliper_distance

def draw_rectangle(xmin, ymin, width, height, angle, lw=3, color='black', ax=None):
    '''draw rectangle rotated around center (unlike patches.Rectangle)'''

    if ax is None:
        fig, ax = plt.subplots(1,1)

    if width > height:
        temp = height
        height = width
        width = temp

    bl = np.array([xmin, ymin])
    br = np.array([xmin+width, ymin])
    tr = np.array([xmin+width, ymin+height])
    tl = np.array([xmin, ymin+height])
    rect = np.array([bl, br, tr, tl])
    rect = close_contour(rect)
    rect = rotate(rect, (0.,1.), angle)
    ax.plot(*rect.T, linewidth=lw, color=color)
    return ax

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(p1, p2):
    ang1 = np.arctan2(*p1[::-1])
    ang2 = np.arctan2(*p2[::-1])
    return (ang1 - ang2) % (2 * np.pi)


def rotate(points, center, angle):
    c, s = np.cos(angle), np.sin(angle)
    R = np.array(((c,-s), (s, c)))
    return np.dot(points-center, R)+center

def close_contour(contour):
    if np.any(contour[0, :] != contour[-1, :]):
        return np.vstack([contour, contour[0]])
    else:
        return contour


def contour_centroid(contour):
    import numpy as np

    # Requires a counter-clockwise, closed polygon
    if _check_clockwise(contour):
        contour = contour[::-1]
    if np.any(contour[0, :] != contour[-1, :]):
        close_contour(contour)

    A = contour_area(contour)

    x0 = contour[:, 0]
    y0 = contour[:, 1]
    x1 = np.roll(x0, 1)
    y1 = np.roll(y0, 1)

    C_x = 1/(6*A) * np.sum((x0 + x1)*(x0*y1 - x1*y0))
    C_y = 1/(6*A) * np.sum((y0 + y1)*(x0*y1 - x1*y0))

    return (C_x, C_y)


def inscribing_circle(contour, plot=False, ax=None):

    # create voronoi diagram from contour
    from scipy.spatial import Voronoi
    vor = Voronoi(contour)
    x, y = vor.vertices[:,0], vor.vertices[:,1]

    # get Voronoi vertices inside of contour (negative distance)
    distances = is_inside_polygon(x, y, contour)
    inside = distances < 0
    vertices_inside = vor.vertices[inside]
    distances = -distances[inside]

    if len(distances) == 0:
        return 0.

    # largest inscribing circle
    index = np.argmax(distances)
    radius = distances[index]
    center_circle = vertices_inside[index]

    if plot:
        import matplotlib.pylab as plt
        if ax is None:
            fig, ax = plt.subplots(1,1)
        # plot closed contour
        contour_c = close_contour(contour)
        ax.plot(*contour_c.T, 'bo-', alpha=0.5)
        # plot center of inscribing circle
        ax.scatter(*center_circle.T, s=100, color='black', alpha=1.0)
        # plot diameter of inscribing circle
        left = [(center_circle[0]-radius), center_circle[1]]
        ax.arrow(*left, radius*2, 0., color='red', lw=3)
        # plot circle
        from matplotlib.patches import Circle
        c = Circle(center_circle, radius=radius, fill=False, linewidth=3, zorder=0)
        ax.add_patch(c)
        ax.axis('equal')
        ax.axis('off')
        ax.set_title('Largest inscribing circle', fontsize=18)

    return radius*2

def efd_features(contour, order=10):
    from pyefd import elliptic_fourier_descriptors
    coeffs = elliptic_fourier_descriptors(contour, order=order, normalize=True)
    return coeffs.flatten()[3:]

def moments_of_inertia(contour, plot=False, ax=None):
    import numpy as np

    # Requires a counter-clockwise, closed polygon
    if _check_clockwise(contour):
        contour = contour[::-1]
    if np.any(contour[0, :] != contour[-1, :]):
        close_contour(contour)
    # We'll do some floating-point math
    contour = contour.astype(np.float64)

    # Center around the origin
    centroid = contour_centroid(contour)

    contour -= np.array(centroid)

    # Calculate the moment of interia tensor
    # See https://en.wikipedia.org/wiki/Second_moment_of_area#Any_polygon
    x0 = contour[:, 0]
    y0 = contour[:, 1]
    x1 = np.roll(x0, 1)
    y1 = np.roll(y0, 1)

    ai  = x0*y1 - x1*y0
    Ix  = 1/12 * np.sum((y0*y0 + y0*y1 + y1*y1) * ai)
    Iy  = 1/12 * np.sum((x0*x0 + x0*x1 + x1*x1) * ai)
    Ixy = 1/24 * np.sum((x0*y1 + 2*x0*y0 + 2*x1*y1 + x1*y0) * ai)

    mofi = np.array([[Ix, Ixy],
                     [Ixy, Iy]])

    # Find the rotation matrix and lengths of the semi-principal axes
    # by singular value decomposition
    _, s, rot = np.linalg.svd(mofi)

    A = contour_area(contour)
    major_length, minor_length = 4.0 * np.sqrt(s/A)

    eccentricity = np.sqrt(1.0 - (s[1] / s[0]))

    if plot:
        import matplotlib.pylab as plt
        from matplotlib import patches
        contour += centroid


        major_vector = np.array([Ixy, s[0]-Iy])
        minor_vector = np.array([Ixy, s[1]-Iy])

        major_vector *= (major_length/2) / np.linalg.norm(major_vector)
        minor_vector *= (minor_length/2) / np.linalg.norm(minor_vector)

        rot = np.rad2deg(np.arctan2(-Ixy, (Ix - Iy)/2)/2)

        if ax is None:
            fig, ax = plt.subplots(nrows=1, ncols=1)

        c = close_contour(contour)
        ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)
        e = patches.Ellipse(centroid,
                            width=minor_length,
                            height=major_length,
                            angle=rot,
                            fill=False,
                            linewidth=3,
                            color="black")
        ax.add_patch(e)

        ax.scatter(*centroid, s=100, color='black', alpha=1.0)
        ax.arrow(*centroid, *major_vector, color='red', lw=3)
        ax.arrow(*centroid, *minor_vector, color='green', lw=3)
        ax.axis('equal')
        ax.axis('off')
        ax.set_title('Major / minor axes', fontsize=18)

    return major_length, minor_length, eccentricity


def curvature_spline(x, y=None, error=0.0, signed=True):
    """Calculate the signed curvature of a 2D curve at each point
    using interpolating splines.

    See: https://gist.github.com/elyase/451cbc00152cb99feac6

    Parameters
    ----------
    x,y: numpy.array(dtype=float) shape (n_points, )
         or
         y=None and
         x is a numpy.array(dtype=complex) shape (n_points, )
         In the second case the curve is represented as a np.array
         of complex numbers.
    error : float
        The admisible error when interpolating the splines
    Returns
    -------
    curvature: numpy.array shape (n_points, )
    Note: This is 2-3x slower (1.8 ms for 2000 points) than `curvature_gradient`
    but more accurate, especially at the borders.
    """
    from scipy.interpolate import UnivariateSpline #,InterpolatedUnivariateSpline

    x, y = x[:,0], x[:,1]
    # handle list of complex case
    #if y is None:
    #    x, y = x.real, x.imag

    t = np.arange(x.shape[0])
    std = error * np.ones_like(x)
    fx = UnivariateSpline(t, x, k=3, w=1 / np.sqrt(std))
    fy = UnivariateSpline(t, y, k=3, w=1 / np.sqrt(std))

    #fx = InterpolatedUnivariateSpline(t, x, k=2)
    #fy = InterpolatedUnivariateSpline(t, y, k=2)

    xˈ = fx.derivative(1)(t)
    xˈˈ = fx.derivative(2)(t)
    yˈ = fy.derivative(1)(t)
    yˈˈ = fy.derivative(2)(t)
    if signed:
        curvature = (xˈ* yˈˈ - yˈ* xˈˈ) / np.power(xˈ** 2 + yˈ** 2, 3 / 2)
    else:
        curvature = np.abs(xˈ* yˈˈ - yˈ* xˈˈ) / np.power(xˈ** 2 + yˈ** 2, 3 / 2)
    curvature = np.clip(curvature, a_min=-np.pi, a_max=np.pi)
    return curvature

def get_curvature(contour, signed):

    # resample the shape with
    c = curvature_spline(contour, error=0.001, signed=signed) #/ np.pi

    #from ..analysis.timeseries import _autocorrelation
    #_autocorrelation(c, plot=True)
    return c

#------------------------------------------------------------

def plot_minimal_enclosing_triangle(contour, triangle, ax=None):
    import matplotlib.pylab as plt
    if ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1)

    tri = close_contour(triangle)
    ax.plot(*tri.T, 'red', lw=5.0, alpha=1.0)

    c = close_contour(contour)
    ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)

    ax.set_title('Triangularity', fontsize=18)
    ax.axis('equal')
    ax.axis('off')

def plot_solidity(contour, ax=None):
    import matplotlib.pylab as plt
    if ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1)

    h = get_hull(contour)
    h = close_contour(h)
    ax.fill(*h.T, 'red', alpha=1.0)

    c = close_contour(contour)
    ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)
    ax.fill(*c.T, 'blue', alpha=0.5)

    ax.set_title('Solidity', fontsize=18)
    ax.axis('equal')
    ax.axis('off')

def plot_waviness(contour, ax=None):
    import matplotlib.pylab as plt
    if ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1)

    h = get_hull(contour)
    h = close_contour(h)
    ax.plot(*h.T, 'r-', lw=5, alpha=1.0)
    ax.plot(*h.T, 'ro-', lw=2, alpha=0.5)

    c = close_contour(contour)
    ax.plot(*c.T, 'b-', lw=5, alpha=0.5)
    ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)

    ax.set_title('Waviness', fontsize=18)
    ax.axis('equal')
    ax.axis('off')

def plot_equivalent_diameter(contour, diameter, ax=None):

    import matplotlib.pylab as plt
    from matplotlib import patches
    if ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1)

    centroid = contour_centroid(contour)
    #contour -= centroid

    c = close_contour(contour)
    ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)

    radius = diameter / 2
    ax.scatter(*centroid, s=100, color='black', alpha=1.0)
    left = [(centroid[0]-radius), centroid[1]]
    ax.arrow(*left, diameter, 0., color='red', lw=3)

    patch = patches.Circle(centroid, radius=radius,
                            linewidth=3,
                            fill=False,
                            color="black")
    ax.add_patch(patch)

    ax.axis('equal')
    ax.axis('off')
    ax.set_title('Equivalent diameter', fontsize=18)

def plot_curvature(contour, ax=None):

    curvature = get_curvature(contour, signed=False)
    # calculate 5% and 95% percentiles (for robust colormapping)
    vmin = np.percentile(curvature, 5)
    vmax = np.percentile(curvature, 95)

    import matplotlib.pylab as plt
    from matplotlib import patches
    if ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1)

    centroid = contour_centroid(contour)
    #c = close_contour(contour)
    #ax.plot(*c.T, 'bo', lw=2, alpha=0.5)
    norm = plt.Normalize(vmin, vmax)
    ax.scatter(*contour.T, s=50, cmap='viridis', c=curvature, alpha=1.0, norm=norm)

    from matplotlib.collections import LineCollection

    # plot colored lines between midpoints
    def interweave(a,b):
        c = np.empty((a.shape[0] + b.shape[0], 2), dtype=a.dtype)
        c[0::2] = a
        c[1::2] = b
        return c

    # add mid points
    contour_midpoints = contour[:-1] + np.diff(contour, axis=0)/2
    # interweave them with the normal contour points
    contour = interweave(contour, contour_midpoints)

    # repeat the curvature measurements to include the midbpoints
    curvature = np.repeat(curvature,2,axis=0)
    # shift the values 1 position
    curvature = np.roll(curvature,-1)

    # center the contour
    #contour -= centroid
    # reshape from contour points to coordinates of line segments
    points = contour.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    # plot color lines
    lc = LineCollection(segments,
                    linestyle='solid',
                    linewidths=5,
                    alpha=1.0,
                    cmap='viridis',
                    zorder=10, norm=norm)
    lc.set_capstyle('butt')
    lc.set_array(curvature)
    line = ax.add_collection(lc)

    ax.axis('equal')
    ax.axis('off')
    ax.set_title('Curvature', fontsize=18)

def plot_elliptic_fourier(contour, order, n_points=50, ax=None):

    from pyefd import elliptic_fourier_descriptors, calculate_dc_coefficients, plot_efd
    coeffs = elliptic_fourier_descriptors(contour, order=order, normalize=False)

    # reconstruct contour from elliptic Fourier descriptors
    locus = calculate_dc_coefficients(contour)
    xt = np.ones((n_points,)) * locus[0]
    yt = np.ones((n_points,)) * locus[1]
    t = np.linspace(0, 1.0, n_points)
    for n in range(coeffs.shape[0]):
        xt += (coeffs[n, 0] * np.cos(2 * (n + 1) * np.pi * t)) + \
              (coeffs[n, 1] * np.sin(2 * (n + 1) * np.pi * t))
        yt += (coeffs[n, 2] * np.cos(2 * (n + 1) * np.pi * t)) + \
              (coeffs[n, 3] * np.sin(2 * (n + 1) * np.pi * t))
    reconstruction = np.array([xt, yt]).T

    import matplotlib.pylab as plt
    from matplotlib import patches
    if ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1)

    c = close_contour(contour)
    ax.plot(*c.T, 'bo-', lw=2, alpha=0.5)

    # plot reconstruction
    r = reconstruction
    r = close_contour(reconstruction)
    ax.plot(*r.T, 'r-', lw=5, alpha=1.0)
    ax.plot(*r.T, 'ro-', lw=2, alpha=0.5)

    ax.set_title(f'Elliptic Fourier\n(order = {order})', fontsize=18)
    ax.axis('equal')
    ax.axis('off')



#------------------------------------------------------------

def to_mask(contour, plot=False, ax=None):
    '''Generate raster image from contour

    Args:
        contour (Nx2 array):
            List of contour elements
    Returns:
        image (array):
            Binary image with contour shape, 1 pixel zero padding
    '''

    from skimage.draw import polygon
    contour -= np.min(contour, axis=0)
    rr, cc = polygon(*contour.T)
    imsize = np.array([np.max(contour[:,1])+2,
                       np.max(contour[:,0])+2], np.uint16)
    im = np.zeros(imsize, np.uint8)
    im[cc+1,rr+1]=1

    if plot:
        import matplotlib.pylab as plt
        if ax is None:
            fig, ax = plt.subplots(nrows=1, ncols=1)
        ax.imshow(im, alpha=0.25, cmap='gray_r')
        ax.plot(*contour.T, lw=5, c='r')
        ax.axis('off')

    contour -= np.mean(contour, axis=0)

    return im

# ------------- helper functions -------------


def is_inside_polygon(xpoint, ypoint, contour, smalld=1e-12):
    '''Check if point is inside a general polygon.
    https://code.activestate.com/recipes/578381-a-point-in-polygon-program-sw-sloan-algorithm/

    Input parameters:

    xpoint -- The x-coord of the point to be tested.
    ypoint -- The y-coords of the point to be tested.
    smalld -- A small float number.

    xpoint and ypoint could be scalars or array-like sequences.

    Output parameters:

    mindst -- The distance from the point to the nearest point of the
              polygon.
              If mindst < 0 then point is outside the polygon.
              If mindst = 0 then point in on a side of the polygon.
              If mindst > 0 then point is inside the polygon.

    Notes:

    An improved version of the algorithm of Nordbeck and Rydstedt.

    REF: SLOAN, S.W. (1985): A point-in-polygon program. Adv. Eng.
         Software, Vol 7, No. 1, pp 45-47.

    '''

    import numpy as np

    contour = close_contour(contour)
    contour = _make_anticlockwise(contour)
    x = contour[:,0]
    y = contour[:,1]


    xpoint = np.asfarray(xpoint)
    ypoint = np.asfarray(ypoint)
    # Scalar to array
    if xpoint.shape is tuple():
        xpoint = np.array([xpoint], dtype=float)
        ypoint = np.array([ypoint], dtype=float)
        scalar = True
    else:
        scalar = False
    # Check consistency
    if xpoint.shape != ypoint.shape:
        raise IndexError('x and y has different shapes')
    # If snear = True: Dist to nearest side < nearest vertex
    # If snear = False: Dist to nearest vertex < nearest side
    snear = np.ma.masked_all(xpoint.shape, dtype=bool)
    # Initialize arrays
    mindst = np.ones_like(xpoint, dtype=float) * np.inf
    j = np.ma.masked_all(xpoint.shape, dtype=int)
    n = len(x) - 1  # Number of sides/vertices defining the polygon
    # Loop over each side defining polygon
    for i in range(n):
        d = np.ones_like(xpoint, dtype=float) * np.inf
        # Start of side has coords (x1, y1)
        # End of side has coords (x2, y2)
        # Point has coords (xpoint, ypoint)
        x1 = x[i]
        y1 = y[i]
        x21 = x[i + 1] - x1
        y21 = y[i + 1] - y1
        x1p = x1 - xpoint
        y1p = y1 - ypoint
        # Points on infinite line defined by
        #     x = x1 + t * (x1 - x2)
        #     y = y1 + t * (y1 - y2)
        # where
        #     t = 0    at (x1, y1)
        #     t = 1    at (x2, y2)
        # Find where normal passing through (xpoint, ypoint) intersects
        # infinite line
        t = -(x1p * x21 + y1p * y21) / (x21 ** 2 + y21 ** 2)
        tlt0 = t < 0
        tle1 = (0 <= t) & (t <= 1)
        # Normal intersects side
        d[tle1] = ((x1p[tle1] + t[tle1] * x21) ** 2 +
                   (y1p[tle1] + t[tle1] * y21) ** 2)
        # Normal does not intersects side
        # Point is closest to vertex (x1, y1)
        # Compute square of distance to this vertex
        d[tlt0] = x1p[tlt0] ** 2 + y1p[tlt0] ** 2
        # Store distances
        mask = d < mindst
        mindst[mask] = d[mask]
        j[mask] = i
        # Point is closer to (x1, y1) than any other vertex or side
        snear[mask & tlt0] = False
        # Point is closer to this side than to any other side or vertex
        snear[mask & tle1] = True
    if np.ma.count(snear) != snear.size:
        raise IndexError('Error computing distances')
    mindst **= 0.5
    # Point is closer to its nearest vertex than its nearest side, check if
    # nearest vertex is concave.
    # If the nearest vertex is concave then point is inside the polygon,
    # else the point is outside the polygon.
    jo = j.copy()
    jo[j == 0] -= 1
    area = _det([x[j + 1], x[j], x[jo - 1]], [y[j + 1], y[j], y[jo - 1]])
    mindst[~snear] = np.copysign(mindst, area)[~snear]
    # Point is closer to its nearest side than to its nearest vertex, check
    # if point is to left or right of this side.
    # If point is to left of side it is inside polygon, else point is
    # outside polygon.
    area = _det([x[j], x[j + 1], xpoint], [y[j], y[j + 1], ypoint])
    mindst[snear] = np.copysign(mindst, area)[snear]
    # Point is on side of polygon
    mindst[np.fabs(mindst) < smalld] = 0
    # If input values were scalar then the output should be too
    if scalar:
        mindst = float(mindst)
    return mindst


def _det(xvert, yvert):
    '''Compute twice the area of the triangle defined by points with using
    determinant formula.
    https://code.activestate.com/recipes/578381-a-point-in-polygon-program-sw-sloan-algorithm/

    Input parameters:

    xvert -- A vector of nodal x-coords (array-like).
    yvert -- A vector of nodal y-coords (array-like).

    Output parameters:

    Twice the area of the triangle defined by the points.

    Notes:

    _det is positive if points define polygon in anticlockwise order.
    _det is negative if points define polygon in clockwise order.
    _det is zero if at least two of the points are concident or if
        all points are collinear.

    '''
    xvert = np.asfarray(xvert)
    yvert = np.asfarray(yvert)
    x_prev = np.concatenate(([xvert[-1]], xvert[:-1]))
    y_prev = np.concatenate(([yvert[-1]], yvert[:-1]))
    return np.sum(yvert * x_prev - xvert * y_prev, axis=0)



def normalize_area(contour, method=None):
    '''Normalizes a contour using one of three methods:
      * "pairwise"
      * "area"
      * "rmsd"
    Note: this assumes a zero-centered contour.
    '''
    methods = ['pairwise', 'area', 'rmsd']

    if method is None:
        raise ValueError("No method provided.")
    if method not in methods:
        raise ValueError(f"Method {method} is not supported."
                         f"Supported methods include: {methods}")

    if method == 'pairwise':
        from scipy.spatial.distance import pdist
        contour = contour/pdist(contour).mean()
    elif method == 'area':
        import numpy as np
        contour = contour/np.sqrt(contour_area(contour))
    elif method == 'rmsd':
        import numpy as np
        contour = contour/np.sqrt(np.sum(contour**2))

    return contour
