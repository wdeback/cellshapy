import numpy as np
from joblib import Parallel, delayed


def align_contours(contours,
                   n_jobs=-1,
                   tolerance=1e-4,
                   max_iterations=100,
                   return_alphas=False,
                   return_mean_shape=False):
    '''Aligns contours.

    Args:
        contours:
            The contours to align.
        n_jobs (int):
            The number of parallel jobs.
        tolerance (float):
            The minimal error which is accepted.
        max_iterations (int):
            The maximum number of iterations to perform.
        return_mean_shape (bool):
            Whether to also return the shape of the last mean.

    Returns:
        The aligned contours and, if return_mean_shape was specified, also the
        shape of the last mean.
    '''

    print("\rInitializing...", end="")
    import time

    # convert to complex representation
    complex_outlines = np.array(
            [_to_complex_representation(o) for o in contours])

    centered = np.array(
            [outline - np.mean(outline) for outline in complex_outlines])
    mean_estimate = centered[0]

    aligned, alphas = zip(*np.array(
            Parallel(n_jobs=n_jobs)(
                    delayed(_align_contour)(mean_estimate, i)
                    for i in centered)))

    updated_mean = np.mean(aligned, axis=0)
    iterations = 1
    print("\rAligning...", end="")
    tic = time.time()

    while (iterations < max_iterations and
           _procrustes_distance(mean_estimate, updated_mean) > tolerance):
        mean_estimate = updated_mean

        aligned, alphas = zip(*np.array(
                Parallel(n_jobs=n_jobs)(
                        delayed(_align_contour)(mean_estimate, i)
                        for i in centered)))

        updated_mean = np.mean(aligned, axis=0)
        toc = time.time()
        print("\rIteration {}, error: {:.6f}, time: {:.2f} sec".format(iterations,
              _procrustes_distance(mean_estimate, updated_mean), toc-tic), end="")
        iterations += 1

    mean = _to_cartesian_representation(updated_mean)
    aligned = np.array([_to_cartesian_representation(i) for i in aligned])

    if return_mean_shape:
        if return_alphas:
            return aligned, mean, alphas
        else:
            return aligned, mean
    else:
        if return_alphas:
            return aligned, alphas
        else:
            return aligned


def _procrustes_distance(reference, sample):
    procrustes_distance = 1 - np.linalg.norm(
            np.dot(np.conjugate(reference / np.linalg.norm(reference)),
                   sample / np.linalg.norm(sample)))**2
    return procrustes_distance


def _align_contour(reference, sample):
    alphas = np.array(
            [np.dot(np.conjugate(np.roll(sample, i)), reference)
             for i in range(len(sample))])
    procrustes_distance = np.array(
            [1 - np.linalg.norm(
                    np.dot(np.conjugate(reference), a * np.roll(sample, i))
                    )**2
             for a, i in zip(alphas, range(len(sample)))])
    best_fit = np.argmin(procrustes_distance)
    tmp = alphas[best_fit] * np.roll(sample, best_fit)

    return tmp / np.linalg.norm(tmp) * np.linalg.norm(sample), alphas[best_fit]


def _to_cartesian_representation(complex_contour):
    return np.reshape(np.ravel(
            np.column_stack((complex_contour.real, complex_contour.imag))),
            (len(complex_contour / 2), 2))


def _to_complex_representation(contour):
    return contour[:, 0]+contour[:, 1] * 1j

def reference_align_contours(videos, references=0, n_jobs=-1):
    '''Aligns contours in a more sensible way than align_contours.

    First, the contours are centered around the origin. Then a reference
    outline is selected from each video. The reference outlines are
    aligned (rotation and shift) among each other. Then the rotation that
    was applied to the reference outline is also applied to the rest of
    the video. In the end, the contour points are shifted in a way that
    the first point in the reference outline is as close as possible to
    the first point in the remaining frames.

    Args:
        videos:
            The videos to align.
        references (int, list of contours, or function):
            The reference contours. If this is an integer, it represents
            the frame in the video to use. If this is given as a list of
            contours, those are directly used. If this is a function, it
            is applied to the videos to retrieve the reference frames
            (has to return a list of contours).
        n_jobs (int):
            The number of parallel jobs.

    Returns:
        The aligned contours and, if return_mean_shape was specified, also the
        shape of the last mean.
    '''
    import numpy as np
    from cellshapy.utils.contour_alignment import align_contours
    from copy import deepcopy
    from joblib import Parallel, delayed

    def ssd(c1, c2):
        from scipy.spatial.distance import euclidean
        return np.sum([euclidean(c1[i], c2[i])**2 for i in range(len(c1))])

    def min_ssd(c1, c2):
        from scipy.spatial.distance import euclidean
        return np.argmin([ssd(c1, np.roll(c2, i, 0)) for i in range(len(c2))])

    def align_shift_to_reference(v, ref):
        v_tmp = [ref] + v
        shifts = [0]
        for f_i in range(len(v)):
            shifts.append(min_ssd(np.roll(v_tmp[f_i], shifts[f_i], 0), v_tmp[f_i+1]))
        ret = [np.roll(f, s, 0) for f, s in zip(v, shifts[1:(len(v)+1)])]
        return ret

    def rotate_and_normalize(contour, alpha):
        from cellshapy.utils.contour_alignment import _to_cartesian_representation, _to_complex_representation
        complex_contour = _to_complex_representation(contour)
        return _to_cartesian_representation(complex_contour * alpha /
                                            np.linalg.norm(complex_contour * alpha) *
                                            np.linalg.norm(complex_contour))

    # Get the contours as a list of lists and center them
    print("Centering contours and getting references... ", end='')
    vs = [[c - np.mean(c, axis=0) for c in v] for v in videos]

    # Select the references
    if isinstance(references, list):
        if len(references) != len(videos):
            raise ValueError("The number of references is not equal to the number of videos.")
        if not isinstance(references[0], np.ndarray):
            raise ValueError("The references should be given as a numpy array.")
        refs = references
    elif isinstance(references, int):
        if references >= len(vs[0]):
            raise ValueError("If given as an integer, the reference frame number has to be smaller than the number of frames.")
        refs = [v[references] for v in vs]
    elif hasattr(references, '__call__'):
        refs = references(videos)
    else:
        raise ValueError("Unsuited type for argument 'references'. Must be one of list, int, or callable.")
    print("done")

    print("Aligning references and obtaining rotation... ", end='')
    refs, alphas = align_contours(refs, return_alphas=True)
    print("done")

    print("Applying rotation to the videos... ", end='')
    # The alpha represents the rotation, that is now applied to the videos.
    fixed_vs = [[rotate_and_normalize(v[f_idx], alpha)
                 for f_idx in range(len(v))]
                for alpha, v in zip(alphas, vs)]
    print("done")

    # Fix the shifts within the videos, so that each first point
    # is as close as possible to the first point in the reference.
    print("Shifting the contour points... ", end='')
    fixed_vs = Parallel(n_jobs=-1)(delayed(align_shift_to_reference)(
        v, refs[v_idx]) for v_idx, v in enumerate(fixed_vs))
    print("done")

    return fixed_vs
