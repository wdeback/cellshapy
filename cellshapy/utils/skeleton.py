
from ..utils.contours import is_inside_polygon, close_contour
import numpy as np


def extract_skeleton(contour, prune_threshold=1e-3, plot=False, ax=None):
    from scipy.spatial import Voronoi
    # create voronoi diagram from contour
    vor = Voronoi(contour)

    vertices_inside, distances = \
        get_vertices_inside_contour(vor.vertices, contour)

    ridges = prune_ridges(vor.vertices, vertices_inside, vor.ridge_vertices)

    if prune_threshold > 0.0:
        vertices_inside, distances, ridges = \
            simplify_skeleton(vor.vertices,
                              vertices_inside,
                              ridges,
                              distances,
                              threshold=prune_threshold)

    def get_skeleton(vertices_all, vertices_inside, ridges):
        ''''''
        # get skeleton: ridges connecting vertices
        skeleton = []
        for simplex in ridges:  # vor.ridge_vertices:
            simplex = np.asarray(simplex)
            points = vertices_all[simplex]
            if np.any(points in vertices_inside):
                print("points in vertices_inside!", points)
            distance = is_inside_polygon(points[:, 0], points[:, 1], contour)
            if np.all(simplex >= 0) and np.all(distance < 0):
                skeleton.append([(x, y) for x, y in vertices_all[simplex]])

        # convert to array
        skeleton = np.array([np.array(list(s)) for s in skeleton])
        return skeleton

    skeleton = get_skeleton(vor.vertices, vertices_inside, ridges)

    # total length of skeleton
    skeleton_length = np.sum(
        [np.linalg.norm(ridge[0]-ridge[1]) for ridge in ridges])
    # print('skel_length = ', length)

    # number of branches
    neighbors = count_neighbors(vor.vertices, vertices_inside, ridges)
    neighbors -= 2  # only vertices with > 2 neighbors are counted as branches
    neighbors = np.clip(neighbors, a_min=0, a_max=None)
    num_branches = np.sum(neighbors*2)
    # print('num_branches = ', num_branches)

    # mean distance of skeleton vertices
    mean_distance = np.mean(distances)

    if plot:
        import matplotlib.pylab as plt
        if ax is None:
            fig, ax = plt.subplots(1, 1)
        # plot closed contour
        contour_c = close_contour(contour)
        ax.plot(*contour_c.T, 'bo-', lw=2, alpha=0.5)
        # plot vertices, colored by distance
        # ax.scatter(*vertices_inside.T, s=10, c=distances)

        # plot inscribing circles
        from matplotlib.patches import Circle
        for p, r in zip(vertices_inside, distances):
            c = Circle(p,
                       radius=r,
                       fill=False,
                       linewidth=1.0,
                       color='lightgray',
                       linestyle='-',
                       zorder=0)
            ax.add_patch(c)

        # plot skeleton
        # print(skeleton.shape)
        # print(skeleton)
        from matplotlib.collections import LineCollection
        lc = LineCollection(skeleton,
                            linestyle='solid',
                            linewidths=3,
                            color='black',
                            zorder=10)
        lc.set_capstyle('round')
        ax.add_collection(lc)
        ax.axis('equal')
        ax.axis('off')
        ax.set_title('Skeleton', fontsize=18)

    return skeleton, skeleton_length, num_branches, mean_distance


def simplify_skeleton(vertices_all,
                      vertices,
                      ridges,
                      distances,
                      threshold=1e-4):

    v, d, r = vertices.copy(), distances.copy(), ridges.copy()

    # get indices of endpoints
    endpoint_indices = np.argwhere(
        count_neighbors(vertices_all, v, r) == 1).flatten()
    # initialize weights for endpoints
    weights = weights_vertices(vertices,
                               distances,
                               endpoint_indices,
                               threshold)
    index_min = np.argmin(weights)

    include_indices = list(range(len(vertices)))
    while weights[index_min] < threshold:
        # remove the index of endpoint with lowest weight
        include_indices = np.delete(include_indices, index_min, axis=0)
        # mask the vertices and distances
        v = vertices[include_indices]
        d = distances[include_indices]
        # remove ridges associated with v
        r = prune_ridges(vertices_all, v, r)
        # get new end points
        endpoint_indices = np.argwhere(
            count_neighbors(vertices_all, v, r) == 1).flatten()
        weights = weights_vertices(v,
                                   d,
                                   endpoint_indices,
                                   threshold)
        index_min = np.argmin(weights)

    return vertices[include_indices], distances[include_indices], r


def get_vertices_inside_contour(vertices_all, contour):
    # get Voronoi vertices inside of contour (negative distance)
    distances = is_inside_polygon(vertices_all[:, 0],
                                  vertices_all[:, 1],
                                  contour)
    inside = distances < 0
    # return vertices in polygon and their distances
    vertices_inside = vertices_all[inside]
    distances = -distances[inside]
    return vertices_inside, distances


def prune_ridges(vertices_all, vertices_inside, ridge_vertices):
    '''filters the list of Voronoi.ridge_vertices
    and return list without infinity and ridges with vertices outside of
    polygon'''
    ridges = []
    for simplex in ridge_vertices:
        infinity = np.any([s == -1 for s in simplex])
        inside = np.all([s in vertices_inside for s in vertices_all[simplex]])
        if inside and not infinity:
            ridges.append(simplex)
    return np.array(ridges)


def count_neighbors(vertices_all, vertices_inside, ridges):

    counts = [0]*len(vertices_inside)
    for i, v in enumerate(vertices_inside):
        for simplex in ridges:
            if v in vertices_all[simplex]:
                counts[i] += 1
    return np.array(counts)


def weights_vertices(vertices, distances, elements, error):
    '''determine weights of vertices in skeleton
    based on the difference in area of the reconstruction of the element is
    removed.

    See eq 2 in https://pdfs.semanticscholar.org/002b/fd0f5611a2eda15d9196441f7f9fcd945472.pdf
    '''
    vertices, distances = vertices.copy(), distances.copy()
    # FIXME: When shapes are normalized to area 1, the plotting-approach
    # doesn't work anymore, because the weights become only 0 and 1. Thus,
    # we multiply by 1/error here to get higher precision. This doesn't change
    # anything for the ratio that is used but increases precision.
    vertices = vertices * (1/error)
    A_original = np.count_nonzero(reconstruct_from_skeleton(vertices,
                                                            distances))
    weights = np.ones((len(vertices)))

    for i, element in enumerate(elements):
        # leave element i out
        mask = np.ones(len(vertices), bool)
        mask[element] = False
        # get area of reconstruction
        A_i = np.count_nonzero(reconstruct_from_skeleton(vertices[mask],
                                                         distances[mask]))
        # calculate weight of i
        weights[element] = 1.0 - (A_i / A_original)
    return np.array(weights)


def reconstruct_from_skeleton(vertices, distances):
    '''create image from skeleton, given as vertices with distances'''

    from skimage.draw import circle

    max_diameter = np.max(distances)+1
    xmin, xmax = (int(np.min(vertices[:, 0]) - max_diameter),
                  int(np.max(vertices[:, 0]) + max_diameter))
    ymin, ymax = (int(np.min(vertices[:, 1]) - max_diameter),
                  int(np.max(vertices[:, 1]) + max_diameter))
    min_xy = (xmin, ymin)
    imsize = (xmax-xmin, ymax-ymin)
    reconstruction_image = np.zeros(imsize, dtype=np.uint8)

    for p, diam in zip(vertices, distances):
        p -= min_xy
        rr, cc = circle(*p, diam)
        reconstruction_image[rr, cc] = 1

    return reconstruction_image.T

