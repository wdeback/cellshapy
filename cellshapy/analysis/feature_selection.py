from sklearn.base import BaseEstimator
import numpy as np

class pca_estimator(BaseEstimator):

    def __init__(self, n_components=2):
        self.n_components = n_components
        self.embedding = None
        self.pca_object = None

    def fit(self, X, y=None):
        ''''''
        from .embedding import pca

        # add extra zeros column if < n_components
        if X.shape[-1] < self.n_components:
            n_cols = self.n_components - X.shape[-1]
            r = np.zeros((X.shape[0], n_cols))
            X = np.c_[ X, r]

        self.embedding, self.pca_object = pca(X, n_components=self.n_components, return_object=True)

    def predict(self, X):
        if self.pca_object is None:
            self.fit(X)
        return self.pca_object.transform(X)

def evaluate_rmse(estimator, X, y):
    y_pred = estimator.predict(X)
    from .embedding import compare_embeddings
    rmse = compare_embeddings(y, y_pred)
    return -rmse

def get_ground_truth(videos, space, n_components):
    '''
    Get ground truth data for embedding.

    In case of:
      - space='shapes', returns embedding ('contours', 'pca')
      - space='morphs', returns embedding ('stxy', 'parafac')

    Args:
        videos (VideoCollection): number of videos to fit shapes from
        space (str): Fit in space or morph space. One of ['shapes', 'morphs']
        n_components (int): number of shape descriptors.
    Returns:
        y_true (array):
    '''
    if space == 'shapes':
        key = ('contours', 'pca')
        # ground truth: PCA on contours
        if key not in videos.embedding or \
                videos.embedding[key].shape[-1] != n_components:
            print('Computing ground truth embedding...')
            videos.embed_shapes(source=key[0], method=key[1], n_components=n_components)
        y = videos.embedding[key]
    elif space == 'morphs':
        # ground truth: PARAFAC on contours
        key = ('stxy', 'parafac')
        if key not in videos.embedding or \
                videos.embedding[key][0].shape[-1] != n_components:
            print('Computing ground truth embedding...')
            videos.embed_morphs(format=key[0], method=key[1], n_components=n_components)
        y = videos.embedding[key][0]
    else:
        raise ValueError(f'Unknown space {space}')
    return y


def exhaustive_search(videos, space, n_components=2, show_top=10):
    '''
    Feature selection: Search for shape descriptors that best approximate the contour-based shape/morph space using exhaustive search.

    Args:
        videos (VideoCollection): number of videos to fit shapes from
        space (str): Fit in space or morph space. One of ['shapes', 'morphs']
        n_components (int): number of shape descriptors. Note: search space quickly because large for n_components>2.
        show_top (int or False or None): if Int, show the best n results. Otherwise, do not show anything.
    Returns:
        descriptors (list of str): descriptors that best approximate contour-based shape space
    '''

    from ..config import tqdm
    import numpy as np
    import pandas as pd
    from itertools import combinations
    from .embedding import compare_embeddings

    spaces = ['shapes', 'morphs']
    if space not in spaces:
        raise ValueError(f'space must be one of {spaces}')

    descriptors = videos.get_descriptors().columns
    y_true = get_ground_truth(videos, space=space, n_components=n_components)

    # get all combinations of desciptors
    descriptor_combination = list(combinations(descriptors, n_components))

    if len(descriptor_combination) > 1e3:
        import warnings
        warnings.warn(f'Testing {len(descriptor_combination)} combinations with exhaustive search. You may want to try genetic_selection().')


    results = []
    for features in tqdm(descriptor_combination):

        if space == 'shapes':
            # for the sample of descriptor combinations, do a PCA to n components
            videos.embed_shapes(source='descriptors', method='pca', n_components=n_components, include=features, verbose=False)
            # compare embedding with the 'ground truth' embedding and calculate the RSME
            y_pred = videos.embedding[('descriptors', 'pca')]

        elif space == 'morphs':
            # for the sample of descriptor combinations, do a PARAFAC to n components
            videos.embed_morphs(format='std', method='parafac', n_components=n_components, include=features, verbose=False)
            # compare embedding with the 'ground truth' embedding
            y_pred = videos.embedding[('std', 'parafac')][0]

        # calculate the RSME
        rmse = compare_embeddings(y_true, y_pred)
        # put in dataframe
        results.append({'number of descriptors': len(features),
                        'descriptors':features,
                        'rmse':rmse})


    df_descriptors = pd.DataFrame(results)

    if show_top is not False and show_top is not None:
        s = df_descriptors['rmse'].nsmallest(show_top)
        display( df_descriptors[df_descriptors['rmse'].isin(s)].sort_values(by='rmse') )

    min_features = df_descriptors[df_descriptors['rmse'].isin(s)].sort_values(by='rmse')['descriptors'].values[0]
    return min_features




def genetic_selection(videos, space, n_components=2, max_features=2,
                        cross_validation=3, n_population=200, n_generations=20,
                        mutation_proba=0.2, crossover_proba=0.5, tournament_size=3,
                        verbose=0):
    '''
    Feature selection: Search for shape descriptors that best approximate the contour-based shape space.

    Args:
        videos (VideoCollection): number of videos to fit shapes from
        space (str): Fit in space or morph space. One of ['shapes', 'morphs']
        n_components (int): number of shape descriptors
        max_features (int): maximum number of features to retain
        cross_validation (int): number of fold in k-fold cross validation
        n_population (int): population size in genetic algorithm (GA)
        n_generations (int): number of generation in GA
        mutation_proba (float): probability of mutation in GA
        crossover_proba (float): probability of crossover in GA
        tournament_size (int): size of tournament in tournament selection in GA
        verbose (int): verbosity level
    Returns:
        descriptors (list of str): descriptors that best approximate contour-based shape space
    '''
    from ..external.sklearn_genetic.genetic_selection import GeneticSelectionCV

    spaces = ['shapes', 'morphs']
    if space not in spaces:
        raise ValueError(f'space must be one of {spaces}')
    if space == 'morphs':
        raise ValueError(f'genetic_selection() not yet implemented for {space}')


    # get data: descriptors
    df = videos.get_descriptors()
    rows_with_NaNs, _ = np.where(df.isna())
    df = df.dropna()
    X = df.values
    feature_names = df.columns.values

    # ground truth: PCA on contours
    y = get_ground_truth(videos, space='shapes', n_components=n_components)
    # delete rows in which X had NaNs
    y = np.delete(y, rows_with_NaNs, axis=0)

    estimator = pca_estimator(n_components=n_components)

    selector = GeneticSelectionCV(estimator,
                                  cv=cross_validation,
                                  verbose=verbose,
                                  scoring=evaluate_rmse, #"neg_mean_squared_error",
                                  max_features=max_features,
                                  n_population=n_population,
                                  crossover_proba=crossover_proba,
                                  mutation_proba=mutation_proba,
                                  n_generations=n_generations,
                                  crossover_independent_proba=0.5,
                                  mutation_independent_proba=0.05,
                                  tournament_size=tournament_size,
                                  caching=True,
                                  n_jobs=-1)
    selector = selector.fit(X, y)
    print(selector.support_)

    from .embedding import compare_embeddings
    X = X[:,selector.support_]
    y_pred = estimator.predict(X)
    rmse = compare_embeddings(y_pred, y)
    min_features = list(feature_names[selector.support_])
    print(f'{min_features}: RMSE = {rmse:.3f}')
    return min_features

