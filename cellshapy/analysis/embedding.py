import numpy as np
import warnings

def pca(data, n_components=2, return_object=False):
    '''Perform a principle component analysis on the given data.

    Args:
        data (pandas DataFrame or numpy array):
            The data to perform PCA on.
        n_components (int):
            The number of principle components.
        return_object (bool):
            Whether or not to return fitted PCA object.

    Returns:
        numpy.ndarray:
            The PCA fit.
        if return_pca:
            Fitted PCA object itself

    '''
    from sklearn.decomposition import PCA
    pca = PCA(n_components=n_components)
    pca.fit(data)
    if return_object:
        return pca.transform(data), pca
    else:
        return pca.transform(data)

def inverse_pca(pca, data):
    '''Perform an inverse principle component analysis.

    Args:
        pca (sklearn.decomposition.PCA):
            Fitted PCA object.
        data (pandas DataFrame or numpy array):
            The data to perform PCA on.

    Returns:
        numpy.ndarray:
    '''
    return pca.inverse_transform(data)

def inverse_umap(umap, data):
    '''Perform an inverse UMAP.

    Args:
        umap (umap.UMAP):
            Fitted UMAP object.
        data (pandas DataFrame or numpy array):
            The data to perform UMAP on.

    Returns:
        numpy.ndarray:
    '''
    try:
        return umap.inverse_transform(data)
    except:
        raise ValueError('Inverse UMAP requires UMAP 0.4 or higher')


def lda(data, labels, n_components=2, return_object=False):
    '''Perform a linear discriminant anlaysis on the given data.

    Args:
        data (pandas DataFrame or numpy array):
            The data to perform LDA on.
        labels (pandas DataFrame or numpy array):
            Ground truth class labels.
        n_components (int):
            The number of components.
        return_object (bool):
            Whether or not to return fitted PCA object.

    Returns:
        numpy.ndarray:
            The LDA fit.
        if return_pca:
            Fitted LDA object itself

    '''
    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
    lda = LinearDiscriminantAnalysis(n_components=n_components)

    n_classes = len(np.unique(np.array(labels)))
    if n_components > (n_classes - 1):
        warnings.warn(f"`n_components` ({n_components}) larger then number of labels-1 ({n_classes}-1).")

    lda.fit(data, labels)
    if return_object:
        return lda.transform(data), lda
    else:
        return lda.fit_transform(data, labels)

def tsne(data,
         n_components=2,
         perplexity=50,
         early_exaggeration=14,
         learning_rate=1000,
         n_iter=1000,
         n_iter_without_progress=30,
         init='pca',
         verbose=0,
         method='barnes_hut',
         angle=0.5,
         try_fast_tsne=True,
         return_object=False):
    '''Perform dimensionality reduction using tSNE.

    Note:
        If try_fast_tsne is True, this method tries to use TSNE from the
        MulticoreTSNE package. If this fails, TSNE from scikit-learn is used
        as a fallback.

    Args:
        data (pandas DataFrame or numpy array):
            The data on which tSNE should be performed.
        n_components (int):
            See sklearn.manifold.TSNE.
        perplexity (float):
            See sklearn.manifold.TSNE.
        early_exaggeration (float):
            See sklearn.manifold.TSNE.
        learning_rate (gloat):
            See sklearn.manifold.TSNE.
        n_iter (int):
            See sklearn.manifold.TSNE.
        n_iter_without_progress (int):
            See sklearn.manifold.TSNE.
        init (str):
            See sklearn.manifold.TSNE. The default here is 'pca'.
        verbose (int):
            See sklearn.manifold.TSNE.
        method (str):
            See sklearn.manifold.TSNE.
        angle (float):
            See sklearn.manifold.TSNE.
        try_fast_tsne (bool):
            See the note. Determines, whether to try using TSNE from the
            MulticoreTSNE package.

    Returns:
        array: The embedding produced by TSNE.
    '''

    if n_components != 2:
        import warnings
        warnings.warn(f"TSNE cannot be meaningfully used for {n_components} components. Defaulting to `n_components=2`.")

    # try multicore tSNE, otherwise use sklearn implementation
    multicore_failed = True
    if try_fast_tsne:
        try:
            import multiprocessing
            n_jobs = multiprocessing.cpu_count()
            # print('n_components = ', n_components)
            from MulticoreTSNE import MulticoreTSNE as TSNE
            tsne = TSNE(n_jobs=n_jobs,
                        n_components=2,
                        perplexity=perplexity,
                        early_exaggeration=early_exaggeration,
                        learning_rate=learning_rate,
                        n_iter=n_iter,
                        n_iter_without_progress=n_iter_without_progress,
                        # init=init,
                        verbose=verbose,
                        method=method,
                        angle=angle)
            multicore_failed = False
            print(' using `MulticoreTSNE`', end='')
            # need to transform to float64 for MulticoreTSNE...
        except ImportError:
            import warnings
            warnings.warn('Consider installing the package MulticoreTSNE '
                          '`pip install Multicore-TSNE`. '
                          'Even for n_jobs=1 this speeds up the computation '
                          'considerably and might yield better converged '
                          'results.')
    if multicore_failed:
        from sklearn.manifold import TSNE
        tsne = TSNE(n_components,
                    perplexity,
                    early_exaggeration,
                    learning_rate,
                    n_iter,
                    n_iter_without_progress,
                    init,
                    verbose,
                    method,
                    angle)
    tsne.fit(data)
    if return_object:
        return tsne.fit_transform(data), tsne
    else:
        return tsne.fit_transform(data)


def umap(data, labels=None, n_components=2, min_dist=0.5, n_neighbors=5, return_object=False):
    '''Perform a uniform manifold approximation and projection.

    Args:
        data (pandas DataFrame or numpy array):
            The data on which to calculate the embedding.
        labels (numpy array, optional):
            Doing supervised dimensionality reduction, if provided.
        n_components (int):
            Length of embedding (default=2).

        Returns:
            array: The embedding produced by UMAP().fit_transform(data).
    '''
    try:
        #return umap.UMAP(n_components=n_components).fit_transform(data)
        from umap import UMAP
        umap = UMAP(n_components=n_components, min_dist=min_dist, n_neighbors=n_neighbors)
        umap.fit(data, y=labels)
        if return_object:
            return umap.transform(data), umap
        else:
            return umap.transform(data)

    except ImportError:
        import warnings
        warnings.warn('umap required installation of `umap-learn` '
                      '(https://github.com/lmcinnes/umap).\nInstall this with '
                      '`conda install -c conda-forge umap-learn` or '
                      '`pip install umap-learn`')


# ------------------------------------------------------

def vae(data, n_components=32, verbose=True, return_object=False):
    from cellshapy.analysis import vae
    autoencoder = vae.VAE(latent_dim=n_components,
                            tensor=data,
                            verbose=verbose)
    autoencoder.fit()
    embedding = autoencoder.transform(data)
    if return_object:
        return embedding, autoencoder
    else:
        return embedding




# ------------------------------------------------------

def parafac(data, n_components=2):
    '''Perform tensor decomposition using parallel factorization \
       (a.k.a. canonical decomposition).

       Uses `tensorly.decomposition.parafac`

    Args:
        data (numpy array, 3d or higher):
            The data on which to calculate the embedding.
        n_components (int):
            The rank, number of components in embedding.

    Returns:
        array: The embedding produced by parafac().
    '''

    data = np.array(data)
    if isinstance(data, list):
        raise ValueError('Cannot convert data into regular tensor. Do the samples have different lengths? You should make them equal length or try `parafac2`.')

    try:
        import tensorly as tl
        from tensorly.decomposition import parafac
        tensor_tl = tl.tensor(data)
        factors = parafac(tensor_tl, rank=n_components)
        
#         # compatibility with tensorly 0.4.4
#         import tensorly as tl
#         if isinstance(output, tl.kruskal_tensor.KruskalTensor):
#             factors = output.factors

        return factors
    
    
    except ImportError:
        import warnings
        warnings.warn('parafac required installation of `tensorly` '
                      '(https://github.com/tensorly/tensorly).\nInstall this with '
                      '`conda install -c tensorly tensorly` or '
                      '`pip install tensorly`')

def tucker(data, n_components=2):
    '''Perform tensor decomposition using parallel factorization \
       (a.k.a. canonical decomposition).

       Uses `tensorly.decomposition.tucker`

    Args:
        data (numpy array, 3d or higher):
            The data on which to calculate the embedding.
        n_components (int):
            The rank, number of components in embedding.

    Returns:
        array: The embedding produced by tucker().
    '''

    data = np.array(data)
    if isinstance(data, list):
        raise ValueError('Cannot convert data into regular tensor. Do the samples have different lengths? You should make them equal length or try `parafac2`.')

    try:
        import tensorly as tl
        from tensorly.decomposition import tucker
        tensor_tl = tl.tensor(data)
        core, factors = tucker(tensor_tl, rank=n_components)
        return factors, core
    except ImportError:
        import warnings
        warnings.warn('parafac required installation of `tensorly` '
                      '(https://github.com/tensorly/tensorly).\nInstall this with '
                      '`conda install -c tensorly tensorly` or '
                      '`pip install tensorly`')


def parafac2(data, n_components=2):
    '''Perform tensor decomposition using parallel factorization \
       (a.k.a. canonical decomposition).

    Args:
        data (numpy array, 3d or higher):
            The data on which to calculate the embedding.
        n_components (int):
            The rank, number of components in embedding.

    Returns:
        array: The embedding produced by parafac().
    '''

    F, D, A = _parafac2(X=data, r=n_components, verbose=False)
    # D: samples
    # A: X/Y values
    # F: time (list of varying sized arrays)

    # return in same order as parafac (note that F is a list!)
    return D,F,A


def _parafac2(X, r=2, tol=1e-5, verbose=True):
    '''PARAFAC2 (PARAllel FACtor analysis2) for unevenly-sized multi-way arrays

    The three-way PARAFAC2 model is best perceived as a model close to the ordinary PARAFAC model.
    The major difference is that strict trilinearity is no longer required,
    so PARAFAC2 can sometimes handle elution time shifts, varying batch trajectories etc.
    The ordinary PARAFAC model is also sometimes called the PARAFAC1 model to distinguish it
    from the PARAFAC2 model.

    See: http://wiki.eigenvector.com/index.php?title=Parafac2

    Code from https://github.com/yunjhongwu/matrix-routines/blob/master/parafac2.py
    MatLab implementation (Rasmus Bro): https://www.mathworks.com/matlabcentral/fileexchange/1089-parafac2
    '''
    import numpy as np
    from functools import reduce
    m = len(X)
    F = np.identity(r)
    D = np.ones((m, r))
    A = np.linalg.eigh(reduce(lambda A, B: A + B, map(lambda Xi: Xi.T.dot(Xi), X)))
    A = A[1][:, np.argsort(A[0])][:, -r:]

    H = [np.linalg.qr(Xi, mode='r') if Xi.shape[0] > Xi.shape[1] else Xi for Xi in X]
    G = [np.identity(r), np.identity(r), np.ones((r, r)) * m]

    err = 1
    conv = False
    niters = 0
    while not conv and niters < 100:
        P = [np.linalg.svd((F * D[i, :]).dot(H[i].dot(A).T), full_matrices=0) for i in range(m)]
        P = [(S[0].dot(S[2])).T for S in P]
        T = np.array([P[i].T.dot(H[i]) for i in range(m)])

        F = np.reshape(np.transpose(T, (0, 2, 1)), (-1, T.shape[1])).T.dot( _KhatriRao(D, A)).dot(np.linalg.pinv(G[2] * G[1]))
        G[0] = F.T.dot(F)
        A = np.reshape(np.transpose(T, (0, 1, 2)), (-1, T.shape[2])).T.dot( _KhatriRao(D, F)).dot(np.linalg.pinv(G[2] * G[0]))
        G[1] = A.T.dot(A)
        D = np.reshape(np.transpose(T, (2, 1, 0)), (-1, T.shape[0])).T.dot( _KhatriRao(A, F)).dot(np.linalg.pinv(G[1] * G[0]))
        G[2] = D.T.dot(D)
        err_old = err
        err = np.sum(np.sum((H[i] - (P[i].dot(F) * D[i, :]).dot(A.T)) ** 2) for i in range(m))
        niters += 1
        conv = abs(err_old - err) < tol * err_old
        if verbose: print("Iteration {0}; error = {1:.6f}".format(niters, err))

    P = [np.linalg.svd((F * D[i, :]).dot(X[i].dot(A).T), full_matrices=0) for i in range(m)]
    F = [(S[0].dot(S[2])).T.dot(F) for S in P]
    return F, D, A

def _KhatriRao(A, B):
    return np.repeat(A, B.shape[0], axis=0) * np.tile(B, (A.shape[0], 1))


#------------------------------------------------------------------------------------------------------------

## diagnostics for PARAFAC: explained variance and core consistency (CORCONDIA)

def variance_explained(X, factors):
    import tensorly as tl

    # reconstruct tensor from factors
    X_approx = tl.kruskal_to_tensor(factors)

    # compute explained variance
    variance_explained = X_approx.var() / X.var()

    #print(f"Variance explained = {variance_explained:.1%}")
    return 100. * variance_explained

def corcondia(X,factors):
    '''https://github.com/alessandrobessi/corcondia/blob/master/coreconsistency.py
    http://www.cs.ucr.edu/~epapalex/src/efficient_corcondia.zip from http://www.cs.ucr.edu/~epapalex/code.html
    '''

    def _superdiagonal(n_factors=3):
        superdiag = np.zeros([n_factors]*3, np.float32)
        for i in range(n_factors): superdiag[i,i,i] = 1
        return superdiag

    def _kron_mat_vector(matrices, tensor):
        from tensorly.tenalg import mode_dot
        K = len(matrices)
        x = tensor
        for k in range(K):
            M = matrices[k]
            x = mode_dot(x, M, k)
        return x

    from tensorly.tenalg import kronecker, mode_dot

    A,B,C = factors
    n_components = A.shape[-1]

    # Compute Penrose-Moore pseudo-inverse via SVDs (see Papalexakis et al, 2015)
    Ua, Sa, Va = np.linalg.svd(A)
    Ub, Sb, Vb = np.linalg.svd(B)
    Uc, Sc, Vc = np.linalg.svd(C)

    SaI = np.zeros((Ua.shape[0],Va.shape[0]), np.float32)
    np.fill_diagonal(SaI, Sa)

    SbI = np.zeros((Ub.shape[0],Vb.shape[0]), np.float32)
    np.fill_diagonal(SbI, Sb)

    ScI = np.zeros((Uc.shape[0],Vc.shape[0]), np.float32)
    np.fill_diagonal(ScI, Sc)

    SaI = np.linalg.pinv(SaI).astype(np.float32)
    SbI = np.linalg.pinv(SbI).astype(np.float32)
    ScI = np.linalg.pinv(ScI).astype(np.float32)

    y = _kron_mat_vector([Ua.T, Ub.T, Uc.T], X).astype(np.float32)
    z = _kron_mat_vector([SaI, SbI, ScI], y).astype(np.float32)
    G = _kron_mat_vector([Va.T, Vb.T, Vc.T], z).astype(np.float32)
    S = _superdiagonal(n_components)

    # compute the core consistency from the sum of squared error
    #  of the core tensor G from the superdiagonal tensor
    corcon = 1.0 - np.power(G-S, 2).sum() / np.power(G,2).sum()
    #print(f'Core consistency = {corcon:.1%}')

    return 100. * corcon


#------------------------------------------------------------------------------

def reconstruct_shapes(videos,
                        source,
                        method,
                        components,
                        num_points,
                        sigma,
                        probe_along_axes):
    '''
    Reconstruct contours at regularly spaced in PCA shape space.
    Returns reconstructed contours and their positions.

    Args:
        source (str):
            As in VideoCollection.embed_shapes().
        method (str):
            As in VideoCollection.embed_shapes().
        components (list of ints):
            Components to plot, default=[1,2]. None means all.
        num_points (int or list or ints):
            Number of reconstructed points per dimension/factor/component.
            If int, same number of points for each dimension.
            If list, number of points can be specified per dimension. In this case, length must be equal to `num_dimensions`.
        sigma (float, default=2.0):
            Number of standard deviations used to determine range of reconstruction points.
        probe_along_axes (bool):
            If true, only positions are returned along axes (num_dimensions * num_points)

    Returns:
        contours (array, size = (samples, time, contour elements, 2)):
            Coordinates of reconstructed contours.
        positions (array, size = (samples, 2))
            Coordinates of reconstructed contours in shape space.

    '''

    if (source, method) not in videos.embedding.keys():
        raise ValueError(f"Embedding for '{source}' using '{method}' not yet calculated. "
                         f"Please run VideoCollection.embed_shapes('{source}','{method}').")

    embedding = videos.embedding[(source, method)]

    # choose components for embedding
    n_components_original = embedding[0].shape[-1]
    if components is None:
        components = list(range(1, n_components_original+1))
    n_components = len(components)
    components_left_out = list(set(list(range(n_components_original))) - set(np.subtract(components, 1)))
    embedding = embedding[:, np.subtract(components, 1)]

    num_dimensions = n_components

    # make list of num_points per factor
    if not isinstance(num_points, list):
        num_points = [num_points]*num_dimensions
        #print('num_points = ', num_points)
    elif len(num_points) != num_dimensions:
        raise ValueError(f'If specified as a list, length of `num_points` ({num_points}) must be equal to `num_dimensions` ({num_dimensions}).')

    if not isinstance(sigma, list):
        sigma = [sigma]*embedding[0].shape[-1]
    elif len(sigma) != num_dimensions:
        raise ValueError(f'If specified as a list, length of `sigma` ({sigma}) must be equal to `num_dimensions` ({num_dimensions}).')

    # generate sampling points (probes)
    if probe_along_axes:
        probe_positions = _get_probes_along_axes(embedding=embedding,
                                        num_points=num_points,
                                        sigma=sigma, plot=False)
    else:
        probe_positions = _get_probes(embedding=embedding,
                                      num_points=num_points,
                                      sigma=sigma, plot=False)

    #print(f"probe_positions.shape = {probe_positions.shape}")

    # add zeros to left-out components
    if probe_along_axes:
        # if probing along axes, total reconstructs is sum of num_points
        probe_positions2 = np.zeros( (np.sum(num_points), n_components_original) )
    else:
        # if probing combinatorially axes, total reconstructs is product of num_points
        probe_positions2 = np.zeros( (np.product(num_points), n_components_original) )
    i=0
    for c in range(n_components_original):
        if c not in components_left_out:
            probe_positions2[:,c] = probe_positions[:,i]
            i=i+1

    # get transform object (e.g. sklearn.decomposition.PCA())
    embedding_object = videos.embed_object[(source, method)]
    if embedding_object is None:
        raise ValueError('Please run embed_shapes() first.')

    # inverse transform: reconstruct contours from
    if method == 'pca':
        data_transformed = inverse_pca(embedding_object, probe_positions2)
    elif method == 'umap':
        data_transformed = inverse_umap(embedding_object, probe_positions2)

    contours = data_transformed.reshape((len(probe_positions), -1, 2))

    return contours, probe_positions

#------------------------------------------------------------------------------

def reconstruct_morphs(videos,
                             format,
                             method,
                             components,
                             num_points,
                             sigma,
                             probe_along_axes,
                             plot_tensor=False):
    '''
    Reconstruct contours at regularly spaced in parafac shape space.
    Returns reconstructed contours and their positions.

    (1) Sample morph space at regular intervals (num_points in num_dimensions).
    (2) Reconstruct original tensor from approximated parafac or tucker decomposition.
    (3) Reconstruct morphs from reconstructed tensor.

    Args:
        format (str):
            As in VideoCollection.embed_morphs().
        method (str):
            As in VideoCollection.embed_morphs().
        components (list of ints, length 2):
            Components to reconstruct.
        num_points (int or list or ints):
            Number of reconstructed points per dimension/factor/component.
            If int, same number of points for each dimension.
            If list, number of points can be specified per dimension. In this case, length must be equal to `num_dimensions`.
        sigma (float, default=2.0):
            Number of standard deviations used to determine range of reconstruction points.
        probe_along_axes (bool):
            If true, only positions are returned along axes (num_dimensions * num_points)
        plot_tensor (bool):
            If true, plot tensor as 2D slices.

    Returns:
        contours (array, size = (samples, time, contour elements, 2)):
            Coordinates of reconstructed contours.
        positions (array, size = (samples, 2))
            Coordinates of reconstructed contours in shape space.

    '''
    available_formats = ['stxy', 'stp', 'stef']
    if format not in available_formats:
        raise ValueError(f"Reconstruction of morphs not available for `format={format}`. Please choose one of {available_formats} instead.")

    available_methods = ['parafac', 'tucker', 'vae']
    if method not in available_methods:
        raise ValueError(f"Reconstruction of morphs not available for `format={format}`. Please choose one of {available_methods} instead.")

    # get embedding factors (and core in case of tucker)
    embed_object = None
    if method == 'parafac':
        embedding = videos.embedding[(format, method)][0]
    elif method == 'tucker':
        embedding, core = videos.embedding[(format, method)]
        embedding = embedding[0]
    elif method == 'vae':
        embed_object = videos.embed_object[(format, method)]
        embedding = videos.embedding[(format, method)]

    # choose components for embedding
    n_components_original = embedding[0].shape[-1]
    if components is None:
        components = list(range(1, n_components_original+1))
    n_components = len(components)
    components_left_out = list(set(list(range(n_components_original))) - set(np.subtract(components, 1)))

    for c in components:
        if c > n_components_original:
            raise ValueError(f"`component` cannot be larger than `n_components`({c} > {n_components_original}).")

    embedding = embedding[:, np.subtract(components, 1)]

    num_dimensions = n_components

    if num_dimensions != n_components:
        raise ValueError(f"`num_dimensions` ({num_dimensions}) must be equal to the number of components ({n_components}).\nPlease run VideoCollection.embed_shapes() again with `n_components={num_dimensions}`")

    # make list of num_points per factor
    if not isinstance(num_points, list):
        num_points = [num_points]*num_dimensions
        #print('num_points = ', num_points)
    elif len(num_points) != num_dimensions:
        raise ValueError(f'If specified as a list, length of `num_points` ({num_points}) must be equal to `num_dimensions` ({num_dimensions}).')

    if not isinstance(sigma, list):
        sigma = [sigma]*embedding.shape[-1]
        #print('sigma = ', sigma)
    elif len(sigma) != num_dimensions:
        raise ValueError(f'If specified as a list, length of `sigma` ({sigma}) must be equal to `num_dimensions` ({num_dimensions}).')


    recon_tensor, recon_positions = reconstruct_tensor(method=method,
                                                    embedding=videos.embedding[(format, method)],
                                                    components=components,
                                                    object=object,
                                                    num_points=num_points,
                                                    sigma=sigma,
                                                    along_axes=probe_along_axes,
                                                    plot=False)

    if plot_tensor:
        from ..plotting.tensor import plot_tensor_2D
        plot_tensor_2D(recon_tensor[0])

    # reconstruct morphs from reconstructed tensor
    recon_contours = tensor_to_contours(recon_tensor, format=format)

    return recon_contours, recon_positions, num_points

#----------------------------------------------------------------

def videos_to_tensor(videos, format, parameter=None, include=None, exclude=None):
    '''Convert contours to tensor in stf, stxy, or stp format'''

    import numpy as np
    format = format.lower()
    formats = ['samples_time_descriptors', 'std',
               'samples_time_xy', 'stxy',
               'samples_time_pairwisedistance', 'stp',
               'samples_time_curvature', 'stc',
               'samples_time_mask', 'stm',
               'samples_time_elliptic_fourier', 'stef']

    if format not in formats:
        raise ValueError(f'format must be one of {formats}, but got {format}.')

    if format in ['stxy', 'samples_time_xy']:
        def get_reshaped_contours(v):
            # get all aligned contours from video
            contours = v.get_aligned_contours()
            # reshape such that x and y coordinates are in 1 dimension
            contours = np.reshape(contours, (*contours.shape[:1], -1), order='F' )
            return contours

        # get reshaped contours for each video
        tensor = np.array([get_reshaped_contours(v) for v in videos])

    elif format in ['stp', 'samples_time_pairwisedistance']:
        def get_pairwise_distances(v):
            #from scipy.spatial.distance import squareform, pdist
            from scipy.spatial.distance import pdist
            contours = v.get_aligned_contours()
            #pairwise_distances = np.array([squareform(pdist(contour)) for contour in contours])
            pairwise_distances = np.array([pdist(contour) for contour in contours])
            return pairwise_distances

        tensor = np.array([get_pairwise_distances(v) for v in videos])

    elif format in ['std', 'samples_time_descriptors']:
        # get shape descriptors for all videos
        tensor = []
        for v in videos:
            df = videos.get_descriptors(video=v)
            if include is not None:
                df = df[list(include)]
            if exclude is not None:
                df = df.drop(list(exclude), errors='ignore')
            #if fill_nans:
            df = df.fillna(0.)
            tensor.append( df.values )
        tensor = np.array(tensor)
        # standardize columns
        tensor -= np.mean(tensor, axis=0)
        tensor /= np.std(tensor, axis=0) + 1e-6

    elif format in ['samples_time_curvature', 'stc']:
        # get signed curvatures
        curvatures = videos.get_curvatures(signed=True)
        # calculate 5% and 95% percentiles (to exclude outliers)
        vmin = np.percentile(curvatures, 5)
        vmax = np.percentile(curvatures, 95)
        tensor = np.clip(curvatures, a_min=vmin, a_max=vmax)

    elif format in ['samples_time_elliptic_fourier', 'stef']:
        try:
            from pyefd import elliptic_fourier_descriptors
        except ImportError:
            raise ImportError('Computing elliptic Fourier descriptors requires `pyefd` package. Install with: `pip install pyefd`.')

        def get_fourier_descriptors(v):
            contours = v.get_aligned_contours()
            return np.array([elliptic_fourier_descriptors(contour, order=5, normalize=False).flatten()
                                for contour in contours])

            def efd_feature(contour):
                coeffs = elliptic_fourier_descriptors(contour, order=10, normalize=True)
                return coeffs[:,3].flatten() # when normalizing, first 3 elements are identical for all shapes and can be disregarded:
            return np.array([efd_feature(contour).flatten() for contour in contours])

        from joblib import Parallel, delayed
        tensor = np.array(Parallel(n_jobs=-1)(delayed(get_fourier_descriptors)(v) for v in videos))
        #tensor = np.array([get_fourier_descriptors(v) ])

    #elif format in ['stm', 'samples_time_mask']:
    #    tensor = np.array([[frame.mask for frame in video] for video in videos])

    return tensor


def tensor_to_contours(tensor, format):
    '''Convert a tensor in stxy or stp format back to contours'''

    import numpy as np
    format = format.lower()
    formats = ['samples_time_xy', 'stxy',
               'samples_time_pairwisedistance', 'stp',
               'samples_time_elliptic_fourier', 'stef']

    print('format = ', format)

    if format not in formats:
        raise ValueError('format must be one of {formats}, but got {format}.')

    if format in ['stxy', 'samples_time_xy']:
        contours_all = []
        for sample in tensor:
            contours = []
            for frame in sample:
                mid = len(frame)//2
                x = frame[:mid]
                y = frame[mid:]
                contour = np.array([x,y]).T
                #contour -= np.mean(contour, axis=0)
                contours.append(contour)

            contours_all.append(np.array(contours))
        contours_all = np.array(contours_all)
        print(contours_all.shape)
        return contours_all

    elif format in ['samples_time_elliptic_fourier', 'stef']:

        def contour_from_elliptic_fourier_descriptors(coeffs, order):
            coeffs = coeffs.reshape(order,4)
            from pyefd import calculate_dc_coefficients, plot_efd
            locus = (0., 0.)
            n_points = config['contours', 'n_points']
            #xt = np.ones((n_points,)) * locus[0]
            #yt = np.ones((n_points,)) * locus[1]
            xt, yt = 0., 0.
            t = np.linspace(0, 1.0, n_points)
            for n in range(coeffs.shape[0]):
                xt += (coeffs[n, 0] * np.cos(2 * (n + 1) * np.pi * t)) + \
                      (coeffs[n, 1] * np.sin(2 * (n + 1) * np.pi * t))
                yt += (coeffs[n, 2] * np.cos(2 * (n + 1) * np.pi * t)) + \
                      (coeffs[n, 3] * np.sin(2 * (n + 1) * np.pi * t))
            reconstruction = np.array([xt, yt]).T
            return reconstruction

        #from joblib import Parallel, delayed
        #tensor = np.array(Parallel(n_jobs=-1)(delayed(get_fourier_descriptors)(v) for v in videos))

        #contours = []
        #for sample in tensor:
        #    contours.append( np.array([contour_from_elliptic_fourier_descriptors(sample) for frame in sample ]) )
        #return np.array(contours)

        from ..config import tqdm
        contours_all = []
        for sample in tqdm(tensor):
            contours = []
            for frame in sample:
                reconstruction = contour_from_elliptic_fourier_descriptors(frame, order=5)
                contours.append( reconstruction )
            contours_all.append( np.array(contours) )

        contours_all = np.array(contours_all)
        print(contours_all.shape)
        return contours_all


    elif format in ['stp', 'samples_time_pairwisedistance']:

        # Compute coordinates from distance matrix: https://math.stackexchange.com/a/423898

        def pdist_to_coords(dm):
            '''Extract coordinates from pairwise distance matrix.
            Adapted from R-code in https://stackoverflow.com/a/18109543'''
            n = dm.shape[0]
            p1, p2 = np.unravel_index(np.argmax(dm, axis=None), dm.shape)
            x2 = dm[p1, p2]
            r1sq = dm[p1,:]**2
            r2sq = dm[p2,:]**2
            x = (r1sq - r2sq + x2**2)/(2*x2)
            _tmp = r1sq - x**2
            _tmp[np.isclose(_tmp, np.zeros(_tmp.shape))] = 0
            y = np.sqrt(_tmp)
            p3 = np.argmax(y)
            x3 = x[p3]
            y3 = y[p3]
            plus = abs(dm[p3,]**2 - (x3 - x)**2 - (y3 - y)**2)
            minus = abs(dm[p3,]**2 - (x3 - x)**2 - (y3 + y)**2)
            y[minus < plus] = -y[minus < plus]
            coords = np.array(list(zip(x, y)))

            '''
            import matplotlib.pyplot as plt
            fig, ax = plt.subplots(1,2)
            ax[0].matshow(dm)
            ax[1].plot(*coords.T)
            plt.show()
            '''

            return coords


        from scipy.spatial.distance import squareform, pdist
        contours_all = np.array([pdist_to_coords(squareform(pdistmat))
                                 for sample in tensor
                                 for pdistmat in sample])



        # re-center and align contours
        from ..utils.contour_alignment import align_contours
        all_contours_aligned = align_contours(contours_all)

        # reshape contours to match tensor dimensions: samples, time, contours
        w,x,y,z = tensor.shape
        all_contours_aligned = all_contours_aligned.reshape(w, x, y, -1)

        return all_contours_aligned


def reconstruct_tensor(method, embedding, components, object=None, num_points=[3], sigma=[2.0], along_axes=False, plot=False):
    '''

    Args:
        embedding ():
            ...
    Returns:

    '''
    available_methods = ['parafac', 'tucker', 'vae']
    if method not in available_methods:
        raise ValueError(f"Tensor reconstruction not available for `method={method}`. Please choose one of {available_methods} instead.")
    import tensorly as tl

    if method in ['parafac', 'vae']:
        factors = embedding
    elif method == 'tucker':
        factors, core = embedding

    for factor in factors:
        print(f"factor.shape = {factor.shape}")

    n_factors = factors[0].shape[-1]


    for c in components:
        if c > n_factors:
            raise ValueError(f"`component` {c} is larger than factors in embedding ({n_factors}).\nPlease run VideoCollection().embed_morphs() with higher number of components.")
    if not isinstance(num_points, list):
        num_points = [num_points]*n_factors
    if not isinstance(sigma, list):
        sigma = [sigma]*n_factors


    # replace original positions with probing positions
    sample_positions = factors[0][:,np.subtract(components, 1)]

    if along_axes:
        probe_positions = _get_probes_along_axes(embedding=sample_positions,
                                                num_points=num_points,
                                                sigma=sigma, plot=plot)
    else:
        probe_positions = _get_probes(embedding=sample_positions,
                                        num_points=num_points,
                                        sigma=sigma, plot=plot)

    # add zeros to missing columns
    num_missing_colums = factors[0].shape[-1] - probe_positions.shape[-1]
    probe_positions = np.concatenate([probe_positions, np.zeros((probe_positions.shape[0], num_missing_colums))], axis=1)
    # set factors for 'samples' to the new probes
    factors[0] = probe_positions

    if method == 'parafac':
        # convert from kruskal format to approximate tensor
        tensor_recon = tl.kruskal_to_tensor(factors)
    elif method == 'tucker':
        # convert from tucker format to approximate tensor
        tensor_recon = tl.tucker_to_tensor(core, factors)
    elif method == 'vae':
        # get VAE object
        vae = object
        # use decoder of VAE to reconstruct contours
        tensor_recon = vae.inverse_transform(factors)
    print('reconstruct_tensor reconstruction.shape: ', tensor_recon.shape)

    return tensor_recon, probe_positions


def _get_probes(embedding, num_points, sigma, plot=False):
    '''Create a mesh of probing positions in embedding space.
    In total, `num_points * num_points` will be created, spaced according to `sigma` time the std.

    Args:
        embedding (array):

        num_points (list of ints):
            Number of points along each dimension
        sigma (float or array):
            Spacing of points in terms of standard deviations along each axis

    '''
    import numpy as np
    num_dimensions = len(num_points)
    if not isinstance(sigma, (list, tuple, np.ndarray)):
        sigma = np.repeat(sigma, num_dimensions)

    #print(num_points)
    #print(len(num_points))
    axes = []
    for i in range(num_dimensions):
        minimum = embedding[:,i].mean() - sigma[i]*embedding[:,i].std()
        maximum = embedding[:,i].mean() + sigma[i]*embedding[:,i].std()
        #print('axis {}: {:.2f}, {:.2f}, {}'.format(i, minimum, maximum, num_points[i]))
        axis = np.linspace(minimum, maximum, num_points[i])
        axes.append( axis )
    #axes = np.moveaxis(np.array(axes), -1, 0)
    axes = np.array(axes)
    mesh = np.meshgrid(*axes, indexing='ij')
    #print(mesh[0].shape, mesh[1].shape)
    probe_positions = np.array(mesh).reshape(num_dimensions, -1)
    probe_positions = np.moveaxis(probe_positions, -1, 0)
    print(probe_positions.shape)

    if plot:
        import matplotlib.pylab as plt
        plt.scatter(*probe_positions.T[:2])

    return probe_positions


def _get_probes_along_axes(embedding, num_points, sigma, plot=False):
    '''Create a mesh of probing positions in embedding space.
    In total, `num_points` will be created, spaced according to `sigma` time the std.

    Args:
        embedding (array):

        num_points (list of ints):
            Number of points along each dimension
        sigma (float or array):
            Spacing of points in terms of standard deviations along each axis

    '''
    import numpy as np
    num_dimensions = len(num_points)
    if not isinstance(sigma, (list, tuple, np.ndarray)):
        sigma = np.repeat(sigma, num_dimensions)

    probe_positions = []

    axes = []
    for i in range(num_dimensions):
        minimum = embedding[:,i].mean() - sigma[i]*embedding[:,i].std()
        maximum = embedding[:,i].mean() + sigma[i]*embedding[:,i].std()
        #print('axis {}: {:.2f}, {:.2f}, {}'.format(i, minimum, maximum, num_points[i]))
        axis = np.linspace(minimum, maximum, num_points[i])
        axes.append( axis )
        positions = np.zeros((num_points[i], num_dimensions))
        # fill in means for all axis, except the probing axis
        for j in range(num_dimensions):
            if i == j:
                positions[:,j] = axis
            else:
                positions[:,j] = embedding[:,j].mean()
        probe_positions.append( positions )
    axes = np.array(axes)
    probe_positions = np.array(probe_positions)
    probe_positions = probe_positions.reshape( (np.sum(np.array(num_points)), num_dimensions) )

    if plot:
        import matplotlib.pylab as plt
        plt.scatter(*probe_positions.T[:2])

    return probe_positions

#-----------------------------------------------------------------------------

def tensor_reconstruction_errors(videocollection,
                                format,
                                method,
                                max_components,
                                video_indices=None,
                                stepsize=1):
    '''
    Calculate the reconstruction error performing decomposition (embedding) and its inverse,
    for different number of components.

    Args:
        ...
        max_components (int):
            Maximum number of components/factor the tensor is decomposed into.
        stepsize (int):
            Interval between n_components.
        video_indices (None, list of ints, or 'all'):
            Which videos to calculate video-specific reconstruction errors for.
            If None or [], no video-specific errors are computed.
            If list of ints, compute error for video in indices.
            If 'all', compute all errors for all videos in tensor.
    Returns:
        reconstruction error (dict):
            Reconstruction errors of different number of components
        reconstruction error for specific videos (list of dicts):
            If video_indices is not None, list of reconstruction errors of different number of components
    '''
    import tensorly as tl
    from ..config import tqdm

    allowed_formats = ['stxy', 'stc']
    allowed_methods = ['parafac']
    if format not in allowed_formats:
        raise ValueError(f"Not implemented for format={format}. Choose one of {allowed_methods}")
    if method not in allowed_methods:
        raise ValueError(f"Not implemented for method={method}. Choose one of {allowed_methods}")

    if (format, method) not in videocollection.tensor:
        _ = videocollection.embed_morphs(format=format, method=method, n_components=1, save_embedding=False)
    tensor_original_all = videocollection.tensor[format]

    if video_indices is None:
        video_indices = []
    if video_indices == 'all':
        video_indices = list(range(tensor_original_all.shape[0]))

    # dict of dicts: keys = video_index, n_components
    reconstruction_errors_videos = dict(zip(video_indices, [dict() for _ in video_indices]))

    reconstruction_error = dict()
    for d in tqdm(range(1, max_components, stepsize)):
        #print("Number of factors = ", d)

        # embed videos
        embedding = videocollection.embed_morphs(format=format, method='parafac', n_components=d, save_embedding=False, verbose=False)
        tensor_reconstructed_all = tl.kruskal_to_tensor(embedding)

        # calculate reconstruction error for whole tensor
        mse_all = np.mean(np.square(np.subtract(tensor_reconstructed_all, tensor_original_all)))
        reconstruction_error[d] = mse_all

        # calculate reconstruction error for specific videos
        for v in video_indices:
            mse = np.mean(np.square(np.subtract(tensor_reconstructed_all[v], tensor_original_all[v])))
            reconstruction_errors_videos[v][d] = mse

    if len(video_indices) == 0:
        return reconstruction_error
    else:
        return reconstruction_error, reconstruction_errors_videos

def choose_elbow(d, window_size=5, order=3, plot=True):

    xs, ys = list(d.keys()), list(d.values())

    # smooth data
    if window_size > 2:
        if order >= window_size:
            raise ValueError(f"`order` ({order}) must be greater than `window_size` ({window_size})")
        from scipy.signal import savgol_filter
        ys = savgol_filter(ys, window_size, order) #  polynomial order 3

    # get first derivative
    d_ys = np.gradient(ys)
    # get second derivative
    dd_ys = np.gradient(d_ys)

    # find peak
    index_max = np.argmax(dd_ys)

    # Heuristic to a peak with reasonable error
    # peak calculated above be one with highest error.
    # here, we choose one that is at least equal or below mean error.
    # (note, however, that the mean depends on how many error were used as input.)
    index = -1
    while ys[index_max] == np.max(ys):
        index_max = np.argsort(dd_ys)[index]
        index -= 1
        #print(index)

    if plot:
        import matplotlib.pylab as plt
        fig, ax = plt.subplots(3,1,figsize=(4,4), sharex=True)
        fig.subplots_adjust(hspace=0)
        ax[0].plot(xs, ys, 'ro-')
        ax[0].scatter(xs[index_max], ys[index_max], color='blue', s=200)
        ax[0].set_ylabel('error')

        ax[1].plot(xs, d_ys, 'bo-')
        ax[1].scatter(xs[index_max], d_ys[index_max], color='red', s=200)
        ax[1].set_ylabel('1st deriv.')

        ax[2].plot(xs, dd_ys, 'bo-')
        ax[2].scatter(xs[index_max], dd_ys[index_max], color='red', s=200)
        ax[2].set_xlabel('number of components')
        ax[2].set_ylabel('2nd deriv.')

    return xs[index_max]


#---------------------------------------------------------------------------


def embedding_clustering_scores(videocollection,
                                    format,
                                    method,
                                    ground_truth,
                                    max_components,
                                    cluster_method='kmeans',
                                    cluster_metric='all',
                                    video_indices=None,
                                    stepsize=1,
                                    projection=None,
                                    plot=False,
                                    plot_clusterings=False):
    '''
    Calculate the reconstruction error performing decomposition (embedding) and its inverse,
    for different number of components.

    Args:
        ...
        ground_truth (list of int):
            Ground truth cluster labels, in same order as samples in tensor.
        max_components (int):
            Maximum number of components/factor the tensor is decomposed into.
        cluster_method (str):
            Clustering algorithm, from sklearn.cluster. Default: 'kmeans'.
            One of: ['kmeans', 'minibatchkmeans',
                    'affinitypropagation', 'meanshift',
                    'agglomerativeclustering',
                    'spectral']
        cluster_metric (str):
            Metric to measure clustering accuracy, from sklearn.metrics.cluster. Default: 'all'
            One of ['all', 'adjusted_mutual_info_score', 'adjusted_rand_score', 'homogeneity_score', 'v_measure_score']
        stepsize (int):
            Interval between n_components.
        video_indices (None, list of ints, or 'all'):
            Which videos to calculate video-specific reconstruction errors for.
            If None or [], no video-specific errors are computed.
            If list of ints, compute error for video in indices.
            If 'all', compute all errors for all videos in tensor.
        projection (str):
            If case embedding > 2 factors, perform 2D projection using ['pca', 'umap' or 'tsne']
        plot (bool):
            If true, plot scores.
    Returns:
        clustering scores (dict of dicts):
            Clustering scores for different metrics and different n_components.
    '''
    import tensorly as tl
    from ..config import tqdm

    allowed_formats = ['stxy', 'stc']
    allowed_methods = ['parafac']
    if format not in allowed_formats:
        raise ValueError(f"Not implemented for format={format}. Choose one of {allowed_methods}")
    if method not in allowed_methods:
        raise ValueError(f"Not implemented for method={method}. Choose one of {allowed_methods}")

    # get number of clusters from ground truth labels
    n_clusters = len(np.unique(np.array(ground_truth)))

    '''
    from sklearn.metrics import adjusted_mutual_info_score, \
                                adjusted_rand_score, \
                                homogeneity_score, \
                                v_measure_score
    metrics = [adjusted_mutual_info_score, adjusted_rand_score, homogeneity_score, v_measure_score]
    '''
    from sklearn.metrics import silhouette_score
    from ..analysis.cluster import cluster

    metrics = [silhouette_score]
    metrics_names = [m.__name__ for m in metrics]
    #print(metrics_names)
    if cluster_metric != 'all':
        if cluster_metric not in metrics_names:
            raise ValueError(f"`cluster_metric` ({cluster_metric}) must be one of [{metrics_names}]")
        metrics = eval(cluster_metric)
    #print(metrics)

    # dict of dicts: keys = video_index, n_components
    clustering_scores = dict(zip(metrics_names, [dict() for _ in metrics]))

    for n_components in tqdm(range(1, max_components, stepsize)):
        #print('n_components = ', d)
        # embed videos
        embedding = videocollection.embed_morphs(format=format, method='parafac', n_components=n_components, save_embedding=False, verbose=False)
        factors = embedding[0]

        # cluster
        #y_pred = cluster(factors, cl_method=cluster_method, n_clusters=n_clusters)

        # compute scores
        for metric in metrics:
            clustering_scores[metric.__name__][n_components] = metric(factors, labels=ground_truth)
            #print(metric(ground_truth, y_pred))

        if projection is not None and factors.shape[-1] > 2:
            import warnings
            from ..analysis.embedding import pca, lda, umap, tsne
            projections = ['pca', 'lda', 'umap', 'tsne']
            warnings.warn(f"Projecting from {factors.shape[-1]} components to 2D using {projection}.")
            if projection not in projections:
                raise ValueError(f'`projection` must be one of {projections}')
            if projection == 'pca':
                factors = pca(factors, n_components=2)
            if projection == 'lda':
                factors = lda(factors, ground_truth, n_components=2)
            elif projection == 'umap':
                factors = umap(factors, n_components=2)
            elif projection == 'tsne':
                factors = tsne(factors, n_components=2)

        # compute scores
        for metric in metrics:
            clustering_scores[metric.__name__][n_components] = metric(factors, labels=ground_truth)
            #print(metric(ground_truth, y_pred))


        if plot_clusterings:
            if factors.shape[-1] == 2:
                import matplotlib.pylab as plt
                plt.scatter(*factors.T, c=ground_truth, s=200)
                #plt.scatter(*factors.T, c=y_pred, s=75)
                plt.title('N_components = {:d}'.format(n_components))
                plt.show()
            else:
                import warnings
                warnings.warn(f"Cannot plot embedding with {factors.shape[-1]} components.")

    if plot:
        import matplotlib.pylab as plt
        fig, ax = plt.subplots(1,1,figsize=(8,4))
        xs = list(range(1, max_components, stepsize))
        for metric_name in clustering_scores.keys():
            scores = np.array(list(clustering_scores[metric_name].values()))
            ax.plot(xs, scores, '-o', label=metric_name.replace('_', ' '), zorder=1)
            max_index = np.argmax(scores)
            ax.scatter(xs[max_index], scores[max_index], s=200, c='red', zorder=0)
            #ax.set_ylim([-0.5,1.0])

        ax.legend(bbox_to_anchor=(1.0, 1.0))
        ax.set_xlabel('n_components')
        ax.set_ylabel('clustering score')
        plt.grid()
        #plt.legend(loc='out')
        plt.show()


    return clustering_scores

# ------------------------------------------------

def compare_embeddings(embedding1, embedding2, method='kabsch', return_reconstruction=False):
    '''
    Return the distance in RSME of two paired set of points
    after scaling, rotation and translation, using Umeyama algorithm.

    Args:
        X (array, NxD): set of N points with D dimensions
        Y (array, NxD): set of N points with D dimensions, paired set of points
        method (str): 'kabsch' (without scaling) or 'umeyama' (with scaling)
        return_reconstruction (bool): return reconstruction

    Returns:
        rsme (float): root square mean error between point sets after rigid alignment
    '''

    if embedding1.shape != embedding2.shape:

        if embedding1.shape[0] != embedding2.shape[0]:
            raise ValueError(f'Embeddings must have the same number of entries {embedding1.shape[0]} != {embedding2.shape[0]}')
        else:
            warnings.warn(f'Embedding do not have identical dimensionlity ({embedding1.shape[-1]} != {embedding2.shape[-1]}). Reducing dimensionality of the one with larger dimensionality with PCA.')

            # use PCA to reduce dimensionality to lowest one...
            if embedding1.shape[-1] > embedding2.shape[-1]:
                embedding1 = pca(embedding1, n_components=embedding2.shape[-1])
            elif embedding1.shape[-1] < embedding2.shape[-1]:
                embedding2 = pca(embedding2, n_components=embedding1.shape[-1])

    X = embedding1.copy()
    Y = embedding2.copy()
    n = X.shape[0]

    methods = ['umeyama', 'kabsch']
    if method not in methods: raise ValueError(f'method must be one of {methods}')
    if method == 'umeyama': use_umeyama = True
    else: use_umeyama = False

    def compute(X,Y):
        # compute rotation matrix, scaling factor and translation vector
        if use_umeyama:
            rsme, Y_hat = umeyama(X,Y,n)
        else:
            rsme, Y_hat = kabsch(X,Y)
        return rsme, Y_hat

    # compute transformations, with flips
    rsme_min = 9999999.9
    dims = X.shape[-1]
    for d in range(dims):
        for flip in [False, True]:
            X = embedding1.copy()
            X = (X - X.mean(axis=0)) / X.std(axis=0)
            Y = embedding2.copy()
            Y = (Y - Y.mean(axis=0)) / Y.std(axis=0)
            if flip:
                Y[:,d] = -Y[:,d]

            rsme1, Y_hat1 = compute(X,Y)
            rsme2, Y_hat2 = compute(Y,X)

            if rsme1 < rsme2:
                rsme = rsme1
                X = Y.copy()
                Y_hat = Y_hat1.copy()
            else:
                rsme = rsme2
                X = X.copy()
                Y_hat = Y_hat2.copy()

            #print(rsme)
            if rsme < rsme_min:
                rsme_min = rsme
                #print("rsme < rsme_min: ", rsme)
                result = (rsme, (X, Y_hat))

    if return_reconstruction:
        return result
    else:
        return result[0]

def kabsch(P, Q):
    """ The Kabsch algorithm

    http://en.wikipedia.org/wiki/Kabsch_algorithm

    The algorithm starts with two sets of paired points P and Q.
    P and Q should already be centered on top of each other.

    Each vector set is represented as an NxD matrix, where D is the
    the dimension of the space.

    The algorithm works in three steps:
    - a translation of P and Q
    - the computation of a covariance matrix C
    - computation of the optimal rotation matrix U

    The optimal rotation matrix U is then used to
    rotate P unto Q so the RMSD can be caculated
    from a straight forward fashion.

    """

    # Computation of the covariance matrix
    C = np.dot(P.T, Q)

    # Computation of the optimal rotation matrix
    # This can be done using singular value decomposition (SVD)
    # Getting the sign of the det(V)*(W) to decide
    # whether we need to correct our rotation matrix to ensure a
    # right-handed coordinate system.
    # And finally calculating the optimal rotation matrix U
    # see http://en.wikipedia.org/wiki/Kabsch_algorithm
    V, S, W = np.linalg.svd(C)
    d = (np.linalg.det(V) * np.linalg.det(W)) < 0.0

    if(d):
        S[-1] = -S[-1]
        V[:,-1] = -V[:,-1]

    # Create Rotation matrix U
    U = np.dot(V, W)

    # Rotate P
    P = np.dot(P, U)

    return rmsd(P,Q), P

def rmsd(V, W):
    """ Calculate Root-mean-square deviation from two sets of vectors V and W.
    """
    D = len(V[0])
    N = len(V)
    rmsd = 0.0
    for v, w in zip(V, W):
        rmsd += sum([(v[i]-w[i])**2.0 for i in range(D)])
    return np.sqrt(rmsd/N)

def mae(V, W):
    """ Calculate mean absolute error from two sets of vectors V and W.
    """
    D = len(V[0])
    N = len(V)
    mae = 0.0
    for v, w in zip(V, W):
        mae += sum([abs(v[i]-w[i]) for i in range(D)])
    return mae/N

def umeyama(X,Y,n):
    R, c, t = similarity_transform(X,Y)
    t2 = np.tile(t, (n,1))
    # reconstruct
    Y_hat = X.dot(R*c) + t2
    # compute RSME between Y_hat and Y
    rmse = np.sqrt(np.mean((Y_hat - Y)**2))
    #mae = np.mean(Y_hat - Y)
    return rmse, Y_hat

def similarity_transform(from_points, to_points):
    '''
    Rigid transformation of points using Umeyama algorithm (Kabsch algotithm with scaling).

    https://gist.github.com/CarloNicolini/7118015

    https://gist.github.com/dboyliao/f7f862172ed811032ba7cc368701b1e8
    '''
    assert len(from_points.shape) == 2, \
        "from_points must be a m x n array"
    assert from_points.shape == to_points.shape, \
        "from_points and to_points must have the same shape"

    N, m = from_points.shape

    mean_from = from_points.mean(axis = 0)
    mean_to = to_points.mean(axis = 0)

    delta_from = from_points - mean_from # N x m
    delta_to = to_points - mean_to       # N x m

    sigma_from = (delta_from * delta_from).sum(axis = 1).mean()
    sigma_to = (delta_to * delta_to).sum(axis = 1).mean()

    cov_matrix = delta_to.T.dot(delta_from) / N

    U, d, V_t = np.linalg.svd(cov_matrix, full_matrices = True)
    cov_rank = np.linalg.matrix_rank(cov_matrix)
    S = np.eye(m)

    if cov_rank >= m - 1 and np.linalg.det(cov_matrix) < 0:
        S[m-1, m-1] = -1
    elif cov_rank < m-1:
        raise ValueError("colinearility detected in covariance matrix:\n{}".format(cov_matrix))

    R = U.dot(S).dot(V_t)
    c = (d * S.diagonal()).sum() / sigma_from
    t = mean_to - c*R.dot(mean_from)

    return R, c, t

