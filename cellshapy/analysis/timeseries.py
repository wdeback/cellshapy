import numpy as np
import matplotlib.pyplot as plt
import warnings

#----------------------------------------------------------------
#  Analyse time series (and helper functions)
#----------------------------------------------------------------

def analyse_series(series, check_periodicity):
    '''

    Args:
        series (array): time series data
    Returns:
        result (dict):
            mean
            std
            periodicity
            period
            trend
            noise
    '''

    result = dict()

    # mean and stdev over whole time series
    result['mean'] = np.mean(series)
    result['median'] = np.median(series)
    result['min'] = np.min(series)
    result['max'] = np.max(series)
    from scipy.integrate import trapz
    result['auc'] = trapz(series)
    result['std'] = np.std(series)


    if check_periodicity:
        # periodicity detection
        result['periodicity'], result['period'] = _estimate_periodicity(series)

        # STL decomposition (seasonality and trend decomposition using Loess)
        stl = _stl_decomposition(series, result['period'])

        # linear regression of trend
        from scipy import stats
        r = stats.linregress(range(len(stl.trend)), stl.trend)
        result['noise'] = np.std(stl.resid)
    else:
        # linear regression
        from scipy import stats
        r = stats.linregress(range(len(series)), series)

    result['slope'] = r.slope
    #result['trend_intercept'] = r.intercept
    #result['trend_corr'] = r.rvalue
    return result


def _autocorrelation(series, plot=False):
    """Autocorrelation plot for time series.
    Adopted from pandas.plotting

    Args:
        series (array): time series
        plot (bool): whether or not to plot
    Returns:
        x (array): time lags
        acf (array): autocorrelation coefficients per time lag
        significant (array, bool): whether or not acf is above 99% confidence

    """
    from pandas.compat import lmap
    n = len(series)
    data = np.asarray(series)
    mean = np.mean(data)
    c0 = np.sum((data - mean) ** 2) / float(n)

    def r(h):
        return ((data[:n - h] - mean) *
                (data[h:] - mean)).sum() / float(n) / c0
    x = np.arange(n) + 1
    acf = lmap(r, x)
    z95 = 1.959963984540054
    z99 = 2.5758293035489004
    significant = (acf > z99 / np.sqrt(n))

    if plot:
        fig, ax = plt.subplots(1,1)
        ax.set_xlim(1, n)
        ax.set_ylim(-1.0, 1.0)
        ax.axhline(y=z99 / np.sqrt(n), linestyle='--', color='grey')
        ax.axhline(y=z95 / np.sqrt(n), color='grey')
        ax.axhline(y=0.0, color='black')
        ax.axhline(y=-z95 / np.sqrt(n), color='grey')
        ax.axhline(y=-z99 / np.sqrt(n), linestyle='--', color='grey')
        ax.set_xlabel("Lag")
        ax.set_ylabel("Autocorrelation")
        ax.plot(x, acf)
        ax.grid()
    return (x, acf, significant)

def _detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, valley=False, show=False, ax=None):

    """Detect peaks in data based on their amplitude and other features.

    __author__ = "Marcos Duarte, https://github.com/demotu/BMC"
    __version__ = "1.0.5"
    __license__ = "MIT"

    Parameters
    ----------
    x : 1D array_like
        data.
    mph : {None, number}, optional (default = None)
        detect peaks that are greater than minimum peak height (if parameter
        `valley` is False) or peaks that are smaller than maximum peak height
         (if parameter `valley` is True).
    mpd : positive integer, optional (default = 1)
        detect peaks that are at least separated by minimum peak distance (in
        number of data).
    threshold : positive number, optional (default = 0)
        detect peaks (valleys) that are greater (smaller) than `threshold`
        in relation to their immediate neighbors.
    edge : {None, 'rising', 'falling', 'both'}, optional (default = 'rising')
        for a flat peak, keep only the rising edge ('rising'), only the
        falling edge ('falling'), both edges ('both'), or don't detect a
        flat peak (None).
    kpsh : bool, optional (default = False)
        keep peaks with same height even if they are closer than `mpd`.
    valley : bool, optional (default = False)
        if True (1), detect valleys (local minima) instead of peaks.
    Returns
    -------
    ind : 1D array_like
        indeces of the peaks in `x`.
    Notes
    -----
    The detection of valleys instead of peaks is performed internally by simply
    negating the data: `ind_valleys = detect_peaks(-x)`

    The function can handle NaN's
    See this IPython Notebook [1]_.
    References
    ----------
    .. [1] http://nbviewer.ipython.org/github/demotu/BMC/blob/master/notebooks/DetectPeaks.ipynb
    """

    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
        if mph is not None:
            mph = -mph
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
                    & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])
    return ind

def _stl_decomposition(series, period, show=False):
    '''
    Decompose time series into trend, seasonality and residuals using Loess regression.

    Reference: Cleveland et al., 1990, ["Seasonal and Trend decomposition using Loess" time series decomposition](https://www.wessa.net/download/stl.pdf).
    Requires: https://github.com/jrmontag/STLDecompose

    Args:
        series (array): 1D time series data
        period (int): estimated period of seasonality (see estimate_periodicity())
        show (bool): plot data, trend, seasonality, residuals
    Returns:
        ... :

    '''

    # perform STL decomposition
    try:
       from stldecompose import decompose
    except:
        warnings.warn('STL decompostion requires `STLDecompose` from https://github.com/jrmontag/STLDecompose. Please install via `pip install stldecompose`')
        return None

    stl = decompose(series, period=period)

    if show:
        _ = stl.plot()
    return stl

def _estimate_periodicity(series, show=False):
    '''
    Estimate the period of seasonality in time series data via autocorrelation.

    Args:
        series (array): 1D time series data
    Returns:
        periodicity (float): strength of periodicity, first peak in (normalized) autocorrelation
        period (int): estimated period of seasonality, position of first peak in autocorrelation
    '''

    # compute autocorrelation coefficients
    x, acf, significant = _autocorrelation(series)

    # detect peaks in autocorrelation
    peaks = _detect_peaks(acf, mpd=2, show=False)

    # filter peaks with a significant autocorrelation (99% confidence)
    peaks = peaks[significant[peaks]]

    # filter peaks before crossing 0
    first_negative = np.argmax(np.array(acf) < 0.0)
    peaks = peaks[peaks > first_negative]

    # default values in case of no periodicity detected
    periodicity = 0.0
    period = 1

    if len(peaks) > 0:
        # define period as the first peak
        period = peaks[0]+1
        # strength of periodicity = autocorrelation coeffient of peak
        periodicity = acf[period]

        if show:
            plt.plot(x, acf)
            plt.scatter(peaks+1, [acf[peak] for peak in peaks], c='red', s=100)

        #print('Periodicity = {:.3f}'.format(periodicity))
        #print('Period = {}'.format(period))

    return periodicity, period


#----------------------------------------------------------------
#  Analyse trajectories (and helper functions)
#----------------------------------------------------------------

def analyse_trajectories(coords):
    '''
    Calculates statistics for trajectories in shape space (e.g. from pca).
    Currently, length and (curvature-based) tortuosity of trajectories are calculated,
    as well as the hull volume and mean pairwise distance of individual points.


    Args:
        coords (nxm array): list of coordinates, where m is number of components of embedding.
    Returns:
        dict with results
    '''

    if coords.ndim < 2:
        raise ValueError('Dimensionality of coords must be >= 2. Got shape {}'.format(coords.shape))
    result = dict()
    result['length of trajectory'] = _length_trajectory(coords)
    result['tortuosity of trajectory'] = _tortuosity_curvature(coords)
    result['hull volume of trajectory'] = _volume_convex_hull(coords)
    result['hull volume (robust) of trajectory'] = _volume_convex_hull(_remove_outliers(coords, m=1.5))
    result['hull volume (core) of trajectory'] = _volume_convex_hull(_remove_outliers(coords, m=1.0))
    result['mean pairwise distance in shape space'] = _mean_pairwise_distance(coords)

    return result

def _mean_pairwise_distance(points):
    '''
    Calculates the mean pairwise distance between set of points
    '''
    from scipy.spatial.distance import pdist
    return np.mean(pdist(points, metric='euclidean'))

def _length_trajectory(coords):
    # compute length of trajectory
    displacements = np.diff(coords, n=1, axis=0)
    lengths = np.linalg.norm(displacements, ord=2, axis=-1)
    length_trajectory = np.sum(lengths)
    return length_trajectory


def _tortuosity_curvature(coords):
    """
    Tortuosity defined sum of (unsigned) curvatures, here using spline
    See eqn. 6 and 7: http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7237216
    """

    from ..utils.contours import curvature_spline
    try:
        return np.mean(curvature_spline(coords, error=0.01, signed=False))
    except:
        warnings.warn('Tortuosity could not be computed')
        return None


def _volume_convex_hull(points):
    '''Compute volume of convex hull decsribed by points'''
    try:
        from scipy.spatial import ConvexHull
        hull = ConvexHull(points, qhull_options='En')
        return hull.volume
    except:
        warnings.warn('ConvexHull could not be computed')
        return None

def _remove_outliers(data, m=2.0):
    '''
    Remove outliers based on pairwise distances.
    Points beyond `m*mean` will be removed.

    Args:
        data (mxn array): list of n-dim points
        m (float): maximum allowed multiple of mean pairwise distance (lower will remove more points)
    Returns:
        data with outliers removed (array)
    '''
    from scipy.spatial.distance import pdist, squareform
    d = pdist(data, metric='euclidean')
    mean_dist = np.mean(d)
    mean_pdist = np.mean(squareform(d), axis=0)
    indices = np.argwhere(mean_pdist < m*mean_dist).flatten()
    return data[indices]
