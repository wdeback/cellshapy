import warnings

import numpy as np
#from keras.utils import plot_model
from keras import backend as K
from keras import callbacks, metrics
from keras.layers import (Activation, BatchNormalization, Conv2D, Dropout,
                          Conv2DTranspose, ConvLSTM2D, Dense, Flatten, Input,
                          Lambda, LeakyReLU, Reshape)
#from keras.datasets import mnist
from keras.losses import binary_crossentropy, mse
from keras.models import Model
from keras.optimizers import SGD, Adam, RMSprop


class VAE():
    '''Variational Auto-Encoder (VAE)'''

    def __init__(self, latent_dim:int,
                        tensor:np.array,
                        num_layers:int=4,
                        epochs:int=2000, # with early stopping
                        beta:float=1.0,
                        batch_size:int=128,
                        validation:float=0.0,
                        filters:int=16,
                        verbose:bool=True,
                        #mode:str='2DConv'
                        ):
        '''
        Variational autoencoder.

        Args:
            latent_dim: int
                length of latent vector
            tensor: ndarray
                3D array with data in `stxy` format (samples, time, contour elements)
            num_layers: int
                Number of convolutional layers in encoder and decoder
            epochs: int
                number of epochs to train, unless stopped by early stopping callback
            beta: float
                beta value to increase weight of KL loss (beta=1.0: normal VAE, beta>1.0: beta-VAE)
            batch_size: int
                batch size
            validation: float
                fraction of data to use as validation set
            filters: int
                number of convolutional filters/kernels to use in first/last layers
            verbose: bool
                verbosity level
            #mode: str
            #    type of autoencoder, one of ['ConvLSTM', '2DConv']
        '''
        self.latent_dim = latent_dim
        if tensor.ndim != 3:
            raise ValueError(f'Tensor must be 3D, but tensor shape found: {tensor.shape}')

        self.num_layers = num_layers

        # zero pad data to next pow2 image size
        shape_before = tensor.shape
        tensor = np.array([self.pad_to_next_divisible(x) for x in tensor])
        shape_after = tensor.shape
        if shape_after != shape_before:
            warnings.warn(f'Tensor was zero-padded from {shape_before} to {shape_after}.')

        # split train and test
        self.n_samples = tensor.shape[0]
        val_ind = np.random.choice(self.n_samples, size=int(validation*self.n_samples), replace=False)
        train_ind = np.array(list(set(np.arange(self.n_samples)) - set(val_ind)))
        # select training/validation data
        self.x_train = tensor[train_ind]
        self.x_validation = tensor[val_ind]
        # add empty channels dimension
        self.x_train = self.x_train[..., np.newaxis]
        self.x_validation = self.x_validation[..., np.newaxis]

        # normalize to maximum value (we take the 95% percentile to be robust)
        self.max_value = np.percentile(self.x_train, 95)
        self.x_train /= self.max_value
        self.x_validation /= self.max_value

        # get sizes etc.
        _, self.rows, self.columns, self.channels = self.x_train.shape
        self.batch_size = batch_size
        self.filters = filters
        self.beta = beta  # beta-VAE: controls weight of KL loss
        self.epochs = epochs
        self.verbose = verbose
        self.callbacks = []
        self.initialize()

    def initialize(self):

        # set allow_growth GPU memory option
        self.limit_GPU_memory()

        # optimizers
        #optimizer = SGD(lr=1e-5, decay=1e-6, momentum=0.5, nesterov=False)
        #optimizer = RMSprop(lr=0.001)#, nesterov=False) #decay=1e-6, nesterov=True)
        optimizer = Adam(lr=2e-5, amsgrad=True)

        # encoder
        self.E, shape = self.encoder(kernel_size=(3,3))

        # decoder
        self.D = self.decoder(kernel_size=(3,3), shape=shape)

        # VAE
        X = Input(shape=(self.rows, self.columns, self.channels))
        E_mean, E_logsigma = self.E(X) # encoder
        Z = self.sampler(E_mean, E_logsigma)
        output = self.D(Z)
            
        self.AE = Model(X, output, name='VAE')

        if self.verbose:
            self.E.summary()
            self.D.summary()
            self.AE.summary()

        # VAE loss = KL-divergence  + MSE reconstruction loss
        kl_loss = K.mean(-0.5 * K.sum(1 + E_logsigma - K.square(E_mean) - K.exp(E_logsigma), axis=-1))
        reconstruction_loss = self.rows * self.columns * metrics.mse(K.flatten(X), K.flatten(output))
        VAEloss = K.mean( reconstruction_loss + self.beta * kl_loss )
        self.AE.add_loss(VAEloss)
        self.AE.compile(optimizer=optimizer)

        # add metrics
        self.AE.metrics_tensors.append(reconstruction_loss)
        self.AE.metrics_names.append("reconstruction loss")
        self.AE.metrics_tensors.append(self.beta * kl_loss)
        self.AE.metrics_names.append("KL loss")



        # callbacks
        early_stopping = callbacks.EarlyStopping(monitor='loss', min_delta=1., patience=100, verbose=self.verbose,
                                                 mode='min', baseline=None, restore_best_weights=True)
        #reduce_learning_rate = callbacks.ReduceLROnPlateau(monitor='loss', patience=10, factor=0.1,
        #                                                   min_lr=1e-6, verbose=self.verbose)
        self.callbacks = [early_stopping]#, reduce_learning_rate]


    def fit(self, plot:bool=True):
        self.history = self.AE.fit(self.x_train,
                    epochs=self.epochs,
                    batch_size=self.batch_size,
                    validation_data=(self.x_validation, None),
                    verbose=self.verbose,
                    shuffle=True,
                    callbacks=self.callbacks)

        if plot:
            self.plot_history()
            # import matplotlib.pyplot as plt
            # fig, ax = plt.subplots(1,1)
            # ax.semilogy(self.history.history['loss'], label='loss')
            # if len(self.x_validation) > 0:
            #     ax.semilogy(self.history.history['val_loss'], label='val_loss')
            # plt.legend()

    def plot_history(self, start=0, end=None):
        if self.history is None or len(self.history.history) == 0:
            warnings.warn('No history found. Please run fit() first.')
        else:
            import matplotlib.pyplot as plt
            metrics = list(self.history.history.keys())
            if 'lr' in metrics: metrics.remove('lr')

            fig, ax = plt.subplots(len(metrics),1,figsize=(10,10), sharex=True)
            plt.subplots_adjust(hspace=.0)
            line_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

            # get list of losses and metrics
            x = self.history.epoch[start:end]
            # plot total loss
            ax[0].semilogy(x, self.history.history['loss'][start:end], label='loss', lw=3, color='k')
            # plot all other losses/metrics
            for i, metric in enumerate(metrics[1:]):
                ax[0].semilogy(x, self.history.history[metric][start:end], label=metric, lw=2, color=line_colors[i])

            # plot all losses/metrics in single plot
            for i, metric in enumerate(metrics[1:]):
                ax[i+1].semilogy(x, self.history.history[metric][start:end], label=metric, lw=2, color=line_colors[i])

            for a in ax:
                a.legend()
                a.set_ylabel('Loss (log)')
            ax[-1].set_xlabel('Epochs')




    def transform(self, X):
        '''Compute latent vectors from inputs X'''
        # zero-pad input to next pow2 size
        data = np.array([self.pad_to_next_divisible(x) for x in X])
        # add channel axis and normalize
        data = data[..., np.newaxis] / self.max_value
        return self.E.predict(data)[0]

    #TODO: test inverse transform
    def inverse_transform(self, X):
        '''Reconstruct from latent vector

        Args:
            X (ndarray, size = (None, VAE.latent_dim)): latent vectors
        Returns:
            output (ndarray): zero-cropped reconstructions, un-normalized to max value
        '''
        # decode, unnormalize and squeeze last axis
        reconstructions = np.squeeze(self.D.predict(X), axis=-1) * self.max_value
        # crop off zero-padding
        reconstructions_cropped = np.array([self.crop(r) for r in reconstructions])
        return reconstructions_cropped

    def sampling(self, args):
        """Reparameterization trick by sampling fr an isotropic unit Gaussian.
        # Arguments
            args (tensor): mean and log of variance of Q(z|X)
        # Returns
            z (tensor): sampled latent vector
        """
        z_mean, z_log_var = args

        batch = K.shape(z_mean)[0]
        dim = K.int_shape(z_mean)[1]
        # by default, random_normal has mean=0 and std=1.0
        epsilon = K.random_normal(shape=(batch, dim))
        return z_mean + K.exp(0.5 * z_log_var) * epsilon

    def encoder(self, kernel_size):
        inp = Input(shape=(self.rows, self.columns, self.channels))
        x = Conv2D(filters=self.filters, kernel_size=kernel_size, strides=2, padding='same')(inp)
        #x = Dropout(rate=0.5)(x)
        x = BatchNormalization(epsilon=1e-5)(x)
        x = LeakyReLU(alpha=0.2)(x)

        for i in range(self.num_layers-1):
            self.filters *= 2
            x = Conv2D(filters=self.filters, kernel_size=kernel_size, strides=2, padding='same')(x)
            #x = Dropout(rate=0.5)(x)
            x = BatchNormalization(epsilon=1e-5)(x)
            x = LeakyReLU(alpha=0.2)(x)

        shape = K.int_shape(x)
        x = Flatten()(x)

        mean = Dense(self.latent_dim)(x)
        logsigma = Dense(self.latent_dim, activation='tanh')(x)
        encoder_model = Model([inp], [mean, logsigma])
        return encoder_model, shape
        
    def sampler(self, mean, logsigma):
        z = Lambda(self.sampling, output_shape=(self.latent_dim,))([mean, logsigma])
        return z


    def decoder(self, kernel_size, shape):
        inp = Input(shape=(self.latent_dim,))

        x = Dense(shape[1]*shape[2]*shape[3])(inp)
        x = Reshape((shape[1],shape[2],shape[3]))(x)
        x = BatchNormalization(epsilon=1e-5)(x)
        x = Activation('relu')(x)

        for i in range(self.num_layers-1):
            x = Conv2DTranspose(filters=self.filters, kernel_size=kernel_size, strides=2, padding='same')(x)
            x = BatchNormalization(epsilon=1e-5)(x)
            x = Activation('relu')(x)
            self.filters //= 2

        x = Conv2DTranspose(filters=self.channels, kernel_size=kernel_size, strides=2, padding='same')(x)
        o = Activation('tanh')(x)

        decoder_model = Model(inp, o)
        return decoder_model

    def limit_GPU_memory(self):
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
        config.log_device_placement = True  # to log device placement (on which device the operation ran)
        sess = tf.Session(config=config)
        set_session(sess)  # set this TensorFlow session as the default session for Keras

    def next_int_divisible_by(self, value, divisor):
        '''if `value` not already divisible by `divisor`, returns next integer divisible by `divisor`'''
        import math
        if value%divisor == 0:
            return value
        else:
            return value + (divisor - value%divisor)

    def pad_to_next_divisible(self, data):
        '''Pad 2D array to next compatible shape'''
        narray = data
        length, height = narray.shape
        divisor = 2**(self.num_layers)
        diff_length = self.next_int_divisible_by(value=length, divisor=divisor) - length
        diff_height = self.next_int_divisible_by(value=height, divisor=divisor) - height
        #print("diff_length :", diff_length)
        #print("diff_height :", diff_height)

        if length % 2 == 0:
            pad_width_lr = diff_length // 2
            pad_width_lr = [pad_width_lr, pad_width_lr]
        else:
            # need an uneven padding for odd-number lengths
            left_pad = diff_length // 2
            right_pad = diff_length - left_pad
            pad_width_lr = [left_pad, right_pad]

        if height % 2 == 0:
            pad_width_tp = diff_height // 2
            pad_width_tp = [pad_width_tp, pad_width_tp]
        else:
            # need an uneven padding for odd-number lengths
            top_pad = diff_height // 2
            bottom_pad = diff_height - top_pad
            pad_width_tp = [top_pad, bottom_pad]
        self.padding = [pad_width_lr, pad_width_tp]
        narray = np.pad(narray, [pad_width_lr, pad_width_tp], 'constant')
        return narray

    def crop(self, arr):
        '''crop zeros from numpy array outside of bounding box
        https://stackoverflow.com/a/39466129'''

        pad_width_lr, pad_width_tp = self.padding
        x_pad_l, x_pad_r = pad_width_lr
        # crop array
        out = arr[x_pad_l : arr.shape[0]-x_pad_r]
        return out
