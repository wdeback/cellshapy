
def cluster(data, cl_method, n_clusters):
        '''
        Cluster data using a specific clustering algorithm.

        Args:
            data (array): data to be clustered
            cl_method (str): one of ['kmeans', 'hdbscan', 'minibatchkmeans',
                                     'affinitypropagation', 'meanshift',
                                     'agglomerativeclustering',
                                     'spectral']
            n_clusters (str): number of clusters (interpreted as 'min_cluster_size' in 'hdbscan')
        '''

        cl_methods = ["kmeans",
                      "hdbscan",
                      "minibatchkmeans",
                      "affinitypropagation",
                      "meanshift",
                      "agglomerativeclustering",
                      "spectral"]

        if cl_method not in cl_methods:
            raise ValueError(f"Unknown `cl_method` ({cl_method}). Choose one of {cl_methods}.")

        if cl_method == "kmeans":
            from sklearn.cluster import KMeans
            cl = KMeans(n_clusters=n_clusters)
        elif cl_method == "hdbscan":
            from cellshapy.config import pkgs
            if not pkgs.hdbscan: #pragma: no cover
                raise ValueError("Package hdbscan is not installed.")
            from hdbscan import HDBSCAN
            if n_clusters is not None and cl_method == 'hdbscan':
                import warnings
                warnings.warn("'n_clusters' is interpreted as 'min_cluster_size' in HDBSCAN.")
            cl = HDBSCAN(min_cluster_size=n_clusters)
        elif cl_method == "minibatchkmeans":
            from sklearn.cluster import MiniBatchKMeans
            cl = MiniBatchKMeans(n_clusters=n_clusters)
        elif cl_method == "affinitypropagation":
            from sklearn.cluster import AffinityPropagation
            if n_clusters is not None:
                cl = AffinityPropagation(damping=n_clusters)
                import warnings
                warnings.warn("'n_clusters' is interpreted as 'damping' in 'affinitypropagation'.")
            else:
                cl = AffinityPropagation()
        elif cl_method == "meanshift":
            from sklearn.cluster import MeanShift
            cl = MeanShift()
        elif cl_method == "agglomerativeclustering":
            from sklearn.cluster import AgglomerativeClustering
            cl = AgglomerativeClustering(n_clusters=n_clusters)
        elif cl_method == "spectral":
            from sklearn.cluster import SpectralClustering
            cl = SpectralClustering(n_clusters=n_clusters,
                                    assign_labels="discretize",
                                    random_state=0)
        else: #pragma: no cover
            ValueError(f"Unsupported clustering method {cl_method}. cl_method "
                       f"needs to be one of {cl_methods}")

        return cl.fit(data).labels_
