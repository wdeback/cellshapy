#! /usr/bin/env python3

packages = ['tensorly',
            'multiprocessing',
            'MulticoreTSNE',
            'umap',
            'pyefd',
            'stldecompose',
            'umap',
            'deap',
            'hdbscan',
            'mahotas']


def checkPackage(packagename):
    import importlib

    spec = importlib.util.find_spec(packagename)
    avail = spec is not None

    if not avail:
        import warnings
        warnings.warn(f"Package {packagename} is missing. " \
                      f"Some features are not available")

    return avail


class TestPackage():
    __broken_attributes__ = (
        '_ipython_canary_method_should_not_exist_',
        '_ipython_display_',
        '__wrapped__',
        '_repr_mimebundle_'
    )

    def __init__(self):
        self._pkg_dict = dict()

    def __getattr__(self, name):
        if name in self.__broken_attributes__:
            raise AttributeError()
        if name not in self._pkg_dict:
            self._pkg_dict[name] = checkPackage(name)
        return self._pkg_dict[name]


pkgs = type('', (), {})()
[setattr(pkgs, package, checkPackage(package)) for package in packages]

pkgs2 = TestPackage()

# Usage:
# from cellshapy.config import pkgs
# if pkgs.pkg_name:
#     # Do something
# else:
#     # Do something else
#
# OR
#
# from cellshapy.config import pkgs2
# if pkgs2.pkg_name:
#     # Do something
# else:
#     # Do something else
