import appdirs
from .config import config
from .config import _is_run_from_ipython
from .config import tqdm as context_sensitive_tqdm
from .features import pkgs2 as pkgs

config_path = appdirs.user_config_dir("cellshapy", "IMB")
config = config(config_path)

tqdm = context_sensitive_tqdm()

__all__ = ["config", "config_path", "tqdm", "pkgs"]
