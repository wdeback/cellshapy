def _str_to_bool(val):
    return val.lower() in ['true', 't', '1', 'y', 'yes']

_defaults = dict()
_defaults["contours", "n_points",              int]   = 50
_defaults["contours", "smoothing",             int]   = 0
_defaults["plotting", "height",                int]   = 300
_defaults["plotting", "width",                 int]   = 250
_defaults["plotting", "duration",              float] = 5.0
_defaults["print",    "limit.videocollection", int]   = 3
_defaults["print",    "limit.video",           int]   = 2
_defaults["print",    "debug",                 bool]  = False

_converter = dict()
_converter[bool] = {"s2t": _str_to_bool, "t2s": str}

class config:
    def __init__(self, path):
        self._path = path
        self._typetable = _defaults.keys()
        self._config = config._load_config(path)


    def _find_type(self, key):
        c, k = key
        t = [_t for _c, _k, _t in self._typetable if c == _c and k == _k]

        if len(t) == 0:
            import warnings
            warnings.warn(f"No default entry for key {(c, k)}")
            return None

        if len(t) > 1:
            raise KeyError(f"More than one entry for key {(c, k)}")

        return t[0]


    @staticmethod
    def _default_config():
        import configparser
        conf = configparser.SafeConfigParser()
        keys = _defaults.keys()
        cats = set([c for c, _, _ in keys])
        for cat in cats:
            conf.add_section(cat)
        for cat, key, t in keys:
            conf.set(cat, key, str(_defaults[cat, key, t]))

        return conf


    @staticmethod
    def _load_config(path):
        import os
        conf = config._default_config()
        if not os.path.exists(path):
            print(f"Creating new config at {path}")
        else: # TODO: what does this do to existing values/non-existant entries?
            print(f"Reading config from {path}")
            conf.read(path)

        config._save_config(conf, path)

        return conf


    @staticmethod
    def _save_config(config, path):
        import os
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, "w") as f:
            config.write(f)


    def __getitem__(self, key):
        #print(f"Request to get {key}")
        if len(key) != 2:
            raise KeyError("Category and key name required.")

        t = self._find_type(key)
        c, k = key

        if t is not None:
            if t in _converter:
                return _converter[t]["s2t"](self._config[c][k])
            else:
                return t(self._config[c][k])
        else: #pragma: no cover
            return self._config[c][k]


    def __setitem__(self, key, value): #pragma: no cover
        #print(f"Request to set {key} to {value}")
        t = self._find_type(key)

        if t is not None:
            if not instanceof(value, t):
                raise ValueError(f"Value {value} is not of type {t}")

        if len(key) != 2:
            raise KeyError("Category and key name required.")
        c, k = key

        if t in _converter:
            val = _converter[t]["t2s"](value)
        else:
            val = str(value)

        self._config[c][k] = val
        config._save_config(self._config, self._path)

    def __repr__(self):
        s = ''
        for c in self._config:
            s += f'{c}\n'
            for k in self._config[c]:
                value = self._config[c][k]
                s += f'  {k} = {value}\n'
        return s




def _is_run_from_ipython(): #pragma: no cover
    """Determines whether run from Ipython.
    Only affects progress bars.
    """
    try:
        __IPYTHON__
        return True
    except NameError:
        return False

def tqdm():
    try:
        if (_is_run_from_ipython()): #pragma: no cover
            from tqdm import tqdm_notebook as tqdm
        else:
            from tqdm import tqdm
        return tqdm
    except ImportError: #pragma: no cover
        UserWarning("Module tqdm is not installed. "
                    "You won't have fancy progressbars.")
        return lambda x: x

# customize warnings such that it omits the line of source code.
# https://stackoverflow.com/a/26256592
import warnings
formatwarning_orig = warnings.formatwarning
warnings.formatwarning = lambda message, category, filename, lineno, line=None: \
    formatwarning_orig(message, category, filename, lineno, line='')
