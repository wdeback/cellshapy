from setuptools import setup

setup(name='cellshapy',
      version='0.01',
      description='Cell shape dynamics analysis',
      url='http://gitlab.com/imb-dev/cellshapy',
      author='Walter de Back and Sebastian Wagner',
      author_email='walter@deback.net',
      license='MIT',
      packages=['cellshapy'],
      install_requires=[
          'joblib',
          'numpy',
          'scikit-image',
          'scikit-learn',
          'pandas',
          'matplotlib',
          'umap-learn',
          'bitarray',
          'statsmodels',
          'appdirs',
          'tensorly'
      ],
      dependency_links = [
      ],
      zip_safe=False)
