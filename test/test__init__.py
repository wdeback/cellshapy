import cellshapy
from nose.tools import raises

@raises(ValueError)
def testquiet_hook():
    # Acutally doesn't test the hook, as the exception is caught
    raise ValueError("Should get caught")
