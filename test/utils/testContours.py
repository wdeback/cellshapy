import warnings
import numpy as np
import matplotlib as plt
plt.use('Agg')

img = np.array([
    [0, 0,  0,  0,  0, 0,   0,   0, 0, 0],  # 0
    [0, 0, 20, 35,  0, 0,   0,   0, 0, 0],  # 1
    [0, 0, 30, 50, 15, 0,   0,   0, 5, 0],  # 2
    [0, 0,  0, 25,  0, 0,   0,   0, 0, 0],  # 3
    [0, 0,  0,  0,  0, 0,   0,   0, 0, 0],  # 4
    [0, 0,  0,  0,  0, 0, 100, 100, 0, 0],  # 5
    [0, 0,  0,  0,  0, 0, 100, 100, 0, 0]   # 6
    # 0  1  2   3   4  5     6    7  8  9
    ])
img2 = np.array([
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,0,0,0,0,0,0,0,0],
    [0,0,0,1,1,1,1,1,0,0,0,0,0,0,0],
    [0,0,0,1,1,1,1,1,1,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,1,0,0,0,0,0,0],
    [0,0,0,0,1,0,1,0,0,0,0,0,0,0,0],
    ])
bin_img = img.astype(np.bool).astype(np.int)

def testExtractContour():
    from cellshapy.utils.contours import extract_contour
    try:
        extract_contour(bin_img, num_points_polygon=0)
        assert False
    except ValueError:
        assert True

    try:
        extract_contour(bin_img, num_points_polygon=1)
        assert False
    except ValueError:
        assert True

    with warnings.catch_warnings(record=True) as w:
        c = extract_contour(bin_img, num_points_polygon=3)
        assert w

    with warnings.catch_warnings(record=True) as w:
        c = extract_contour(bin_img, num_points_polygon=50)
        assert w
        assert c.shape == (50, 2)

    with warnings.catch_warnings(record=True) as w:
        from copy import deepcopy
        im = deepcopy(bin_img)
        im[5:7, 6:8] = 0
        im[2, 8] = 0
        c = extract_contour(im, num_points_polygon=50)
        assert not w
        assert c.shape == (50, 2)

    with warnings.catch_warnings(record=True) as w:
        c = extract_contour(bin_img, num_points_polygon=50, plot=True)
        assert w
        assert c.shape == (50, 2)

def test_get_centroid_image():
    from cellshapy.utils.contours import _get_centroid_image

    im = np.array([[0, 1, 1], [0, 1, 1], [0, 0, 0]])
    cent = _get_centroid_image(im)
    assert cent is not None
    assert np.allclose(cent, [0.5, 1.5])

def test_check_clockwise():
    from cellshapy.utils.contours import _check_clockwise
    cw = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
    assert _check_clockwise(cw)
    assert not _check_clockwise(cw[::-1])

def test_make_clockwise():
    from cellshapy.utils.contours import _check_clockwise
    from cellshapy.utils.contours import _make_clockwise
    cw = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
    assert (_make_clockwise(cw) == cw).all()
    assert (_make_clockwise(cw[::-1]) == cw).all()

def test_make_anticlockwise():
    from cellshapy.utils.contours import _check_clockwise
    from cellshapy.utils.contours import _make_anticlockwise
    cw = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
    assert (_make_anticlockwise(cw) == cw[::-1]).all()
    assert (_make_anticlockwise(cw[::-1]) == cw[::-1]).all()

def test_bwperim_own():
    from cellshapy.utils.contours import _bwperim_own
    perim = np.array([
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,1,1,1,0,0,0,0,0,0,0,0],
        [0,0,0,1,0,0,0,1,0,0,0,0,0,0,0],
        [0,0,0,1,0,0,0,0,1,0,0,0,0,0,0],
        [0,0,0,0,1,1,0,1,1,0,0,0,0,0,0],
        [0,0,0,0,1,0,1,0,0,0,0,0,0,0,0],
    ]).astype(np.bool)
    perim_calc = _bwperim_own(img2)
    assert img2.shape == perim_calc.shape
    assert perim.shape == perim_calc.shape
    assert (perim == perim_calc).all()

def test_get_ordered_coords_from_edge():
    from cellshapy.utils.contours import _get_ordered_coords_from_edge
    exp = np.array([[0, 5], [1, 3], [2, 2], [3, 2], [4, 3],
                    [5, 3], [6, 4], [5, 5], [6, 6], [5, 7],
                    [5, 8], [4, 9], [3, 9], [2, 8], [1, 7],
                    [0, 6], [0, 5]])
    res = _get_ordered_coords_from_edge(img2)
    assert len(exp) == len(res)
    assert (exp == res).all()

def test_bezier_spline():
    pass

def testExtract_shape_descriptors():
    pass

def testPlot_features():
    pass

def testContour_area():
    pass

def testContour_perimeter():
    pass

def testGet_hull():
    pass

def testConvex_hull_area_perimeter():
    pass

def testCaliper_diameter():
    pass

def testDraw_rectangle():
    pass

def testUnit_vector():
    pass

def testAngle_between():
    pass

def testRotate():
    pass

def testClose_contour():
    pass

def testContour_centroid():
    pass

def testInscribing_circle():
    pass

def testEfd_features():
    pass

def testMoments_of_inertia():
    pass

def testCurvature_spline():
    pass

def testGet_curvature():
    pass

def testPlot_minimal_enclosing_triangle():
    pass

def testPlot_solidity():
    pass

def testPlot_wavinness():
    pass

def testPlot_equivalent_diameter():
    pass

def testPlot_curvature():
    pass

def testPlot_elliptic_fourier():
    pass

def testTo_mask():
    pass

def testIs_inside_polygon():
    pass

def test_det():
    pass

def testNormalize_area():
    pass


