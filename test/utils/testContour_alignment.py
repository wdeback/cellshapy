def testAlign_contours():
    import numpy as np
    from cellshapy.utils.contours import rotate
    from cellshapy.utils.contour_alignment import align_contours

    c1 = np.array(
        [[0, 0],
         [0, 1],
         [1, 1],
         [1, 0]])

    cx = [rotate(c1, (0.5, 0.5), i) for i in range(0, 360, 1)]

    ac = align_contours(cx)

    cf = ac[0]
    for c in ac:
        c = np.array(c)
        assert np.allclose(c, cf)

    ac, ms = align_contours(cx, return_mean_shape=True)
    assert ac is not None
    assert ms.shape == cx[0].shape

def test_align_contour():
    import numpy as np
    from cellshapy.utils.contour_alignment import _align_contour
    from cellshapy.utils.contour_alignment import _to_complex_representation
    from cellshapy.utils.contour_alignment import _to_cartesian_representation
    c1 = np.array([[0, 0], [0, 1], [1, 1], [1, 0]])
    cr = np.array([[-0.5, -0.5], [-0.5, 0.5], [0.5, 0.5], [0.5, -0.5]])

    c = _align_contour(_to_complex_representation(cr),
                         _to_complex_representation(c1))

    assert c is not None
    assert np.allclose(_to_cartesian_representation(c), c1) or \
           np.allclose(_to_cartesian_representation(c), cr)


