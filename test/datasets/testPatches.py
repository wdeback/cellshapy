def testStar():
    from cellshapy.datasets import generateShape
    from cellshapy.datasets.patches import Star

    s = generateShape(Star, numSpikes=7, inner=10, outer=20)

    assert s.shape == (14, 2)

    try:
        s == generateShape(Star, numSpikes=0, inner=10, outer=20)
        assert False
    except ValueError:
        assert True
