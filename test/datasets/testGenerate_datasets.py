def testGenerate_ellipses():
    from cellshapy.datasets.generate_datasets import generate_ellipses

    es = generate_ellipses(xys=None, heights=None, widths=None, angles=None, same_length=True)
    assert es.shape == (20, 619, 2)

    es = generate_ellipses(xys=[(0, 0)]*5, heights=None, widths=None, angles=None, same_length=True)
    assert es.shape == (5, 619, 2)

    try:
        es = generate_ellipses(xys=[(0, 0)]*5, heights=[2]*4)
        assert False
    except ValueError:
        assert True


def testShapeGenerator():
    import tempfile as tmp
    import shutil
    import os
    from cellshapy.datasets.generate_datasets import generate_ellipses, ShapeGenerator
    sg = ShapeGenerator(generate_ellipses)

    tmp_d = tmp.mkdtemp()
    sg.generateTiffVideo(os.path.join(tmp_d, "foo.tiff"))

    shutil.rmtree(tmp_d)
    assert True


def testEllipseGenerator():
    from cellshapy.datasets.generate_datasets import EllipseGenerator

    g = EllipseGenerator()
    arr = g.generateArray()

    assert arr.shape == (20, 100, 100)


def testGenerateShape():
    from cellshapy.datasets.generate_datasets import generateShape
    from matplotlib.patches import Ellipse, Rectangle

    rect = generateShape(Rectangle, xy=(0, 0), width=1, height=1)

    assert (rect == [[0, 0], [1, 0], [1, 1], [0, 1]]).all()

    try:
        rect = generateShape(Rectangle, n_points=3, xy=(0, 0), width=1, height=1)
        assert False
    except ValueError:
        assert True

    rect = generateShape(Rectangle, n_points=12, xy=(0, 0), width=1, height=1)
    assert rect.shape == (48, 2)


def testGenerateClosestVertexMapping():
    import numpy as np
    from cellshapy.datasets.generate_datasets import generateClosestVertexMapping

    poly1 = np.array([[1.0, 1.0], [2.0, 1.0], [2.0, 2.0], [1.0, 2.0]])
    poly2 = np.array([[0.5, 2.5], [0.5, 0.5], [2.5, 0.5], [2.4, 2.4]])

    mapping = generateClosestVertexMapping(poly1, poly2)

    assert (mapping == [[2,3], [3, 0], [0, 1], [1, 2]]).all()


def testClosePolygon():
    import numpy as np
    from cellshapy.datasets.generate_datasets import closePolygon

    poly = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
    poly_c = closePolygon(poly)

    assert (poly_c[0] == poly_c[-1]).all()


def testOpenPolygon():
    import numpy as np
    from cellshapy.datasets.generate_datasets import openPolygon

    poly = np.array([[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]])
    poly_o = openPolygon(poly)

    assert (poly_o[0] != poly_o[-1]).any()


def testExpandPolygons():
    import numpy as np
    from cellshapy.datasets.generate_datasets import expandPolygons

    p1 = np.array([[ 0., 1.], [-1., 0.], [-0., -1.], [1., -0.]])
    p2 = np.array([[ 0., 1.], [-0.951, 0.309], [-0.588, -0.809], [0.588, -0.809], [0.951, 0.309]])

    xp1, xp2 = expandPolygons(p1, p2)

    assert len(xp1) == len(xp2)
    assert all(p in xp1 for p in p1)
    assert all(p in xp2 for p in p2)


def testMorph():
    import numpy as np
    from cellshapy.datasets.generate_datasets import morph, expandPolygons

    p1 = np.array([[ 0., 1.], [-1., 0.], [-0., -1.], [1., -0.]])
    p2 = np.array([[ 0., 1.], [-0.951, 0.309], [-0.588, -0.809], [0.588, -0.809], [0.951, 0.309]])

    xp1, xp2 = expandPolygons(p1, p2)
    m = morph(p1, p2, 23)

    assert len(m) == 23
    assert (m[0] == xp1).all()
    assert (m[-1] == xp2).all()


def testMorphGenerator():
    import numpy as np
    from cellshapy.datasets.generate_datasets import morph, MorphGenerator

    p1 = np.array([[ 0., 1.], [-1., 0.], [-0., -1.], [1., -0.]])
    p2 = np.array([[ 0., 1.], [-0.951, 0.309], [-0.588, -0.809], [0.588, -0.809], [0.951, 0.309]])

    m = morph(p1, p2, 23)
    mg = MorphGenerator(p1, p2, 23)

    assert (m == mg.generate()).all()


def testMorph_chain():
    import numpy as np
    from cellshapy.datasets.generate_datasets import generateShape, morph_chain
    from matplotlib.patches import RegularPolygon

    s1 = generateShape(RegularPolygon, xy=(0, 0), numVertices=4, radius=1)
    s2 = generateShape(RegularPolygon, xy=(0, 0), numVertices=5, radius=1)
    s3 = generateShape(RegularPolygon, xy=(0, 0), numVertices=7, radius=1)
    s4 = generateShape(RegularPolygon, xy=(0, 0), numVertices=9, radius=1)

    import warnings
    with warnings.catch_warnings(record=True) as w:
        try:
            mc = morph_chain(shapes=[s1, s2, s3, s4], steps=None, each_steps=None)
        except ValueError: # We need this exception, so we don't actually calculate
            assert w

    try:
        mc = morph_chain(shapes=[s1], steps=20, each_steps=None)
        assert False
    except ValueError:
        assert True

    try:
        mc = morph_chain(shapes=[s1, s2], steps=[20], each_steps=20)
        assert False
    except ValueError:
        assert True

    try:
        mc = morph_chain(shapes=[s1, s2], steps=None, each_steps=None)
        assert False
    except ValueError:
        assert True

    try:
        mc = morph_chain(shapes=[s1, s2], steps=[20, 20], each_steps=None)
        assert False
    except ValueError:
        assert True

    try:
        mc = morph_chain(shapes=[s1, s2], steps=None, each_steps=[1, 2, 3])
        assert False
    except ValueError:
        assert True

    mc = morph_chain(shapes=[s1, s2], steps=10, each_steps=None)   # 10 steps, 20 vertices, 2 points
    assert mc.shape == (10, 20, 2)
    mc = morph_chain(shapes=[s1, s2], steps=[10], each_steps=None) # 10 steps, 20 vertices, 2 points
    assert mc.shape == (10, 20, 2)
    mc = morph_chain(shapes=[s1, s2], steps=None, each_steps=10)   # 10 steps, 20 vertices, 2 points
    assert mc.shape == (10, 20, 2)


def testMorphChainGenerator():
    import numpy as np
    from cellshapy.datasets.generate_datasets import generateShape, morph_chain, MorphChainGenerator
    from matplotlib.patches import RegularPolygon

    s1 = generateShape(RegularPolygon, xy=(0, 0), numVertices=4, radius=1)
    s2 = generateShape(RegularPolygon, xy=(0, 0), numVertices=5, radius=1)
    s3 = generateShape(RegularPolygon, xy=(0, 0), numVertices=7, radius=1)

    mc1 = morph_chain(shapes=[s1, s2, s3], steps=[10, 20], each_steps=None)
    mcg = MorphChainGenerator(shapes=[s1, s2, s3], steps=[10, 20], each_steps=None)
    mc2 = mcg.generate()

    assert (mc1 == mc2).all()


def testMorph():
    import numpy as np
    from cellshapy.datasets.generate_datasets import generateShape, morph_chain, MorphChainGenerator, Morph
    from matplotlib.patches import RegularPolygon

    s1 = generateShape(RegularPolygon, xy=(0, 0), numVertices=4, radius=1)
    s2 = generateShape(RegularPolygon, xy=(0, 0), numVertices=5, radius=1)
    s3 = generateShape(RegularPolygon, xy=(0, 0), numVertices=7, radius=1)

    mc1 = morph_chain(shapes=[s1, s2, s3], steps=[10, 20], each_steps=None)
    mc2 = MorphChainGenerator(shapes=[s1, s2, s3], steps=[10, 20], each_steps=None).generate()
    mc3 = Morph(s1).morph_to(s2, 10).morph_to(s3, 20).generate()

    assert (mc1 == mc2).all()
    assert (mc2 == mc3).all()

    try:
        mc = Morph([[1, 2]]).generate()
        assert False
    except ValueError:
        assert True

    try:
        mc = Morph(np.array([[[1, 2]]])).generate()
        assert False
    except ValueError:
        assert True

    try:
        mc = Morph(np.array([[1, 2, 3]]))
        assert False
    except ValueError:
        assert True

    arr = Morph(s1).morph_to(s2, 10).morph_to(s3, 20).generateArray()
    assert arr.shape == (30, 100, 100)

    import tempfile as tmp
    import os
    import shutil

    tmp_d = tmp.mkdtemp()
    tmp_f = os.path.join(tmp_d, "foo.tiff")
    Morph(s1).morph_to(s2, 10).morph_to(s3, 20).generateTiffVideo(tmp_f)
    shutil.rmtree(tmp_d)
    assert True
