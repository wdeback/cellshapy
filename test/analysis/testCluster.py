from cellshapy.analysis.cluster import cluster
import numpy as np

dat = np.array(
        [[0.8971938 , 0.21369471],
         [0.79359004, 0.12315395],
         [0.28983771, 0.32496786],
         [0.76541068, 0.51885572],
         [0.29579958, 0.29273316],
         [0.05846938, 0.28746022],
         [0.93511634, 0.39040558],
         [0.09061179, 0.01427457],
         [0.91159334, 0.07244455],
         [0.36990989, 0.73011203]])

def testCluster():
    pass

def testClusterUnknownMethod():
    try:
        ret = cluster(dat, cl_method="foo bar baz", n_clusters=2)
        assert False
    except ValueError:
        assert True

def testClusterKmeans():
    ret = cluster(dat, cl_method="kmeans", n_clusters=2)
    assert len(ret) == len(dat)

def testClusterHdbscan():
    import warnings
    with warnings.catch_warnings(record=True) as w:
        ret = cluster(dat, cl_method="hdbscan", n_clusters=2)
        assert ([0, 0, 1, 0, 1, 1, 0, 1, 0, 1] == ret).all()
        assert w

def testClusterMinibatchkmeans():
    ret = cluster(dat, cl_method="minibatchkmeans", n_clusters=2)
    assert len(ret) == len(dat)

def testClusterAffinitypropagation():
    ret = cluster(dat, cl_method="affinitypropagation", n_clusters=2)
    assert ([0, 0, 1, 0, 1, 1, 0, 1, 0, 1] == ret).all()

def testClusterMeanshift():
    ret = cluster(dat, cl_method="meanshift", n_clusters=2)
    assert ([0, 0, 1, 0, 1, 1, 0, 3, 0, 2] == ret).all()

def testClusterAgglomerativeclustering():
    ret = cluster(dat, cl_method="agglomerativeclustering", n_clusters=2)
    assert ([1, 1, 0, 1, 0, 0, 1, 0, 1, 0] == ret).all()

def testClusterSpectral():
    ret = cluster(dat, cl_method="spectral", n_clusters=2)
    assert ([0, 0, 1, 0, 1, 1, 0, 1, 0, 1] == ret).all()
