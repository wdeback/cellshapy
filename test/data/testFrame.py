from cellshapy import Frame
import numpy as np
from nose.tools import raises
import warnings

import matplotlib as plt
plt.use("Agg")

base_ar = np.array([
            [0, 0, 0,  0,  0,  0,   0,   0, 0, 0],  # 0
            [0, 0, 20, 35, 0,  0,   0,   0, 0, 0],  # 1
            [0, 0, 30, 50, 15, 0,   0,   0, 5, 0],  # 2
            [0, 0, 0,  25, 0,  0,   0,   0, 0, 0],  # 3
            [0, 0, 0,  0,  0,  0,   0,   0, 0, 0],  # 4
            [0, 0, 0,  0,  0,  0, 100, 100, 0, 0],  # 5
            [0, 0, 0,  0,  0,  0, 100, 100, 0, 0]   # 6
            # 0  1  2  3  4  5  6  7  8  9
            ])

none_ar = None

empty_1d_ar = np.array([])

empty_2d_ar = np.array([[]])


def check__init__(f, ar):
    from bitarray import bitarray
    assert isinstance(f.mask, list)
    for ba in f.mask:
        assert isinstance(ba, bitarray)

    assert Frame._bitarray_to_np(f.mask).dtype == np.bool
    assert f.centroid.shape == (2,)

    # check that all features are there
    for key in ['area', 'area bbox', 'area hull', 'aspect ratio', 'circularity',
                'eccentricity', 'elongation', 'equivalent diameter',
                'inscribing circle diameter', 'major axis length',
                'max caliper diameter', 'mean curvature',
                'min caliper diameter', 'minor axis length', 'perimeter',
                'perimeter bbox', 'perimeter hull', 'perimeter to area ratio',
                'solidity', 'waviness']:
        assert key in f.features.keys()

    # should not have been called yet
    assert f.contour_aligned is None
    assert f.polygon is None
    assert isinstance(f.embedding, dict)


# __init__
def test__init__base_ar():
    ar = base_ar.astype(np.bool).astype(np.integer)
    with warnings.catch_warnings(record=True) as w:
        f = Frame(ar, frame_index=0, video_index=0, min_size=2)
        assert w

    check__init__(f, ar)

    # Test that old objects are still there
    m = Frame._bitarray_to_np(f.mask)
    assert m[1:3, 2:4].all()
    assert m[3, 3]
    assert m[2, 4]
    assert m[5:7, 6:8].all()

    # Test that the single object was removed
    assert not m[2, 8]


@raises(ValueError)
def test__init__none_ar():
    Frame(none_ar, frame_index=0, video_index=0, min_size=2)


@raises(ValueError)
def test__init__empty_1d_ar():
    Frame(empty_1d_ar, frame_index=0, video_index=0, min_size=2)


@raises(ValueError)
def test__init__empty_2d_ar():
    Frame(empty_1d_ar, frame_index=0, video_index=0, min_size=2)


# from_array
def testfrom_array():
    ar = base_ar.astype(np.bool).astype(np.integer)
    with warnings.catch_warnings(record=True) as w:
        f1 = Frame(ar, frame_index=0, video_index=0, min_size=2)
        f2 = Frame.from_array(ar, frame_index=0, video_index=0, min_size=2)
        assert w
        assert f1 == f2


# from_file
def testfrom_file():
    import tempfile
    import os
    import skimage.io

    ar = base_ar.astype(np.bool).astype(np.integer)

    with warnings.catch_warnings(record=True) as w:
        f1 = Frame(ar, frame_index=0, video_index=0, min_size=2)
        assert w

    with warnings.catch_warnings(record=True) as w:
        _, tmp = tempfile.mkstemp(suffix=".png")
        skimage.io.imsave(tmp, ar)
        assert w

    with warnings.catch_warnings(record=True) as w:
        f2 = Frame.from_file(tmp, frame_index=0, video_index=0, min_size=2)
        os.unlink(tmp)
        assert w

    f2.filename = None
    assert f1 == f2


# show
def testshow():
    ar = base_ar.astype(np.bool).astype(np.integer)

    with warnings.catch_warnings(record=True) as w:
        f = Frame(ar, frame_index=0, video_index=0, min_size=2)
        assert w

    # Should throw Errors, as the contours have not been aligned
    try:
        f.show(show_image=True, show_contour=True)
    except RuntimeError:
        assert True

    try:
        f.show(show_image=False, show_contour=True)
    except RuntimeError:
        assert True

    # Should work
    f.show(show_image=True, show_contour=False)

    # Should fail
    try:
        f.show(show_image=False, show_contour=False)
    except ValueError:
        assert True

# extract_contour implicitly tested in constructor
# extract_features implicitly tested in constructor


# get_polygon
@raises(ValueError)
def testget_polygon_uncalculated():
    ar = base_ar.astype(np.bool).astype(np.integer)

    with warnings.catch_warnings(record=True) as w:
        f = Frame(ar, frame_index=0, video_index=0, min_size=2)
        assert(w)

    f.get_polygon()


def testget_polygon_calculated():
    from cellshapy import Video, VideoCollection
    ar = base_ar.astype(np.bool).astype(np.integer)

    with warnings.catch_warnings(record=True) as w:
        f = Frame(ar, frame_index=0, video_index=0, min_size=2)
        assert(w)

    v = Video(0)
    v.frames = [f]

    vc = VideoCollection()
    vc.videos = [v]

    vc.align_contours(show_samples=0)

    # Function still broken
    f.get_polygon()
