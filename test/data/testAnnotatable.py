from cellshapy import VideoCollection, Video, Frame


def testUpdateAnnotation():
    vc = VideoCollection()
    vc.videos = [Video(i) for i in range(5)]

    annotation = {"condition": 1}
    new_annotation = {"condition": 2}
    broken_annotation = ["foo"]

    [v.updateAnnotation(annotation) for v in vc]
    for v in vc:
        assert v.annotations["condition"] == 1

    [v.updateAnnotation(new_annotation) for v in vc]
    for v in vc:
        assert v.annotations["condition"] == 2

    try:
        [v.updateAnnotation(broken_annotation) for v in vc]
        assert False
    except ValueError:
        assert True

def testHasAnnotation():
    v = Video(0)
    v.updateAnnotation({"foo": "bar"})
    assert v.hasAnnotation("foo")
    assert not v.hasAnnotation("bar")
    assert not v.hasAnnotation("baz")

def testRemoveAnnotation():
    v = Video(0)
    v.updateAnnotation({"foo": "bar"})
    ret = v.removeAnnotation("foo")
    assert ret == "bar"
    assert not v.hasAnnotation("foo")

def testClearAnnotations():
    v = Video(0)
    v.updateAnnotation({"foo": "bar", "baz": 42})
    assert v.hasAnnotation("foo")
    assert v.hasAnnotation("baz")
    v.clearAnnotations()
    assert not v.hasAnnotation("foo")
    assert not v.hasAnnotation("baz")
    v.updateAnnotation({"ping": "pong"})
    assert v.hasAnnotation("ping")

def testSelectAnnotation():
    import numpy as np
    # create a VideoCollection and add some Videos
    vc = VideoCollection()
    vc.videos = [Video(i) for i in range(5)]

    # add some annotations to the VC and the Videos
    annotation = {"condition": 1}
    vc.updateAnnotation(annotation)
    for v in vc:
        v.updateAnnotation(annotation)

    # Change a single annotation
    vc[2].updateAnnotation({"condition": 0})
    vc[2].frames = [Frame(video_index=2,
                          frame_index=i,
                          image=np.array([[0, 0], [0, 1]])) for i in range(5)]
    [f.updateAnnotation({"condition": 0}) for f in vc[2]]

    # Define our predicate
    def condOne(an):
        return an.hasAnnotation('condition') and \
            an.annotations['condition'] == 1

    # Get the objects defined by the predicate
    sel = vc.selectAnnotation(condOne)

    # Assert that all condition annotations are 1
    for v in sel:
        assert v.annotations['condition'] == 1

    condZero = lambda x: x.hasAnnotation('condition') and \
        x.annotations['condition'] == 0
    sel2 = vc.selectAnnotation(condZero)
    assert isinstance(sel2, VideoCollection)
    assert len(sel2) == 1
    assert sel2[0].annotations['condition'] == 0

    vc._strange_internal = Video(23)
    sel3 = vc.selectAnnotation(condOne)
    assert sel3._strange_internal is None
    vc._strange_internal.updateAnnotation({'condition': 1})
    sel3 = vc.selectAnnotation(condOne)
    assert sel3._strange_internal is not None
