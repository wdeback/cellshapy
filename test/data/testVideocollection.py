from cellshapy import Frame, Video, VideoCollection
import numpy as np
import warnings
from nose.tools import raises
import itertools
import copy


import matplotlib as plt
plt.use("Agg")
plt.rcParams.update({'figure.max_open_warning': 0})

sources = ["descriptors", "contours"]
methods = ["pca", "tsne", "umap"]
combs = list(itertools.product(sources, methods))

def create_testdataset_arrays():
    f1 = np.array([
                [0, 0,   0,   0,   0, 0, 0, 0, 0, 0],  # 0
                [0, 0, 200, 220,   0, 0, 0, 0, 0, 0],  # 1
                [0, 0, 255, 255, 150, 0, 0, 0, 5, 0],  # 2
                [0, 0,   0, 250,   0, 0, 0, 0, 0, 0],  # 3
                [0, 0,   0,   0,   0, 0, 0, 0, 0, 0],  # 4
                [0, 0,   0,   0,   0, 0, 0, 0, 0, 0],  # 5
                [0, 0,   0,   0,   0, 0, 0, 0, 0, 0]   # 6
                # 0 1    2    3    4  5  6  7  8  9
                ])

    f2 = np.array([
                [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 0
                [0,   0, 200, 220,   0, 0, 0, 0, 0, 0],  # 1
                [0, 150, 255, 255, 150, 0, 0, 0, 5, 0],  # 2
                [0,   0,   0, 250,   0, 0, 0, 0, 0, 0],  # 3
                [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 4
                [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 5
                [0,   0,   0,   0,   0, 0, 0, 0, 0, 0]   # 6
                # 0   1    2    3    4  5  6  7  8  9
                ])

    f3 = np.array([
                [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 0
                [0,  50, 200, 225,   0, 0, 0, 0, 0, 0],  # 1
                [0, 150, 255, 255, 150, 0, 0, 0, 5, 0],  # 2
                [0,   0,   0, 250,   0, 0, 0, 0, 0, 0],  # 3
                [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 4
                [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 5
                [0,   0,   0,   0,   0, 0, 0, 0, 0, 0]   # 6
                # 0   1    2    3    4  5  6  7  8  9
                ])

    fs = [f1, f2, f3]

    refVid1 = Video(0)
    refVid2 = Video(1)
    refVid3 = Video(2)
    with warnings.catch_warnings(record=True):
        refVid1.frames = [Frame(image=f.astype(np.bool).astype(np.integer),
                                contour=None,
                                frame_index=i,
                                video_index=0)
                                for i, f in enumerate(fs)]
        refVid2.frames = [Frame(image=f.astype(np.bool).astype(np.integer),
                                contour=None,
                                frame_index=i,
                                video_index=1)
                                for i, f in enumerate(fs)]
        refVid3.frames = [Frame(image=f.astype(np.bool).astype(np.integer),
                                contour=None,
                                frame_index=i,
                                video_index=2)
                                for i, f in enumerate(fs)]

    vc = VideoCollection()
    vc.videos = [refVid1, refVid2, refVid3]
    return vc, [refVid1, refVid2, refVid3]


def create_testdataset_morphs():
    '''Generate video collection from morph generator'''
    from cellshapy import VideoCollection
    from cellshapy.datasets import MorphChainGenerator, generateShape
    from matplotlib.patches import Ellipse, RegularPolygon, Circle
    from cellshapy.datasets.patches import Star

    with warnings.catch_warnings(record=True):
        s1 = generateShape(Star, xy=(10, 0), numSpikes=5, inner=20, outer=40)
        s2 = generateShape(RegularPolygon, xy=(0, 20), numVertices=3, radius=80)
        s3 = generateShape(RegularPolygon, xy=(10, 0), numVertices=4, radius=80, orientation=np.deg2rad(45))
        s4 = generateShape(Ellipse, xy=(0, 20), height=160, width=50)
        s5 = generateShape(Circle, xy=(10, 0), radius=80)
        s6 = s1

        morph1 = MorphChainGenerator(shapes=[s1, s2, s3, s4, s5, s6], steps=[1, 2, 3, 4, 5])
        morph2 = MorphChainGenerator(shapes=[s6, s5, s4, s3, s2, s1], steps=[1, 2, 3, 4, 5])

    vid1 = morph1.generate()
    vid2 = morph2.generate()
    vc = VideoCollection.from_contours(contours=[vid1, vid2])
    return vc, [vid1, vid2]

dataset_generators = [create_testdataset_arrays,
                      create_testdataset_morphs]
datasets = [d() for d in dataset_generators]

refVC, (refVid1, refVid2, refVid3) = create_testdataset_arrays()

def check__init__(vids):
    vc = VideoCollection()
    vc.videos = vids
    pass

def test__init__():
    for dataset in datasets:
        _, vids = dataset
        yield check__init__, vids

def testfrom_glob_dir():
    import os
    import tempfile as tmp
    import shutil
    import skimage.io
    import warnings

    tmp_d = tmp.mkdtemp()
    print(f"Using temporary directory {tmp_d}")

    for i, v in enumerate(refVC):
        tmp_dd = os.path.join(tmp_d, str(i))
        os.mkdir(tmp_dd)
        print(f"Temp dir for video {tmp_dd}")

        for j, f in enumerate(v.frames):
            tmp_f = os.path.join(tmp_dd, f"{j}.png")
            m = Frame._bitarray_to_np(f.mask)
            ar = m.astype(np.bool).astype(np.integer)
            with warnings.catch_warnings(record=True):
                skimage.io.imsave(tmp_f, ar)

    with warnings.catch_warnings(record=True):
        testVC = VideoCollection.from_glob(f"{tmp_d}/*", n_jobs=1)

    shutil.rmtree(tmp_d)
    for v in testVC:
        v.filename = None
        for f in v:
            f.filename = None
    assert testVC == refVC

def testfrom_glob_composite():
    import os
    import tempfile as tmp
    from skimage.external import tifffile
    import shutil

    tmp_d = tmp.mkdtemp()
    print(f"Using temporary directory {tmp_d}")

    for i, v in enumerate(refVC):
        tmp_f = os.path.join(tmp_d, f"{str(i)}.tif")
        print(f"Temp file for video {tmp_f}")

        masks = [Frame._bitarray_to_np(f.mask) for f in v]
        bw_fs = [m.astype(np.bool).astype(np.integer) for m in masks]
        bw_ar = np.array(bw_fs).astype(np.uint8)

        tifffile.imsave(tmp_f, bw_ar)

    with warnings.catch_warnings(record=True) as w:
        testVC = VideoCollection.from_glob(f"{tmp_d}/*",
                                           composite=True,
                                           n_jobs=1)
        assert w

    shutil.rmtree(tmp_d)

    for v in testVC:
        v.filename = None
        for f in v:
            f.filename = None
    assert refVC == testVC


def contained(item, collection):
    assert item in collection


def testget_contours():
    ac = refVC.get_contours()
    for vid in refVC:
        for f in vid:
            yield contained, f.contour, ac


@raises(ValueError)
def testget_aligned_contours_uncalculated():
    refVC.get_aligned_contours()


def testget_aligned_contours_calculated():
    VC = copy.deepcopy(refVC)  # make a copy
    VC.align_contours()
    aac = VC.get_aligned_contours()
    for vid in VC:
        for f in vid:
            yield contained, f.contour_aligned, aac


@raises(UserWarning)
def checkget_embeddings_uncalculated(source, method):
    refVC.get_embeddings(source=source, method=method)


def testget_embeddings_uncalculated():
    for source, method in combs:
        yield checkget_embeddings_uncalculated, source, method


def testget_embeddings_calculated():
    for source, method in combs:
        VC = copy.deepcopy(refVC)  # make a copy
        VC.align_contours()

        with warnings.catch_warnings(record=True) as w:
            VC.embed_shapes(source=source, method=method)

        ae = VC.get_embeddings(source=source, method=method)

        for vid in VC:
            for f in vid:
                yield contained, f.embedding[source, method], ae.values


def testget_descriptors():
    af = refVC.get_descriptors(filter_numeric=False).values

    def flatten(l):
        if (type(l) is list):
            if len(l) > 1:
                return flatten(l[0]) + flatten(l[1:])
            else:
                return flatten(l[0])
        elif (type(l) is tuple):
            return flatten(list(l))
        else:
            return [l]

    idx = 0
    for v in refVC:
        for f in v:
            fe = sorted(flatten(list(f.features.values())))
            fc = sorted(flatten(list(af[idx])))
            idx += 1
            assert fc == fe


def testalign_contours():
    VC = copy.deepcopy(refVC)
    unal_contours = VC.get_contours()
    VC.align_contours()
    al_contours = VC.get_aligned_contours()

    assert len(unal_contours) == len(al_contours)
    assert all([i is not None for i in al_contours])


def testembed():
    for source, method in combs:
        VC = copy.deepcopy(refVC)
        VC.align_contours()

        try:
            ae = VC.get_embeddings(source=source, method=method)
        except UserWarning:
            assert True

        with warnings.catch_warnings(record=True) as w:
            VC.embed_shapes(source=source, method=method)

        ae = VC.get_embeddings(source=source, method=method)
        assert ae is not None

# def testembed_shapes():
#     '''test whether the shapes from the morph dataset are embedded correctly, using descriptors/pca'''
#     dataset = datasets[1]
#     vc, _ = dataset
#     vc = copy.deepcopy(vc)
#     vc.align_contours()
#     source = 'descriptors'
#     method = 'pca'
#     try:
#         em = vc.get_embeddings(source=source, method=method)
#     except UserWarning:
#         assert True

#     with warnings.catch_warnings(record=True) as w:
#         vc.embed_shapes(source=source, method=method)

#     ae = vc.get_embeddings(source=source, method=method)

#     print('np.array([ ', end='')
#     for row in ae.values:
#         print('[ ', end='')
#         for element in row:
#             print(f'{element}, ', end='')
#         print(' ],', end='')
#     true_embedding = np.array([[0.16408436678880886, -0.738823920349008],\
#                                 [ 0.18796948912511272, -0.6342624514813923],\
#                                 [ -2.5064868168698133, -0.6163728224058317],\
#                                 [ -2.5090382214369056, -0.6132862445089283],\
#                                 [ 0.27025613194883724, 0.8842970502890174],\
#                                 [ 2.650071029865995, 4.103799482287775],\
#                                 [ 2.6521978386470844, 4.14021585718604],\
#                                 [ -0.36307887567059666, 1.864489476478187],\
#                                 [ -2.9913619090671277, 0.5788095332780233],\
#                                 [ -5.574335029487645, -0.351939703502889],\
#                                 [ -5.574335029487643, -0.35193970350289056],\
#                                 [ -2.427612684880447, -0.6552759087899546],\
#                                 [ 0.5524531096554446, -1.0029204118337527],\
#                                 [ 3.427897365914295, -1.521770150289533],\
#                                 [ 6.874527653634077, -2.727989416393354],\
#                                 [ -5.574335029487643, -0.35193970350289056],\
#                                 [ -5.574335029487645, -0.351939703502889],\
#                                 [ 2.6521978386470844, 4.14021585718604],\
#                                 [ 2.650071029865995, 4.103799482287775],\
#                                 [ 0.27025613194885245, 0.8842970502890141],\
#                                 [ -2.5090382214369056, -0.6132862445089283],\
#                                 [ -2.5064868168698133, -0.6163728224058317],\
#                                 [ -1.399052539668821, -0.7056882412914642],\
#                                 [ -0.3263011161662927, -0.7298750390043691],\
#                                 [ 0.18796948912511272, -0.6342624514813923],\
#                                 [ 0.16408436678880886, -0.738823920349008],\
#                                 [ 1.8325231950589291, -1.0647532794472825],\
#                                 [ 3.3811647632967152, -1.2626219107469296],\
#                                 [ 5.00732728790549, -1.6439746237608666],\
#                                 [ 6.910746231800655, -2.77180511622248]])
#     assert np.allclose(ae, true_embedding)




def testshape_space():
    import copy
    for source, method in combs:
        VC = copy.deepcopy(refVC)
        VC.align_contours()

        with warnings.catch_warnings(record=True) as w:
            VC.embed_shapes(source=source, method=method)

        VC.shape_space(source=source, method=method)
        pass


@raises(ValueError)
def testshow():
    for s_im, s_co in itertools.product([True, False], [True, False]):
        refVC.show(show_image=s_im, show_contour=s_co)


