from cellshapy import Frame, Video
import numpy as np
from nose.tools import raises
import warnings

import matplotlib as plt
plt.use("Agg")

f1 = base_ar = np.array([
            [0, 0,   0,   0,   0, 0, 0, 0, 0, 0],  # 0
            [0, 0, 200, 220,   0, 0, 0, 0, 0, 0],  # 1
            [0, 0, 255, 255, 150, 0, 0, 0, 5, 0],  # 2
            [0, 0,   0, 250,   0, 0, 0, 0, 0, 0],  # 3
            [0, 0,   0,   0,   0, 0, 0, 0, 0, 0],  # 4
            [0, 0,   0,   0,   0, 0, 0, 0, 0, 0],  # 5
            [0, 0,   0,   0,   0, 0, 0, 0, 0, 0]   # 6
            # 0 1    2    3    4  5  6  7  8  9
            ])

f2 = base_ar = np.array([
            [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 0
            [0,   0, 200, 220,   0, 0, 0, 0, 0, 0],  # 1
            [0, 150, 255, 255, 150, 0, 0, 0, 5, 0],  # 2
            [0,   0,   0, 250,   0, 0, 0, 0, 0, 0],  # 3
            [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 4
            [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 5
            [0,   0,   0,   0,   0, 0, 0, 0, 0, 0]   # 6
            # 0   1    2    3    4  5  6  7  8  9
            ])

f3 = base_ar = np.array([
            [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 0
            [0,  50, 200, 225,   0, 0, 0, 0, 0, 0],  # 1
            [0, 150, 255, 255, 150, 0, 0, 0, 5, 0],  # 2
            [0,   0,   0, 250,   0, 0, 0, 0, 0, 0],  # 3
            [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 4
            [0,   0,   0,   0,   0, 0, 0, 0, 0, 0],  # 5
            [0,   0,   0,   0,   0, 0, 0, 0, 0, 0]   # 6
            # 0   1    2    3    4  5  6  7  8  9
            ])

fs = [f1, f2, f3]
bw_fs = [f.astype(np.bool).astype(np.integer) for f in fs]


# __init__
def test__init__():
    v = Video(0)
    with warnings.catch_warnings(record=True) as w:
        v.frames = [Frame(f, frame_index=i, video_index=0)
                    for i, f in enumerate(bw_fs)]
        assert w
    pass


# from_dir
def testfrom_dir():
    import skimage.io
    import tempfile as tmp
    import shutil

    tmp_d = tmp.mkdtemp()

    for i, ar in enumerate(bw_fs):
        tmp_f = f"{tmp_d}/{i}.png"
        print(tmp_f)
        with warnings.catch_warnings(record=True) as w:
            skimage.io.imsave(tmp_f, ar)
            assert w

    v1 = Video(0)
    v1.frames = [Frame(ar, frame_index=i, video_index=0, min_size=2)
                 for i, ar in enumerate(bw_fs)]

    v2 = Video.from_dir(path=tmp_d, video_index=0, min_size=2, n_jobs=1)

    shutil.rmtree(tmp_d)

    v2.filename = None
    for f in v2:
        f.filename = None
    assert v1 == v2


# from_composite
def testfrom_composite():
    import os
    import tempfile as tmp
    from skimage.external import tifffile

    fs = [f1, f2, f3, f1, f2]
    bw_fs = [f.astype(np.bool).astype(np.integer) for f in fs]
    bw_ar = np.array(bw_fs).astype(np.uint8)

    v1 = Video(0)
    with warnings.catch_warnings(record=True) as w:
        v1.frames = [Frame(f, frame_index=i, video_index=0)
                     for i, f in enumerate(bw_fs)]
        assert w

    _, tmp_f = tmp.mkstemp(suffix='.tif')
    tifffile.imsave(tmp_f, bw_ar)

    with warnings.catch_warnings(record=True) as w:
        v2 = Video.from_composite(path=tmp_f, video_index=0, n_jobs=1)
        assert w

    os.unlink(tmp_f)

    v2.filename = None
    for f in v2:
        f.filename = None
    assert v1 == v2


# show
@raises(ValueError)
def testshow():
    fs = [f1, f2, f3, f1, f2]
    bw_fs = [f.astype(np.bool).astype(np.integer) for f in fs]
    v = Video(0)
    with warnings.catch_warnings(record=True) as w:
        v.frames = [Frame(f, frame_index=i, video_index=0)
                    for i, f in enumerate(bw_fs)]
        assert w

    v.show(show_image=True, show_contour=True)
    v.show(show_image=True, show_contour=False)
    v.show(show_image=False, show_contour=True)
    v.show(show_image=False, show_contour=False)
