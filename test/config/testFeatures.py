def testCheckPackage():
    import warnings
    from cellshapy.config.features import checkPackage
    if not checkPackage("sys"):
        assert False
    if not checkPackage("cellshapy"):
        assert False

    with warnings.catch_warnings(record=True) as w:
        if checkPackage("qwertyuiopasdfghjklzxcvbnm"):
            assert False
        assert w

def testTestPackage():
    import warnings
    from cellshapy.config import pkgs

    if not pkgs.sys:
        assert False
    if not pkgs.cellshapy:
        assert False

    with warnings.catch_warnings(record=True) as w:
        if pkgs.qwertyuiopasdfghjklzxcvbnm:
            assert False
        assert w

    try:
        pkgs._ipython_canary_method_should_not_exist_
        assert False
    except AttributeError:
        assert True
