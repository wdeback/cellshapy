def test_str_to_bool():
    from cellshapy.config.config import _str_to_bool

    assert _str_to_bool("true")
    assert _str_to_bool("TRUE")
    assert _str_to_bool("True")
    assert _str_to_bool("t")
    assert _str_to_bool("T")
    assert _str_to_bool("1")
    assert _str_to_bool("y")
    assert _str_to_bool("Y")
    assert _str_to_bool("yes")
    assert _str_to_bool("Yes")
    assert _str_to_bool("Yes")

    assert not _str_to_bool('false')
    assert not _str_to_bool('False')
    assert not _str_to_bool('FALSE')
    assert not _str_to_bool('f')
    assert not _str_to_bool('F')
    assert not _str_to_bool('0')
    assert not _str_to_bool('23')
    assert not _str_to_bool('42')
    assert not _str_to_bool('no')
    assert not _str_to_bool('No')
    assert not _str_to_bool('NO')
    assert not _str_to_bool('jfelnfesnf')


def test__get_item__():
    from cellshapy.config import config
    try:
        config["foo"]
        assert False
    except KeyError:
        assert True

    try:
        config["foo", "bar", "baz"]
        assert False
    except KeyError:
        assert True

    assert config["contours", "n_points"]
    assert isinstance(config["contours", "n_points"], int)
    assert isinstance(config["print", "debug"], bool)
    assert isinstance(config["plotting", "duration"], float)

def test_find_type_missing_default():
    from cellshapy.config import config
    import warnings

    with warnings.catch_warnings(record=True) as w:
        config._find_type(("foo", "bar"))
        assert w

def test_find_type_multiple_values():
    import tempfile as tmp
    import os
    from cellshapy.config.config import config
    from cellshapy.config.config import _defaults

    tmp_f = tmp.mktemp()
    _defaults["print", "debug", str] = "False"
    c2 = config(tmp_f)
    try:
        t = c2._find_type(("print", "debug"))
        assert False
    except KeyError:
        assert True
    os.unlink(tmp_f)

